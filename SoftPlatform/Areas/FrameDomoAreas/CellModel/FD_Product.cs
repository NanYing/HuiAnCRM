
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：FD_Product(框架演示商品管理)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public  int?  FD_ProductID{get;set;}

        /// <summary>
        /// 商品类别下拉
        /// </summary>
        //public  int?  FD_PCategoryDropID{get;set;}

        /// <summary>
        /// 商品类别树
        /// </summary>
        //public  int?  FD_PCategoryTreeID{get;set;}

        /// <summary>
        /// 商品编号
        /// </summary>
        public  string  FDProductNo{get;set;}

        /// <summary>
        /// 商品名称
        /// </summary>
        public  string  FDProductName{get;set;}

        /// <summary>
        /// 商品编号、名称
        /// </summary>
        public  string  FDProductNo__FDProductName{get;set;}

        /// <summary>
        /// 规格
        /// </summary>
        public  string  FDSpecifications{get;set;}

        /// <summary>
        /// 商品价格
        /// </summary>
        public  decimal?  FDProductPrice{get;set;}

        /// <summary>
        /// 上市日期
        /// </summary>
        public  DateTime?  FDListedDate{get;set;}

        /// <summary>
        /// 创建人ID
        /// </summary>
        public  int?  FDAddPersonID{get;set;}

        /// <summary>
        /// 创建人
        /// </summary>
        public  string  FDAddPersonName{get;set;}

        /// <summary>
        /// 商品状态
        /// </summary>
        public  int?  FDProductStatuID{get;set;}

        /// <summary>
        /// 商品状态
        /// </summary>
        public  string  FDProductStatuName{get;set;}

        /// <summary>
        /// 商品描述
        /// </summary>
        public  string  FDProductContext{get;set;}

        /// <summary>
        /// 主图片文件名
        /// </summary>
        public  string  FDProductMainImageFileName{get;set;}

        /// <summary>
        /// 主图片
        /// </summary>
        public  string  FDProductMainImageFilePath{get;set;}

        /// <summary>
        /// 文件附件标识
        /// </summary>
        public  string  FD_ProductAttachmentGuid{get;set;}

        /// <summary>
        /// 附件
        /// </summary>
        public  int?  FD_ProductAtt{get;set;}

        public SoftProjectAreaEntity FD_Product { get; set; }
        public List<SoftProjectAreaEntity> FD_Products { get; set; }
    }
}
