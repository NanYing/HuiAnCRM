﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Core
{
    public static class IntExt
    {
        #region Int32转换为4字节byte数组

        /// <summary>
        /// Int32转换为4字节byte数组
        /// </summary>
        /// <param name="Num"></param>
        /// <returns></returns>
        public static byte[] ToBytes4(this Int32? Num)
        {
            Int32 val = 0;
            if (Num != null)
                val = (int)Num;

            byte[] bytes = val.ToBytes4();
            return bytes;
        }

        public static byte[] ToBytes4(this Int32 val)
        {
            byte[] bytes = BitConverter.GetBytes(val);
            return bytes;
        }

        #endregion

        #region Int32转换为2字节byte数组

        public static byte[] ToBytes2(this Int32? Num)
        {
            Int32 val = 0;
            if (Num != null)
                val = (int)Num;

            byte[] bytes = val.ToBytes2();
            return bytes;
        }

        public static byte[] ToBytes2(this Int32 Num)
        {
            var val = Convert.ToInt16(Num);
            byte[] bytes = BitConverter.GetBytes(val);
            return bytes;
        }

        #endregion

        #region Int32转换为1字节byte数组

        public static byte ToBytes1(this Int32? Num)
        {
            Int32 val = 0;
            if (Num != null)
                val = (int)Num;

            byte bytes = val.ToBytes1();
            return bytes;
        }

        public static byte ToBytes1(this Int32 Num)
        {
            var val = Convert.ToByte(Num);
            byte[] bytes = BitConverter.GetBytes(val);
            return bytes[0];
        }

        #endregion

    }
}
