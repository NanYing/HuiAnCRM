﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：FD_ProductAttachment(商品附件框架演示)
    /// </summary>
    public class FD_ProductAttachmentController : BaseController
    {
        public FD_ProductAttachmentController()
        {
        }

        /// <summary>
        /// 商品附件框架演示--编辑列表
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexEdit(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            ModularOrFunCode = "FrameDomoAreas.FD_ProductAttachment.IndexEdit";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (!string.IsNullOrEmpty(domain.Item.FD_ProductAttachmentGuid))
            {
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "RefPKTableGuid___equal", Value = domain.Item.FD_ProductAttachmentGuid });
                resp = domain.QueryIndex();
            }
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 商品附件框架演示--查看列表
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexDetail(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            ModularOrFunCode = "FrameDomoAreas.FD_ProductAttachment.IndexDetail";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (!string.IsNullOrEmpty(domain.Item.FD_ProductAttachmentGuid))
            {
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "RefPKTableGuid___equal", Value = domain.Item.FD_ProductAttachmentGuid });
                resp = domain.QueryIndex();
            }
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 商品附件框架演示--上传
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpLoad(SoftProjectAreaEntityDomain domain)
        {
            #region 保存文件

            if (domain.Item.RefPKTableGuid == null)
                throw new Exception("附件Guid不能为空");
            domain.Item.AttachmentFileSizeDisp = Tool.FileSizeDisp(domain.Item.AttachmentFileSize);
            ModularOrFunCode = "FrameDomoAreas.FD_ProductAttachment.Upload";
            domain.ModularOrFunCode = ModularOrFunCode;
            var respadd = domain.AddSave();
            #endregion

            #region 查询
            if (respadd.Item.FD_ProductAttachmentID == null)
                throw new Exception("添加失败");
            domain.Item.FD_ProductAttachmentID = respadd.Item.FD_ProductAttachmentID;
            ModularOrFunCode = "FrameDomoAreas.FD_ProductAttachment.IndexEdit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            resp.Items.Add(resp.Item);
            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;

            #endregion
            return View("Rows", resp);
        }

        /// <summary>
        /// 商品附件框架演示--删除
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult Delete(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_ProductAttachment.Delete";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.DeleteByID();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 商品附件框架演示--下载列表
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexDown(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            ModularOrFunCode = "FrameDomoAreas.FD_ProductAttachment.IndexDown";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (!string.IsNullOrEmpty(domain.Item.FD_ProductAttachmentGuid))
            {
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "RefPKTableGuid___equal", Value = domain.Item.FD_ProductAttachmentGuid });
                resp = domain.QueryIndex();
            }
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 商品附件框架演示--图片列表
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexImage(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            ModularOrFunCode = "FrameDomoAreas.FD_ProductAttachment.IndexImage";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (!string.IsNullOrEmpty(domain.Item.FD_ProductAttachmentGuid))
            {
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "RefPKTableGuid___equal", Value = domain.Item.FD_ProductAttachmentGuid });
                resp = domain.QueryIndex();
            }
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

    }
}
