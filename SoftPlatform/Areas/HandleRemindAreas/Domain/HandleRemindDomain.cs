﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using SoftPlatform.Controllers;
namespace SoftProject.Domain
{
    public partial class SoftProjectAreaEntityDomain
    {
        #region 客户下拉
        static List<SoftProjectAreaEntity> _CustomerIDDrops = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> CustomerIDDrops
        {
            get
            {
               // if (_CustomerIDDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _CustomerIDDrops = domain.CustomerIdDrops_GetAll().Items;
                }
                return _CustomerIDDrops;
            }
        }

    
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static void CustomerIDDrop_Clear()
        {
            _CustomerIDDrops = new List<SoftProjectAreaEntity>();
        }


        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase CustomerIdDrops_GetAll()
        {
            string sql = "SELECT * FROM V_C_Customer";// where C_CustomerID={0}", C_CustomerID);
            var resp = Query16(sql, 2);
            return resp;
        } /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase getHandleRemind()
        {
            string sql = string.Format("SELECT * FROM V_HandleRemind where Pre_UserID={0} and RemindFS like '%1%' and (getdate() between RemindTime and DoThisTime)", LoginInfo.Pre_UserID);
            var resp = Query16(sql, 2);
            return resp;
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void CustomerIDDrop_AddCache()
        {

            ModularOrFunCode = "CustomerAreas.C_CustomerID.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.CustomerIDDrops.Add(resp.Item);

        }

        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void CustomerIDDrop_UpdateCache()
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerID.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var CustomerIDDrop = SoftProjectAreaEntityDomain.CustomerIDDrops.Where(p => p.C_CustomerID == Item.C_CustomerID).FirstOrDefault();

            SoftProjectAreaEntityDomain.CustomerIDDrops.Remove(CustomerIDDrop);
            SoftProjectAreaEntityDomain.CustomerIDDrops.Add(resp.Item);
        }

        /// <summary>
        /// 查询条件-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string QueryHtmlDropDownList_C_CustomerID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var CustomerIDDropsTemp = CustomerIDDrops;
            var str = HtmlHelpers.DropDownList(null, "C_CustomerID___equal", CustomerIDDropsTemp, "C_CustomerID", "CustomerName", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        /// <summary>
        /// 编辑页面-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string HtmlDropDownList_C_CustomerID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var CustomerIDDropsTemp = CustomerIDDrops;
            var str = HtmlHelpers.DropDownList(null, "Item.C_CustomerID", CustomerIDDropsTemp, "C_CustomerID", "CustomerName", val, "");
            return str.ToString();
        }
        #endregion

        #region 客户下拉
        static List<SoftProjectAreaEntity> _RemindFS = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> RemindFS
        {
            get
            {
                // if (_CustomerIDDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _RemindFS = domain.RemindFS_GetAll().Items;
                }
                return _RemindFS;
            }
        }
        public MyResponseBase RemindFS_GetAll()
        {
            string sql = string.Format("SELECT  dbo.Sys_DictDetail.DText, dbo.Sys_DictDetail.DValue" +
            " FROM         dbo.Sys_Dict INNER JOIN" +
                     " dbo.Sys_DictDetail ON dbo.Sys_Dict.Sys_DictID = dbo.Sys_DictDetail.Sys_DictID" +
            " WHERE     (dbo.Sys_Dict.Category = 'RemindFS')");
            var resp = Query16(sql, 2);
            return resp;
        }
        #endregion
        public MyResponseBase HandleRemind_AddSave()
        {
            var resp = new MyResponseBase();
            #region 提方式
            if (!string.IsNullOrEmpty(Item.RemindFSs))
            {
                Item.RemindFSs = Item.RemindFSs.Substring(0, Item.RemindFSs.Length - 1);
                var arrs = Item.RemindFSs.Split(',');
                var RemindFSNames = "";
                var RemindFS = "";
                foreach (var s in arrs)
                {
                    RemindFSNames += SoftProjectAreaEntityDomain.RemindFS.Where(p => p.DValue.ToString() == s).FirstOrDefault().DText + ",";
                    RemindFS += s + ",";
                }
                RemindFSNames = RemindFSNames.Substring(0, RemindFSNames.Length - 1);
                Item.RemindFSText = RemindFSNames;
                RemindFS = RemindFS.Substring(0, RemindFS.Length - 1);
                Item.RemindFS = RemindFS;
            }
            #endregion
             resp = ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                resp =AddSave();
            }));
            return resp;
        }

    }
}
