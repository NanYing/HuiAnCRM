
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：MeetingRoom(会议室管理)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 月份
        /// </summary>
        public int month { get; set; }

        /// <summary>
        /// 日
        /// </summary>
        public int day { get; set; }

        /// <summary>
        /// X轴数据
        /// </summary>
        public string Xdata { get; set; }

        /// <summary>
        /// Y轴数据
        /// </summary>
        public string Ydata { get; set; }

        /// <summary>
        /// 年月（如2014年9月）
        /// </summary>
        public string YearMonth { get; set; }

        /// <summary>
        /// 新增客户总数量
        /// </summary>
        public int data1 { get; set; }

        /// <summary>
        /// 修改客户总数量
        /// </summary>
        public int data2 { get; set; }

        /// <summary>
        /// 删除客户总数量
        /// </summary>
        public int data3 { get; set; }

        /// <summary>
        /// 年
        /// </summary>
        public int year { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public int C_ReportID { get; set; }

        public SoftProjectAreaEntity C_Report { get; set; }
        public List<SoftProjectAreaEntity> C_Reports { get; set; }
    }
}
