
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：C_CustomerTransaction(客户成交记录)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 成交编号
        /// </summary>
        public  int?  C_CustomerTransactionID{get;set;}

        /// <summary>
        /// 客户编号
        /// </summary>
        //public  int?  C_CustomerID{get;set;}

        /// <summary>
        /// 约访编号
        /// </summary>
        //public  int?  C_CustomerYuefangID{get;set;}

        /// <summary>
        /// 保单号码
        /// </summary>
        public  string  TransactionPolicyNumber{get;set;}

        /// <summary>
        /// 成交险种
        /// </summary>
        public  string  TransactionInsurance{get;set;}

        /// <summary>
        /// 受益人
        /// </summary>
        public  string  TransactionBeneficiary{get;set;}

        /// <summary>
        /// 被保险人
        /// </summary>
        public  string  TransactionInsured{get;set;}

        /// <summary>
        /// 投保日期
        /// </summary>
        public  DateTime?  TransactionDate{get;set;}

        /// <summary>
        /// 保费
        /// </summary>
        public  decimal?  TransactionPremium{get;set;}

        /// <summary>
        /// 保单年限
        /// </summary>
        public  int?  TransactionPolicyLife{get;set;}

        /// <summary>
        /// 录入时间
        /// </summary>
        public  DateTime?  TransactionInputTime{get;set;}

        /// <summary>
        /// 录入员
        /// </summary>
        public  string  TransactionInputMen{get;set;}

        /// <summary>
        /// 录入人ID
        /// </summary>
        public  int?  TransactionInputMenID{get;set;}

        public SoftProjectAreaEntity C_CustomerTransaction { get; set; }
        public List<SoftProjectAreaEntity> C_CustomerTransactions { get; set; }
    }
}
