﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：Pre_Department(部门管理)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 缓存字段和属性
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
#region 缓存定义
static List<SoftProjectAreaEntity> _Pre_Departments = new List<SoftProjectAreaEntity>();

public static List<SoftProjectAreaEntity> Pre_Departments
{
    get
    {
        if (_Pre_Departments.Count == 0)
        {
            SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
            _Pre_Departments = domain.Pre_Department_GetAll().Items;
        }
        return _Pre_Departments;
    }
}

#endregion

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static void Pre_Departments_Clear()
{
    _Pre_Departments = new List<SoftProjectAreaEntity>();
}


        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public MyResponseBase Pre_Department_GetAll()
{
    string sql = "SELECT * FROM V_Pre_Department ";
    var resp = Query16(sql, 2);
    return resp;
}

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public void Pre_Department_AddCache()
{

    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Index";
    Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
    resp = ByID();
    SoftProjectAreaEntityDomain.Pre_Departments.Add(resp.Item);

}

        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public void Pre_Department_UpdateCache()
{
    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Index";
    Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
    resp = ByID();
    var Pre_Department = SoftProjectAreaEntityDomain.Pre_Departments.Where(p => p.Pre_DepartmentID == Item.Pre_DepartmentID).FirstOrDefault();

    SoftProjectAreaEntityDomain.Pre_Departments.Remove(Pre_Department);
    SoftProjectAreaEntityDomain.Pre_Departments.Add(resp.Item);
}

        /// <summary>
        /// 查询条件-下拉树(父节点)--自身管理
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string QueryHtmlDropDownList_DepartmentParentID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items = Pre_Departments;
var tt = new SelectTreeList(Items, "0", "DepartmentName", "Pre_DepartmentID", "DepartmentParentID", "Pre_DepartmentID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "DepartmentParentID___equal", tt, "=="+NameCn+"==");
    var strDrop = str.ToString();
    return strDrop;
}

        /// <summary>
        /// 编辑页面-下拉树(父节点)-模块自身管理
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string HtmlDropDownLiss_DepartmentParentID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items= Pre_Departments;
var tt = new SelectTreeList(Items, "0", "DepartmentName", "Pre_DepartmentID", "DepartmentParentID", "Pre_DepartmentID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "Item.DepartmentParentID", tt, "=="+NameCn+"==");
    return str.ToString();
}


        /// <summary>
        /// 查询条件-下拉树(节点)--其它模块使用
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string QueryHtmlDropDownList_Pre_DepartmentID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items = Pre_Departments;
var tt = new SelectTreeList(Items, "0", "DepartmentName", "Pre_DepartmentID", "DepartmentParentID", "Pre_DepartmentID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "Pre_DepartmentID___equal", tt, "=="+NameCn+"==");
    var strDrop = str.ToString();
    return strDrop;
}

        /// <summary>
        /// 编辑页面-下拉树(父节点)-其它模块使用
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string HtmlDropDownLiss_Pre_DepartmentID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items= Pre_Departments;
var tt = new SelectTreeList(Items, "0", "DepartmentName", "Pre_DepartmentID", "DepartmentParentID", "Pre_DepartmentID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "Item.Pre_DepartmentID", tt, "=="+NameCn+"==");
    return str.ToString();
}


        /// <summary>
        /// 列表页面-树(父节点)
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string JqTreeNs_Pre_DepartmentID(string paramNameType, string NameCn, SoftProjectAreaEntity item)
{
    var TreeQueryType = Convert.ToInt32(paramNameType);
    var Itemst = SoftProjectAreaEntityDomain.Pre_Departments;
    var treeList = new TreeList(Itemst, "0", "DepartmentName", "Pre_DepartmentID", "DepartmentParentID", "", "", "Pre_DepartmentID", "", "");
    var str = HtmlHelpersProject.JqTreeN(null, "Pre_DepartmentID", treeList, "", TreeQueryType);
    return str.ToString();
}

   }
}
