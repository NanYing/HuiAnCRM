﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;
using SoftProject.CellModel;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：C_Customer(客户信息)
    /// </summary>
    public class C_CustomerController : BaseController
    {
        public C_CustomerController()
        {
        }

        /// <summary>
        /// 客户信息--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {

            ModularOrFunCode = "CustomerAreas.C_Customer.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (LoginInfo.Pre_RoleID == 5)
            {
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "ServiceUserID___equal", Value = LoginInfo.Pre_UserID.ToString() });
            }
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }
        /// <summary>
        /// 客户信息--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteIndex(SoftProjectAreaEntityDomain domain)
        {

            ModularOrFunCode = "CustomerAreas.C_Customer.DeleteIndex";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户信息--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            #region 初始值
            ModularOrFunCode = "CustomerAreas.C_Customer.Add";
            resp.Item.CustomerInputTime = DateTime.Now;
            resp.Item.CustomerInputMen = LoginInfo.UserName;
            resp.Item.CustomerInputMenID = LoginInfo.Pre_UserID;
            resp.Item.ServiceUserID = LoginInfo.Pre_UserID;
            resp.Item.CustomerDataStatus = 0;
            resp.Item.CustomerDeleteSmsStatus = 0;
            resp.Item.VisitCount = 0;
            resp.Item.BargainCount = 0;
            #endregion
            SoftProjectAreaEntityDomain.ServiceUserDrops_Clear();
            ProjectCache.HtmlDropDownLiss.Remove("ServiceUserID");
            ProjectCache.HtmlDropDownLiss.Add("ServiceUserID", SoftProjectAreaEntityDomain.HtmlDropDownList_ServiceUserID);
            //  SoftProjectAreaEntityDomain.initTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_Customer.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户信息--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            ModularOrFunCode = "CustomerAreas.C_Customer.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (!domain.IsEmail(domain.Item.CustomerEmail))
            {
                throw new Exception("Email地址格式不正确！");
            }
            domain.Item.VisitCount = 0;
            domain.Item.BargainCount = 0;
            var resp = domain.AddSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 客户信息--删除客户
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public HJsonResult Delete(SoftProjectAreaEntityDomain domain)
        {

            #region 初始值

            #endregion
            ModularOrFunCode = "CustomerAreas.C_Customer.Delete";
            domain.ModularOrFunCode = ModularOrFunCode;
            string C_CustomerID = Request.QueryString["C_CustomerID"];
            SoftProjectAreaEntityDomain.C_CustomerIDs = C_CustomerID;
            //int? x = domain.Item.C_CustomerID;
            MyResponseBase resp = domain.deleteCurstomerStatic();
            resp = domain.quueryDeleteCurstomerStatic();
            Dictionary<int?, List<SoftProjectAreaEntity>> myDic = new Dictionary<int?, List<SoftProjectAreaEntity>>();
            Dictionary<string, string> messageDic = new Dictionary<string, string>();
            if (resp.Items.Count >= 3)
            {
                foreach (SoftProjectAreaEntity item in resp.Items)
                {
                    if (item.ServiceUserID != null)
                    {
                        if (!myDic.ContainsKey(item.ServiceUserID))
                            myDic[item.ServiceUserID] = new List<SoftProjectAreaEntity>();
                        myDic[item.ServiceUserID].Add(item);
                    }
                }
                foreach (KeyValuePair<int?, List<SoftProjectAreaEntity>> kvp in myDic)
                {
                    string customerName = "";
                    foreach (SoftProjectAreaEntity item in kvp.Value)
                    {
                        customerName += item.CustomerName + ",";
                    }
                    customerName = customerName.Substring(0, customerName.Length - 2);
                    string message = "您在【汇安财富】的客户 " + customerName + " 已经被管理员删除！";
                    string phone = domain.queryUserPhone(kvp.Key);
                    if (phone != null && phone.Length > 0)
                        messageDic.Add(phone, message);
                }
            }
            return new HJsonResult(new { Data = messageDic });
        }

        /// <summary>
        /// 客户信息--还原客户
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public HJsonResult Redelete(SoftProjectAreaEntityDomain domain)
        {

            #region 初始值

            #endregion
            ModularOrFunCode = "CustomerAreas.C_Customer.Delete";
            domain.ModularOrFunCode = ModularOrFunCode;
            string C_CustomerID = Request.QueryString["C_CustomerID"];
            SoftProjectAreaEntityDomain.C_CustomerIDs = C_CustomerID;
            //int? x = domain.Item.C_CustomerID;
            var resp = domain.reDeleteCurstomerStatic();
            return new HJsonResult(new { Data = 1 });
        }

        /// <summary>
        /// 客户信息--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();


            SoftProjectAreaEntityDomain.ServiceUserDrops_Clear();
            ProjectCache.HtmlDropDownLiss.Remove("ServiceUserID");
            ProjectCache.HtmlDropDownLiss.Add("ServiceUserID", SoftProjectAreaEntityDomain.HtmlDropDownList_ServiceUserID);

            ModularOrFunCode = "CustomerAreas.C_Customer.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            SoftProjectAreaEntityDomain.initCustomerTab(this, resp, domain);
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户信息--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (!domain.IsEmail(domain.Item.CustomerEmail))
            {
                throw new Exception("Email地址格式不正确！");
            }
            var resp = domain.EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 客户信息--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            SoftProjectAreaEntityDomain.initCustomerTabView(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_Customer.Detail";
            //   domain.ModularOrFunCode = ModularOrFunCode;

            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }
        /// <summary>
        /// 客户信息--基本信息
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult BaseInfo(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.BaseInfo";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            ModularOrFunCode = "CustomerAreas.C_Customer.BaseInfo";
            domain.ModularOrFunCode = ModularOrFunCode;
            resp.FunNameEn = "BaseInfo";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户信息--相关人员
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Dashboard(SoftProjectAreaEntityDomain domain)
        {
            if (domain.Item.C_CustomerID == null)
                throw new Exception("客户ID不能为空");
            //var resp = new MyResponseBase { Item=domain.Item};

            ModularOrFunCode = "CustomerAreas.C_Customer.Dashboard";
            domain.ModularOrFunCode = ModularOrFunCode;
            //   domain.Design_ModularOrFun = Design_ModularOrFun;

            var resp = domain.ByID();

            return View("Dashboard", resp);
        }
        [HttpGet]
        public ActionResult ExportToExcel(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.ExportToExcel";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string cid = Request.Params["C_CustomerID"];
            var resp = domain.queryCurstomerByIds(cid);
            var Fields = HtmlHelpersProject.PageFormEleTypes(Design_ModularOrFun);
            byte[] buff = Data2Excel.ToExcel(resp.Items, Fields);
            #region 文件下载

            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            Response.ContentType = "application/octet-stream";
            //String filename = HttpUtility.UrlEncode("客户预测.xls", System.Text.Encoding.UTF8);
            String filename = "客户信息(" + string.Format("{0:d}", DateTime.Now) + ").xls";
            filename = filename.Replace("/", "-");
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(buff);
            Response.Flush();
            Response.End();

            #endregion
            return new EmptyResult();
        }
        /// <summary>
        /// 客户信息--发送邮件
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult SendMail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.SendMail";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string toMail = Request.Form["toMail"].ToString();
            string subject = Request.Form["subject"].ToString();
            string content = Request.Form["content"].ToString();
            string[] reciver = toMail.Split(',');
            string FromMail = LoginInfo.Email;
            string MailPassword = LoginInfo.MailPassword;
            var result = "";
            for (int i = 0; i < reciver.Length; i++)
            {
                MailMessage myMail = new MailMessage(FromMail, reciver[i]);
                myMail.Subject = subject;//主题
                myMail.Body = content;//正文
                SmtpClient sc = new SmtpClient("smtp." + FromMail.Substring(FromMail.IndexOf("@") + 1));//发邮件的服务器
                NetworkCredential nc = new NetworkCredential(FromMail, MailPassword);//申请的帐户信息
                sc.Credentials = nc;
                try
                {
                    sc.Send(myMail);
                    result = "success";
                }
                catch
                {
                    result = "fail";
                }
            }

            return new HJsonResult(new { Data = result });
        }
        /// <summary>
        /// 获取业务员
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public HJsonResult getPreUser(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            ModularOrFunCode = "CustomerAreas.C_Customer.getPreUser";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.queryUserList();
            return new HJsonResult(new { Data = resp });
        }
        /// <summary>
        /// 分配业务员
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public HJsonResult setPreUser(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion

            ModularOrFunCode = "CustomerAreas.C_Customer.setPreUser";
            domain.ModularOrFunCode = ModularOrFunCode;
            string C_CustomerID = Request.QueryString["C_CustomerID"];
            string ServiceUserID = Request.QueryString["ServiceUserID"];
            SoftProjectAreaEntityDomain.C_CustomerIDs = C_CustomerID;
            var resp = domain.setCurstomerServiceUser(ServiceUserID);
            return new HJsonResult(new { Data = 1 });
        }
        /// <summary>
        /// 客户信息--查询(业务员)
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexByUser(SoftProjectAreaEntityDomain domain)
        {

            ModularOrFunCode = "CustomerAreas.C_Customer.IndexByUser";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.queryCurstomerByServiceUser(LoginInfo.Pre_UserID);

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }
    }
}