﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：ActivitySeat(活动座位管理)
    /// </summary>
    public class ActivitySeatController : BaseController
    {
        public ActivitySeatController()
        {
        }

        /// <summary>
        /// 活动座位管理
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        //[HttpPost]
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ActivityAreas.ActivitySeat.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 活动查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        //[HttpPost]
        public HJsonResult ActivityBaseInfo(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ActivityAreas.ActivitySeat.ActivityBaseInfo";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string ActivityID = Request.Form["ActivityID"].ToString();
            var resp = domain.ActivityBaseInfo(ActivityID);//获取当前活动信息
            var resp1 = domain.ActivitySeatInfo(ActivityID);//获取座位信息
            return new HJsonResult(new { ActivityData = resp,SeatData = resp1});
        }

        /// <summary>
        /// 客户查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        //[HttpPost]
        public HJsonResult CustomerBaseInfo(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ActivityAreas.ActivitySeat.CustomerBaseInfo";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string CustomerIdCardNo = Request.Form["CustomerIdCardNo"].ToString();
            string ActivityID = Request.Form["ActivityID"].ToString();

            var resp = domain.CustomerBaseInfo(CustomerIdCardNo);
            var resp1 = domain.CustomerSeatInfo(CustomerIdCardNo, ActivityID);
            return new HJsonResult(new { CustomerData = resp,CustomerSeatData = resp1 });
        }

        /// <summary>
        /// 活动座位--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        //[HttpPost]
        public HJsonResult Save(SoftProjectAreaEntityDomain domain)
        {
           
            string CustomerIdCardNo = Request.Form["CustomerIdCardNo"].ToString();
            string ActivityID = Request.Form["ActivityID"].ToString();
            string Seat = Request.Form["SeatNum"].ToString();
            var resp1 = domain.CustomerSeatInfo(CustomerIdCardNo, ActivityID);        
            string OldSeat = resp1.Item.Seat;//记录当前客户此次活动上一次保存的座位
            var resp2 = domain.CustomerBaseInfo(CustomerIdCardNo);
            
            ModularOrFunCode = "ActivityAreas.ActivitySeat.Save";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            if (OldSeat == null)
            {
                //var resp = domain.Default();
                domain.Item.C_CustomerID = resp2.Item.C_CustomerID;
                domain.Item.ActivityID = Convert.ToInt32(ActivityID);
                domain.Item.Seat = Seat;
                domain.AddSave();
            }
            else {
                domain.Item.ActivitySeatID = resp1.Item.ActivitySeatID;
                domain.Item.C_CustomerID = resp2.Item.C_CustomerID;
                domain.Item.ActivityID = Convert.ToInt32(ActivityID);
                domain.Item.Seat = Seat;
                domain.EditSave();
            }
            
            return new HJsonResult(new { Data = OldSeat });
        }
    }
}
