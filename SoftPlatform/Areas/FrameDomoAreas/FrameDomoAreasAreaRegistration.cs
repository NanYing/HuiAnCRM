﻿using SoftProject.Domain;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    public class FrameDomoAreasAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FrameDomoAreas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FrameDomoAreas_default",
                "FrameDomoAreas/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            ProjectCache.QueryHtmlDropDownLists.Add("FD_PCategoryDropID", SoftProjectAreaEntityDomain.QueryHtmlDropDownList_FD_PCategoryDropID);
            ProjectCache.HtmlDropDownLiss.Add("FD_PCategoryDropID", SoftProjectAreaEntityDomain.HtmlDropDownLiss_FD_PCategoryDropID);

            #region 树

            //自身管理：按父节点查询
            ProjectCache.QueryHtmlDropTrees.Add("PCategoryTreeParentID", SoftProjectAreaEntityDomain.QueryHtmlDropDownList_PCategoryTreeParentID);
            ProjectCache.HtmlDropTrees.Add("PCategoryTreeParentID", SoftProjectAreaEntityDomain.HtmlDropDownLiss_PCategoryTreeParentID);

            //其它模块使用
            ProjectCache.QueryHtmlDropTrees.Add("FD_PCategoryTreeID", SoftProjectAreaEntityDomain.QueryHtmlDropDownList_FD_PCategoryTreeID);
            ProjectCache.HtmlDropTrees.Add("FD_PCategoryTreeID", SoftProjectAreaEntityDomain.HtmlDropDownLiss_FD_PCategoryTreeID);

            //列表显示树
            ProjectCache.JqTreeNs.Add("FD_PCategoryTreeID", SoftProjectAreaEntityDomain.JqTreeNs_FD_PCategoryTreeID);

            #endregion
        }
    }
}
