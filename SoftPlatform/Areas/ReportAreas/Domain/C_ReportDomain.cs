﻿
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

//namespace Framework.Web.Mvc
namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：C_Report(报表管理)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {

        #region 单客户
        /// <summary>
        /// 查询成交次数
        /// </summary>
        /// <param name="year"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerReport(string year, string CustomerID)
        {
            //var sql = string.Format("select CAST(DAY(OrderDate) as varchar(20)) + '日' as Xdata,SUM(OrderAmount) as Ydata from OrderReport where OrderDate between '{0}' and '{1}' group by DAY(OrderDate)", year, CustomerID);
            var sql = string.Format("select month(YuefangVisitingTime)  as month,cast(count(*) as varchar(20)) as Ydata from V_C_CustomerYuefang where year(YuefangVisitingTime) = {0} and C_CustomerID = {1} group by month(YuefangVisitingTime)", year, CustomerID);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items);
            return resp;
        }


        /// <summary>
        /// 查询成交金额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerMoneyReport(string year, string CustomerID)
        {
            //var sql = string.Format("select CAST(DAY(OrderDate) as varchar(20)) + '日' as Xdata,SUM(OrderAmount) as Ydata from OrderReport where OrderDate between '{0}' and '{1}' group by DAY(OrderDate)", year, CustomerID);
            var sql = string.Format("select month(TransactionDate)  as month,CAST(sum(TransactionPremium) as varchar(20))as Ydata from V_C_CustomerTransaction where year(TransactionDate) = {0} and C_CustomerID = {1} group by month(TransactionDate)", year, CustomerID);
            var resp = Query16(sql, 2);           
            resp.Items = FullItem(resp.Items);
            return resp;
        }
        #endregion

        #region 按业务员
        /// <summary>
        /// 统计业务员的每月客户来访次数
        /// </summary>
        /// <param name="year"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public MyResponseBase queryUserLaifangReport(string year, string userID)
        {
            var sql = string.Format("select month(YuefangVisitingTime)  as month,CAST(count(*) as varchar(20))as Ydata from V_C_CustomerYuefang where year(YuefangVisitingTime) = {0} and UserNO = '{1}' group by month(YuefangVisitingTime)", year, userID);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items);
            return resp;
        }

        /// <summary>
        /// 统计业务员的每月签单额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public MyResponseBase queryUserMoneyReport(string year, string userID)
        {
            var sql = string.Format("select month(TransactionDate)  as month,CAST(sum(TransactionPremium) as varchar(20))as Ydata from V_C_CustomerTransaction where year(TransactionDate) = {0} and UserNO = '{1}' group by month(TransactionDate)", year, userID);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items);
            return resp;
        }

         /// <summary>
        /// 统计每月每月签单次数
        /// </summary>
        /// <param name="year"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public MyResponseBase queryUserCountReport(string year, string userID)
        {
            var sql = string.Format("select month(TransactionDate)  as month,CAST(count(*) as varchar(20))as Ydata from V_C_CustomerTransaction where year(TransactionDate) = {0} and UserNO = '{1}' group by month(TransactionDate)", year, userID);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items);
            return resp;
        }

        #endregion

        #region 按所有客户月统计
        /// <summary>
        /// 统计每月客户来访次数
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerLaifangReport(string year)
        {
            var sql = string.Format("select month(YuefangVisitingTime)  as month,CAST(count(*) as varchar(20))as Ydata from V_C_CustomerYuefang where year(YuefangVisitingTime) = {0}  group by month(YuefangVisitingTime)", year);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items);
            return resp;
        }

        /// <summary>
        /// 统每月签单额
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerMoneyYearReport(string year)
        {
            var sql = string.Format("select month(TransactionDate)  as month,CAST(sum(TransactionPremium) as varchar(20))as Ydata from V_C_CustomerTransaction where year(TransactionDate) = {0}  group by month(TransactionDate)", year);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items);
            return resp;
        }

        /// <summary>
        /// 统计每月签单次数
        /// </summary>
        /// <param name="year"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerCountReport(string year)
        {
            var sql = string.Format("select month(TransactionDate)  as month,CAST(count(*) as varchar(20))as Ydata from V_C_CustomerTransaction where year(TransactionDate) = {0}  group by month(TransactionDate)", year);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items);
            return resp;
        }

        #endregion

        #region 所有客户日统计
        /// <summary>
        /// 统计每日客户来访次数
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerLaifangDayReport(string year, string month)
        {
            var sql = string.Format("select day(YuefangVisitingTime)  as day,CAST(count(*) as varchar(20))as Ydata from V_C_CustomerYuefang where year(YuefangVisitingTime) = {0} and month(YuefangVisitingTime) = {1} group by day(YuefangVisitingTime)", year, month);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items, year, month);
            return resp;
        }

        /// <summary>
        /// 统计每日签单额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerMoneyDayReport(string year, string month)
        {
            var sql = string.Format("select day(TransactionDate)  as day,CAST(sum(TransactionPremium) as varchar(20))as Ydata from V_C_CustomerTransaction where year(TransactionDate) = {0} and  month(TransactionDate) = {1} group by day(TransactionDate)", year, month);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items, year, month);
            return resp;
        }

        /// <summary>
        /// 统计每日签单次数
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public MyResponseBase queryCustomerCountDayReport(string year, string month)
        {
            var sql = string.Format("select day(TransactionDate)  as day,CAST(count(*) as varchar(20))as Ydata from V_C_CustomerTransaction where year(TransactionDate) = {0} and  month(TransactionDate) = {1} group by day(TransactionDate)", year, month);
            var resp = Query16(sql, 2);
            resp.Items = FullItem(resp.Items,year,month);
            return resp;
        }

        #endregion

         #region 客户增删改报表
        /// <summary>
        /// 为客户增删改报表提供数据，按月份倒序排列(新增客户总数量 修改客户总数量 删除客户总数量)
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase CustomerRecordMonthReport()
        {
            var sql = "";

            sql = String.Format("delete C_Report where type=1");
            Query16(sql, 4);

            //新增客户数量
            //sql = "select cast(YEAR(CreateDate) as varchar(10))+'年'+cast(MONTH(CreateDate) as varchar(10))+'月' as 日期 ,COUNT(*) as 次数 from C_Customer  group by cast(YEAR(CreateDate) as varchar(10))+'年'+cast(MONTH(CreateDate) as varchar(10))+'月'";
            sql = "select YEAR(CreateDate) as year,MONTH(CreateDate) as month ,count(*) as 次数 from C_Customer group by YEAR(CreateDate),MONTH(CreateDate)";
            DataTable dtNew = Query16(sql, 0).DataTable; 

            //修改客户总数量
            //sql = "select cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月' as 日期 ,COUNT(*) as 次数 from C_Customer  group by cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月'";
            sql = "select YEAR(UpdateDate) as year,MONTH(UpdateDate) as month ,count(*) as 次数 from C_Customer where CustomerDataStatus = 0 group by YEAR(UpdateDate),MONTH(UpdateDate)";
            DataTable dtModify = Query16(sql, 0).DataTable; 

            //删除客户总数量
            //sql = "select cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月' as 日期 ,COUNT(*) as 次数 from C_Customer where CustomerDataStatus = 1 group by cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月'";
            sql = "select YEAR(UpdateDate) as year,MONTH(UpdateDate) as month ,count(*) as 次数 from C_Customer where CustomerDataStatus = 1 group by YEAR(UpdateDate),MONTH(UpdateDate)";
            DataTable dtDelete = Query16(sql, 0).DataTable;
            
            foreach(DataRow r in dtNew.Rows)
            {
                string y = r["year"].ToString();
                string m = r["month"].ToString();
                int data1 = int.Parse(r["次数"].ToString());
                int data2 = 0;
                int data3 = 0;
                DataRow[] drs2 = dtModify.Select("year="+y+" and month="+m);
                if(drs2.Length>0)
                {
                    data2 = int.Parse(drs2[0]["次数"].ToString());
                    dtModify.Rows.Remove(drs2[0]);
                }
                DataRow[] drs3 = dtDelete.Select("year="+y+" and month="+m);
                if(drs3.Length>0)
                {
                    data3 = int.Parse(drs3[0]["次数"].ToString());
                    dtDelete.Rows.Remove(drs3[0]);
                }

                sql = String.Format("insert into C_Report(YearMonth,data1,data2,data3,year,month,type) values('{0}',{1},{2},{3},{4},{5},1)",y+"年"+m+"月",data1,data2,data3,y,m);
                Query16(sql, 1);
            }
             foreach(DataRow r in dtModify.Rows)
            {
                string y = r["year"].ToString();
                string m = r["month"].ToString();
                int data2 = int.Parse(r["次数"].ToString());
                sql = String.Format("insert into C_Report(YearMonth,data1,data2,data3,year,month,type) values('{0}',{1},{2},{3},{4},{5},1)", y + "年" + m + "月", 0, data2, 0, y, m);
                Query16(sql, 1);
            }

            foreach(DataRow r in dtModify.Rows)
            {
                string y = r["year"].ToString();
                string m = r["month"].ToString();
                int data3 = int.Parse(r["次数"].ToString());
                sql = String.Format("insert into C_Report(YearMonth,data1,data2,data3,year,month,type) values('{0}',{1},{2},{3},{4},{5},1)", y + "年" + m + "月", 0, 0, data3, y, m);
                Query16(sql, 1);
            }
            sql = "select * from V_C_Report";
            MyResponseBase resp = Query16(sql, 2);
            return resp;
        }

        /// <summary>
        /// 为客户增删改明细提供数据，按日升序排列(新增客户总数量 修改客户总数量 删除客户总数量)
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public DataTable CustomerRecordDayReport()
        {
            string YearMonth = Item.YearMonth;           
            int ys = YearMonth.IndexOf('年');
            int ms = YearMonth.IndexOf('月');
            string year = YearMonth.Substring(0, ys);
            string month = YearMonth.Substring(ys+1, ms - ys -1);
            DataTable dtResult=new DataTable();
            var sql = "";

            sql = String.Format("delete C_Report where type=1");
            Query16(sql, 4);

            //新增客户数量
            //sql = "select cast(YEAR(CreateDate) as varchar(10))+'年'+cast(MONTH(CreateDate) as varchar(10))+'月' as 日期 ,COUNT(*) as 次数 from C_Customer  group by cast(YEAR(CreateDate) as varchar(10))+'年'+cast(MONTH(CreateDate) as varchar(10))+'月'";
            sql = String.Format("select DAY(CreateDate) as day, count(*) as 次数 from C_Customer where YEAR(CreateDate)={0} and MONTH(CreateDate)={1} group by DAY(CreateDate)",year,month);
            DataTable dtNew = Query16(sql, 0).DataTable;

            //修改客户总数量
            //sql = "select cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月' as 日期 ,COUNT(*) as 次数 from C_Customer  group by cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月'";
            sql = String.Format("select DAY(UpdateDate) as day, count(*) as 次数 from C_Customer where CustomerDataStatus = 0 and YEAR(CreateDate)={0} and MONTH(CreateDate)={1}  group by DAY(UpdateDate)", year, month);
            DataTable dtModify = Query16(sql, 0).DataTable;

            //删除客户总数量
            //sql = "select cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月' as 日期 ,COUNT(*) as 次数 from C_Customer where CustomerDataStatus = 1 group by cast(YEAR(UpdateDate) as varchar(10))+'年'+cast(MONTH(UpdateDate) as varchar(10))+'月'";
            sql = String.Format("select DAY(UpdateDate) as day,count(*) as 次数 from C_Customer where CustomerDataStatus = 1 and YEAR(CreateDate)={0} and MONTH(CreateDate)={1} group by DAY(UpdateDate),MONTH(UpdateDate)", year, month);
            DataTable dtDelete = Query16(sql, 0).DataTable;

            DataTable dt = new DataTable();
            dt.Columns.Add("日", typeof(int));
            dt.Columns.Add("data1", typeof(int));
            dt.Columns.Add("data2", typeof(int));
            dt.Columns.Add("data3", typeof(int));
            dt.Columns.Add("year", typeof(int));
            dt.Columns.Add("month", typeof(int));

            foreach (DataRow r in dtNew.Rows)
            {
                int d = int.Parse(r["day"].ToString());
                int data1 = int.Parse(r["次数"].ToString());
                int data2 = 0;
                int data3 = 0;
                DataRow[] drs2 = dtModify.Select("day=" +d);
                if (drs2.Length > 0)
                {
                    data2 = int.Parse(drs2[0]["次数"].ToString());
                    dtModify.Rows.Remove(drs2[0]);
                }
                DataRow[] drs3 = dtDelete.Select("day=" + d);
                if (drs3.Length > 0)
                {
                    data3 = int.Parse(drs3[0]["次数"].ToString());
                    dtDelete.Rows.Remove(drs3[0]);
                }
                dt.Rows.Add(d, data1, data2, data3, year, month);
                
            }
            foreach (DataRow r in dtModify.Rows)
            {
                int d = int.Parse(r["day"].ToString());               
                int data2 = int.Parse(r["次数"].ToString());
                int data3 = 0;
                DataRow[] drs3 = dtDelete.Select("day=" + d);
                if (drs3.Length > 0)
                {
                    data3 = int.Parse(drs3[0]["次数"].ToString());
                    dtDelete.Rows.Remove(drs3[0]);
                }
                dt.Rows.Add(d, 0, data2, data3, year, month);
            }

            foreach (DataRow r in dtDelete.Rows)
            {
                int d = int.Parse(r["day"].ToString());
                int data3 = int.Parse(r["次数"].ToString());
                dt.Rows.Add(d, 0, 0, 3, year, month);
            }
            DataView dv = dt.DefaultView;
            dv.Sort = "日";
            dtResult = dv.ToTable();

            return dtResult;
                
        }

        #endregion 客户增删改报表

        /// <summary>
        /// 为没有月的横坐标生成一个，并排序
        /// </summary>
        /// <param name="itemTemp"></param>
        /// <returns></returns>
        private List<SoftProjectAreaEntity> FullItem(List<SoftProjectAreaEntity> itemTemp)
        {
            var reportItems = itemTemp;
            List<int> months = new List<int>();
            foreach (var report in reportItems)
            {
                months.Add(report.month);
            }
            for (int i = 1; i <= 12; i++)
            {
                if (months.Contains(i) == false)
                {
                    var t = new SoftProjectAreaEntity();
                    t.month = i;
                    t.Ydata = "0";
                    reportItems.Add(t);
                }
            }
            var reportItemsTemp = reportItems.OrderBy(k => k.month).ToList<SoftProjectAreaEntity>();
            for (int i = 1; i <= 12; i++)
                reportItemsTemp[i - 1].Xdata = i.ToString() + "月";
            return reportItemsTemp;
        }

        /// <summary>
        /// 为没有日的横坐标生成一个，并排序
        /// </summary>
        /// <param name="itemTemp"></param>
        /// <returns></returns>
        private List<SoftProjectAreaEntity> FullItem(List<SoftProjectAreaEntity> itemTemp,string year,string month)
        {
            int days = 0;
            int m = int.Parse(month);
            if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)
                days = 31;
            else if (m == 2)
            {
                int y = int.Parse(year);
                if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
                    days = 29;
                else
                    days = 28;                 
            }
            else
                days = 30;

            var reportItems = itemTemp;
            List<int> months = new List<int>();
            foreach (var report in reportItems)
            {
                months.Add(report.day);
            }
            for (int i = 1; i <= days; i++)
            {
                if (months.Contains(i) == false)
                {
                    var t = new SoftProjectAreaEntity();
                    t.day = i;
                    t.Ydata = "0";
                    reportItems.Add(t);
                }
            }
            var reportItemsTemp = reportItems.OrderBy(k => k.day).ToList<SoftProjectAreaEntity>();
            for (int i = 1; i <= reportItemsTemp.Count; i++)
                reportItemsTemp[i - 1].Xdata = i.ToString() + "日";
            return reportItemsTemp;
        }

        public int GetCustomerCount(string CustomerName)
        {
            var sql = string.Format("select count(*) from V_C_Customer where  CustomerName = '{0}'", CustomerName);
            var resp = Query16(sql, 1);
            return (int)resp.Obj;
        }

        public int GetCustomerIDByName(string CustomerName)
        {
            var sql = string.Format("select C_CustomerID from V_C_Customer where  CustomerName = '{0}'", CustomerName);
            var resp = Query16(sql, 1);
            return (int)(resp.Obj);
        }

    }
}