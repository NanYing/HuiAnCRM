﻿//弹出短信发送窗口
function openMessageWindow() {
    var phone = "";
    var tr = $("#tbList tr");
    var checkeds = tr.find(".jq-checkall-item:checked");
    checkeds.each(function (i) {
        var tr = $(this).parent().parent();
        var CustomerPhone = tr.data("customerphone");
        if (CustomerPhone != "") {
            if (phone != "") phone += ",";
            phone += CustomerPhone;
        }
    });
    if (phone == "" || phone.length == 0)
        alert("请选择要发送短信的客户");
    else {
        $("#sendPhoneNumber").val(phone);
        $("#sendMessageText").val("");
        $("#dialog").dialog({ modal: true });
    }
}
//发送短信
function sendMessage() {
    var sendPhoneNumber = $("#sendPhoneNumber").val();
    var sendMessageText = $("#sendMessageText").val();
    var url = "http://utf8.sms.webchinese.cn/?Uid=huian&Key=aeb65fc83ed2458a1d85&smsMob=" + sendPhoneNumber + "&smsText=" + sendMessageText;
    $.get(url, function (result) {
        alert("发送成功:" + result + "条");
    });
    alert("短信已提交，待后台发送");
    $('#dialog').dialog('close');

}

//导出客户数据到excel
function bsuExportExcel() {
    var id = "";
    var tr = $("#tbList tr");
    var checkeds = tr.find(".jq-checkall-item:checked");
    checkeds.each(function (i) {
        var tr = $(this).parent().parent();
        var customerid = tr.data("c_customerid");
        if (customerid != "") {
            if (id != "") id += ",";
            id += customerid;
        }
    });
    if (id == "" || id.length == 0)
        alert("请选择要导出的客户");
    else {
        var url = "ExportToExcel?C_CustomerID=" + id;
        //如果页面中没有用于下载iframe，增加iframe到页面中
        if ($('#downloadcsv').length <= 0)
            $('body').append("<iframe id=\"downloadcsv\" style=\"display:none\"></iframe>");
        $('#downloadcsv').attr('src', url);
    }
}
//分配业务员-选择业务员
function setSalesman() {
    var url = "getPreUser";

    $.get(url, function (result) {
        $("#ServiceUserID").empty();
        $(result.Data.Items).each(function (i, val) {
            $("#ServiceUserID").append("<option value=\"" + result.Data.Items[i].Pre_UserID + "\">" + result.Data.Items[i].UserName + "</option>");
        })
        var id = "";
        var tr = $("#tbList tr");
        var checkeds = tr.find(".jq-checkall-item:checked");
        checkeds.each(function (i) {
            var tr = $(this).parent().parent();
            var customerid = tr.data("c_customerid");
            if (customerid != "") {
                if (id != "") id += ",";
                id += customerid;
            }
        });
        if (id == "" || id.length == 0)
            alert("请选择要分配的客户");
        else {
            $("#cids").val(id);
            $("#SalesmanSdialog").dialog({ modal: true });
        }
    });
}
//分配业务员-分配业务员
function setSalesmanSave() {
    var ServiceUserID = $("#ServiceUserID").val();
    var cids = $("#cids").val();
    var url = "setPreUser?C_CustomerID=" + cids + "&ServiceUserID=" + ServiceUserID;
    $.get(url, function (result) {
        alert("分配成功!");
        window.location.reload();
    });
    $('#dialog').dialog('close');

}
//弹出短信窗口




//发送邮件-弹出窗口
function openMailWindow() {
    debugger;
    var email = "";
    var tr = $("#tbList tr");
    var checkeds = tr.find(".jq-checkall-item:checked");
    //debugger;
    checkeds.each(function (i) {
        var tr = $(this).parent().parent();
        var CustomerEmail = tr.data("customeremail");
        if (CustomerEmail != "") {
            if (email != "") email += ",";
            email += CustomerEmail;
        }
    });
    if (email == "" || email.length == 0)
        alert("请选择要发送邮件的客户");
    else {
        $("#toMail").val(email);
        //var file = $("#upfile")
        //file.after(file.clone().val(""));
        //file.remove();//发送邮件之前将文件里面的内容清空
        $("#subject").val("");
        $("#maildialog").dialog({
            modal: true, height: "600", //高度
            width: "630" //宽度 
        });
        UE.getEditor("myEditor").setContent("");
    }
}
//发送邮件-清空文本框
function clearAll() {
    UE.getEditor("myEditor").setContent("");
    $("#subject").val("");
}
//发送邮件-发送
function sendMail() {
    var toMail = $("#toMail").val();
    var subject = $("#subject").val();
    var content = UE.getEditor("myEditor").getContentTxt();
    if ($.trim(subject) == "" || $.trim(subject).length == 0) {
        alert("主题不能为空！");
        return;
    }
    debugger;
    if ($.trim(content) == "" || $.trim(content).length == 0) {
        alert("内容不能为空！");
        return;
    }
    Framework.Ajax.PostJson("toMail=" + toMail + "&subject=" + subject + "&content=" + content, "/CustomerAreas/C_Customer/SendMail", function (results) {
        if (results.Data == "success") {
            alert("邮件发送成功！");
            //发送完毕后关闭dialog
            $("#maildialog").dialog('close');
        } else {
            alert("邮件发送失败，请检查发件人邮件服务器配置！");
        }
    });

}
//删除客户
function deleteCustomer() {
    var id = "";
    var tr = $("#tbList tr");
    var checkeds = tr.find(".jq-checkall-item:checked");
    checkeds.each(function (i) {
        var tr = $(this).parent().parent();
        var customerid = tr.data("c_customerid");
        if (customerid != "") {
            if (id != "") id += ",";
            id += customerid;
        }
    });
    if (id == "" || id.length == 0)
        alert("请选择要删除的客户");
    else {
        if (confirm("确认要删除这些用户吗？")) {
            var url = "Delete?C_CustomerID=" + id;
            $.get(url, function (result) {
                $.each(result.Data, function (idx, item) {
                    var url = "http://utf8.sms.webchinese.cn/?Uid=huian&Key=aeb65fc83ed2458a1d85&smsMob=" + idx + "&smsText=" + item;
                       $.get(url, function (result) {
                    });
                });
                alert("删除成功");
                window.location = "Index";

            });
        }
    }
}