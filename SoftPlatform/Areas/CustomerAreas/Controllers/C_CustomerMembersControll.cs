﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：C_CustomerMembers(客户成员信息)
    /// </summary>
    public class C_CustomerMembersController : BaseController
    {
        public C_CustomerMembersController()
        {
        }

        /// <summary>
        /// 客户成员信息--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            if (!domain.Querys.QueryDicts.ContainsKey("C_CustomerID___equal"))
            {
                if (domain.Item.C_CustomerID == null)
                    throw new Exception("主键不能为空");
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "C_CustomerID___equal", Value = domain.Item.C_CustomerID.ToString() });
            }

            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            SoftProjectAreaEntityDomain.initCustomerTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Index";

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }
        /// <summary>
        /// 客户成员信息--列表查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult List(SoftProjectAreaEntityDomain domain)
        {
            if (!domain.Querys.QueryDicts.ContainsKey("C_CustomerID___equal"))
            {
                if (domain.Item.C_CustomerID == null)
                    throw new Exception("主键不能为空");
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "C_CustomerID___equal", Value = domain.Item.C_CustomerID.ToString() });
            }

            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.List";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            SoftProjectAreaEntityDomain.initCustomerTabView(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.List";

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户成员信息--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            resp.Item.C_CustomerID = domain.Item.C_CustomerID;

            resp.Item.MemberInputTime = DateTime.Now;
            resp.Item.MemberInputMen = LoginInfo.UserName;
            resp.Item.Pre_UserID = LoginInfo.Pre_UserID;
           // SoftProjectAreaEntityDomain.initTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户成员信息--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.AddSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 客户成员信息--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
          //  var resp = domain.Default();u
            var resp = domain.ByID();
            domain.Item.C_CustomerID = resp.Item.C_CustomerID;
          //  SoftProjectAreaEntityDomain.initTab(this, resp, domain);

            
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户成员信息--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 客户成员信息--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            domain.Item.C_CustomerID = resp.Item.C_CustomerID;
          //  SoftProjectAreaEntityDomain.initTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Detail";
            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户成员信息--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail2(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Detail2";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            domain.Item.C_CustomerID = resp.Item.C_CustomerID;
           // SoftProjectAreaEntityDomain.initTabView(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerMembers.Detail2";
            resp.FunNameEn = "Detail2";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }
        /*

        /// <summary>
        /// 客户成员信息--相关人员
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
//[HttpPost]
[HttpGet]
public ActionResult ZZZ(SoftProjectAreaEntityDomain domain)
{
    ModularOrFunCode = "CustomerAreas.C_CustomerMembers.ZZZ";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.MMM();

            resp.FunNameEn = "ZZZ";
    ModularOrFunCode = "CustomerAreas.C_CustomerMembers.ZZZ";
            if (Request.IsAjaxRequest())
                 resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
}*/

    }
}