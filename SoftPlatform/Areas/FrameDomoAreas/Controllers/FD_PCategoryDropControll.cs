﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：FD_PCategoryDrop(商品类别下拉框架演示)
    /// </summary>
    public class FD_PCategoryDropController : BaseController
    {
        public FD_PCategoryDropController()
        {
        }

        /// <summary>
        /// 商品类别下拉框架演示--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            //domain.Querys.Add(new Query { QuryType = 0, FieldName = "Pre_CompanyID___equal", Value = LoginInfo.Pre_CompanyID.ToString() });

            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 商品类别下拉框架演示--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            #region 初始化代码
            #endregion
            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 商品类别下拉框架演示--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.AddSave();
            //domain.FD_PCategoryDrop_AddCache();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 商品类别下拉框架演示--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 商品类别下拉框架演示--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.EditSave();
            //domain.FD_PCategoryDrop_UpdateCache();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 商品类别下拉框架演示--Row
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Row(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            resp.Items.Add(resp.Item);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View("Rows", resp);
        }

    }
}
