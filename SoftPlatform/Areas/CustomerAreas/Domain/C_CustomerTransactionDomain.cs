﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using SoftPlatform.Controllers;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：C_CustomerTransaction(成交记录)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        public MyResponseBase updateBargainCount(int? C_CustomerID)
        {
            string sql = string.Format("update C_Customer set BargainCount=BargainCount + 1 where C_CustomerID={0}", C_CustomerID);
            var resp = Query16(sql);
            return resp;
        }

       
    }
}