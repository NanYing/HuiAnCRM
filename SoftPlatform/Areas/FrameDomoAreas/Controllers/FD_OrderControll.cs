﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.CellModel;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：FD_Order(订单管理)
    /// </summary>
    public class FD_OrderController : BaseController
    {
        public FD_OrderController()
        {
        }

        /// <summary>
        /// 订单管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            //if (!domain.Querys.QueryDicts.ContainsKey("___equal"))
            //{
            //    if (domain.Item. == null)
            //throw new Exception("主键不能为空");
            //domain.Querys.Add(new Query { QuryType = 0, FieldName = "___equal", Value = domain.Item..ToString() });
            //}

            ModularOrFunCode = "FrameDomoAreas.FD_Order.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 订单管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();

            resp.Item.FDOrderDate = DateTime.Now;
            #region 订单明细

            resp.EditAction = new SoftProjectAreaEntity
            {
                ControllName = "FD_OrderDetail",		//子控制器，例如：订单明细Controll
                ActionNameEn = "IndexEdit",			//子控制器的Action，例如：订单明细Controll的IndexEdit
                ActionFieldNames = "FD_OrderID",		//主表主键ID：FD_OrderID，只有在Panle、PanleHead才有用
                FD_OrderID = resp.Item.FD_OrderID	//主表主键ID值：
            };
            #endregion

            ModularOrFunCode = "FrameDomoAreas.FD_Order.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 订单管理--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            ModularOrFunCode = "FrameDomoAreas.FD_Order.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.FD_Order_AddSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 订单管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Order.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            #region 订单明细

            resp.EditAction = new SoftProjectAreaEntity
            {
                ControllName = "FD_OrderDetail",		//子控制器，例如：订单明细Controll
                ActionNameEn = "IndexEdit",			//子控制器的Action，例如：订单明细Controll的IndexEdit
                ActionFieldNames = "FD_OrderID",		//主表主键ID：FD_OrderID，只有在Panle、PanleHead才有用
                FD_OrderID = resp.Item.FD_OrderID	//主表主键ID值：
            };
            #endregion

            ModularOrFunCode = "FrameDomoAreas.FD_Order.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 订单管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Order.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.FD_Order_EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 订单管理--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Order.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

    }
}
