﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Core
{
    public static class StringExt
    {
        /// <summary>
        /// 字符串转换为日期类型
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static DateTime ToDate(this string str)
        {
            DateTime dtime = new DateTime();
            DateTime.TryParse(str, out dtime);
            return dtime;
        }

        /// <summary>
        /// 字符串转换为日期类型
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static DateTime? ToDateNull(this string str)
        {
            if (str == "" || str == null)
                return null;
            DateTime dtime = new DateTime();
            DateTime.TryParse(str, out dtime);
            return dtime;
        }

        /// <summary>
        /// 字符串转换为Decimal类型
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Decimal ToDecimal(this string str)
        {
            Decimal dec = new Decimal();
            Decimal.TryParse(str, out dec);
            return dec;
        }

        /// <summary>
        /// 字符串转换为Decimal类型
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Decimal? ToDecimalNull(this string str)
        {
            if (str == "" || str == null)
                return null;
            Decimal dec = new Decimal();
            Decimal.TryParse(str, out dec);
            return dec;
        }

        /// <summary>
        /// 字符串转换为Byte[]类型
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this string str)
        {
            byte[] byteArray = System.Text.Encoding.Default.GetBytes(str);
            return byteArray;
        }

        /// <summary>
        /// 字符串转换为ASCII的Byte[]类型
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] ToBytesASCII(this string str)
        {
            byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(str);
            return byteArray;
        }


        /// <summary>
        /// 转换为16进制字符串
        /// </summary>
        /// <param name="InString"></param>
        /// <returns></returns>
        public static byte[] ToByteHex(this string InString)
        {
            string[] ByteStrings;
            ByteStrings = InString.Split(" ".ToCharArray());
            byte[] ByteOut;
            ByteOut = new byte[ByteStrings.Length - 1];
            for (int i = 0; i == ByteStrings.Length - 1; i++)
            {
                ByteOut[i] = Convert.ToByte(("0x" + ByteStrings[i]));
            }
            return ByteOut;
        }

        //private char[] HexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'a', 'b', 'c', 'd', 'e', 'f' };
        //private bool CharInArray(char aChar, char[] charArray)
        //{
        //    return (Array.Exists<char>(charArray, delegate(char a) { return a == aChar; }));
        //}

        ///// <summary>
        ///// 十六进制字符串转换字节数组
        ///// </summary>
        ///// <param name="s"></param>
        ///// <returns></returns>
        //public byte[] HexStringToByteArray(string s)
        //{
        //    // s = s.Replace(" ", "");
        //    StringBuilder sb = new StringBuilder(s.Length);
        //    foreach (char aChar in s)
        //    {
        //        if (CharInArray(aChar, HexDigits))
        //            sb.Append(aChar);
        //    }
        //    s = sb.ToString();
        //    int bufferlength;
        //    if ((s.Length % 2) == 1)
        //        bufferlength = s.Length / 2 + 1;
        //    else bufferlength = s.Length / 2;
        //    byte[] buffer = new byte[bufferlength];
        //    for (int i = 0; i < bufferlength - 1; i++)
        //        buffer[i] = (byte)Convert.ToByte(s.Substring(2 * i, 2), 16);
        //    if (bufferlength > 0)
        //        buffer[bufferlength - 1] = (byte)Convert.ToByte(s.Substring(2 * (bufferlength - 1), (s.Length % 2 == 1 ? 1 : 2)), 16);
        //    return buffer;
        //}

    }
}
