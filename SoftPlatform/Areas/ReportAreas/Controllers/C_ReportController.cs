﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：C_Report(报表分析)
    /// </summary>
    public class C_ReportController : BaseController
    {
        public C_ReportController()
        {
        }

        #region 单客户每月报表
        /// <summary>
        /// 单客户来访年统计表（本页用柱状图表示单客户的每月来访次数和签单额）
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>

        public ActionResult CustomerReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.CustomerReport";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();
            if (Request.IsAjaxRequest())
                 resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }


        public HJsonResult queryCustomerReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string CustomerID = Request.Form["CustomerID"].ToString();
            var resp = domain.queryCustomerReport(year, CustomerID);
            return new HJsonResult(new { Data = resp });
        }

        public HJsonResult queryCustomerMoneyReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerMoneyReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string CustomerID = Request.Form["CustomerID"].ToString();
            var resp = domain.queryCustomerMoneyReport(year, CustomerID);
            return new HJsonResult(new { Data = resp });
        }
        #endregion

        #region 业务员每月报表
        /// <summary>
        /// 业务员来访年统计表（本页用柱状图来表示某个业务员的每月签单额、客户来访次数、签单次数）
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult UserReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.UserReport";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();
            if (Request.IsAjaxRequest())
                 resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 统计业务员的每月客户来访次数
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryUserLaifangReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string userID = Request.Form["userID"].ToString();
            var resp = domain.queryUserLaifangReport(year, userID);
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 统计业务员的每月签单额
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryUserMoneyReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerMoneyReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string userID = Request.Form["userID"].ToString();
            var resp = domain.queryUserMoneyReport(year, userID);
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 统计每月每月签单次数
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryUserCountReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string userID = Request.Form["userID"].ToString();
            var resp = domain.queryUserCountReport(year, userID);
            return new HJsonResult(new { Data = resp });
        }
        #endregion

        #region 所有客户每月报表
        /// <summary>
        ///来访年统计表（本页用柱状图来表示所有客户的每月签单额、客户来访次数、签单次数）
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult CustomerAllYearReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.CustomerAllYearReport";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();
            if (Request.IsAjaxRequest())
                resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 统计每月客户来访次数
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryCustomerLaifangReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerLaifangReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            var resp = domain.queryCustomerLaifangReport(year);
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 统计每月签单额
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryCustomerMoneyYearReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerMoneyYearReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            var resp = domain.queryCustomerMoneyYearReport(year);
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 统计每月签单次数
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryCustomerCountReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerCountReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            var resp = domain.queryCustomerCountReport(year);
            return new HJsonResult(new { Data = resp });
        }
        #endregion

        #region 所有客户每日报表
        /// <summary>
        ///来访年统计表（本页用柱状图来表示所有客户的每月签单额、客户来访次数、签单次数）
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult CustomerAllDayReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.CustomerAllDayReport";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();
            if (Request.IsAjaxRequest())
                resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 统计每日客户来访次数
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryCustomerLaifangDayReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerLaifangDayReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string month = Request.Form["month"].ToString();
            var resp = domain.queryCustomerLaifangDayReport(year,month);
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 统计每日签单额
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryCustomerMoneyDayReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerMoneyDayReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string month = Request.Form["month"].ToString();
            var resp = domain.queryCustomerMoneyDayReport(year,month);
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 统计每日签单次数
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult queryCustomerCountDayReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.queryCustomerCountDayReport";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string year = Request.Form["year"].ToString();
            string month = Request.Form["month"].ToString();
            var resp = domain.queryCustomerCountDayReport(year, month);
            return new HJsonResult(new { Data = resp });
        }
        #endregion

        #region 客户增删改报表 
        /// <summary>
        /// 按月份倒序排列(新增客户总数量 修改客户总数量 删除客户总数量)
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult CustomerRecordMonthReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.CustomerRecordMonthReport";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();
            if (Request.IsAjaxRequest())
                resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.Items = domain.CustomerRecordMonthReport().Items;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        ///显示整月每天(新增客户总数量 修改客户总数量 删除客户总数量)
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult CustomerRecordDayReport(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.CustomerRecordDayReport";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();
            if (Request.IsAjaxRequest())
                resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.ViewContextName = Design_ModularOrFun.PartialView;

            resp.DataTable = domain.CustomerRecordDayReport();
            return View(Design_ModularOrFun.MainView, resp);
        }

        #endregion
        #region 功能控制

        /// <summary>
        /// 判断姓名为customerName的客户是否唯一
        /// </summary>
        /// <param name="CustomerName"></param>
        /// <returns></returns>
        public HJsonResult isOnlyOne(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ReportAreas.C_Report.isOnlyOne";
            domain.Design_ModularOrFun = Design_ModularOrFun;
            string CustomerName = Request.Form["CustomerName"].ToString();
            int CustomerID=-1;
            int nCount = domain.GetCustomerCount(CustomerName);
            if (nCount==1)
            {
                CustomerID = domain.GetCustomerIDByName(CustomerName);
            }
            else
            {
                
            }
            return new HJsonResult(new { Data = nCount, ID = CustomerID.ToString() });
        }
        #endregion 
    }
}
