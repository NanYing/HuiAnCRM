﻿/// <reference path='jquery-1.9.1.intellisense.js' />

//Import('PollingRuns');

//(function (PollingRuns) {

//    PollingRuns.Init = function () {
//        var connectionUrl = "/ProjectAreas/P_Monitor/Polling";
//        ///发送方式 
//        var method = "POST";
//        ///事件载体
//        //event_host: $("body"),
//        ///连接失败时重连接时间
//        var period = 1000 * 20;
//        ///连接超时时间
//        var timeOut = 180 * 1000;
//        var v = 0;
//        ///连接ID
//        var id = "";
//        var No = $("#Item_ProjectCode").val();
//        var PushFrequency = $("#Item_PushFrequency").val();
//        var PageNameCn = $("#Item_PageNameCn").val();
//        var UserName = $("#Item_UserName").val();
//        var error_num = 0;

//        var Reconnect = function () {
//            v++;
//            $.ajax({
//                url: connectionUrl,
//                type: method,
//                data: {
//                    No: No,
//                    PushFrequency: PushFrequency,
//                    PageNameCn: PageNameCn,
//                    UserName: UserName,
//                    id: id,
//                    v: v
//                },
//                dataType: "json",
//                timeout: timeOut,
//                success: function (json) {
//                    //alert(json.id);
//                    id = json.id;
//                    ///版本号相同才回发服务器
//                    if (json.v == v)
//                        Reconnect();
//                    ///无消息返回时不处理
//                    if (json.result == "-1")
//                        return;
//                    //alert(json.datas.data.content);
//                    $.each(json.datas, function (i, ajaxData) {
//                        ajaxData.data.type = ajaxData.t;
//                        //alert(ajaxData.data.content);
//                        if (ajaxData.t != "1")
//                            return;

//                        var UIDatas = ajaxData.data.UIDatas;
//                        if (!Framework.Tool.isUndefined(UIDatas)) {
//                            var tempp = $.ENumberable.From(UIDatas);
//                            //alert($(".uidisp").length);
//                            $(".uidisp").each(function (i) {
//                                var dom = $(this);
//                                var address = dom.data("address");
//                                var disptype = dom.data("disptype");
//                                var dataformat = dom.data("dataformat");

//                                var items = tempp.Where(function (x) {
//                                    return (x.Address == address)
//                                });
//                                if (items.Count() > 0) {
//                                    var item = items.First();
//                                    if (disptype == "s")//状态显示
//                                    {
//                                        //"停止=0.运行=1，故障=2
//                                        dom.removeClass("redcircle");
//                                        dom.removeClass("greencircle");
//                                        if (item.Value == "1")
//                                            dom.addClass("greencircle");
//                                        else if (item.Value == "2")
//                                            dom.addClass("redcircle");
//                                    }
//                                    else {
//                                        dom.html(item.Value);
//                                    }
//                                }
//                            });
//                        }
//                        //data-address="10" data-disptype="s"
//                        $("#new_msg").html($("<p>" + ajaxData.data.content + "</p>"));
//                        //polling.event_host.triggerHandler("sys_msg", [ajaxData.data]);
//                    });
//                }, ///出错时重连
//                error: function (errorMess) {
//                    //alert("error111");
//                    if (error_num < 5) {
//                        setTimeout(Reconnect, 1000 * 2);
//                        error_num++;
//                        return;
//                    }
//                    //alert("error222");
//                    ///20秒后重新连接
//                    setTimeout(Reconnect, period);
//                }, ///释放资源
//                complete: function (XHR, TS) { XHR = null }
//            });
//        }

//        Reconnect();
//    };
//    return PollingRuns;
//}(PollingRuns));


$(document).ready(
	function () {
	    var connectionUrl = "/ProjectAreas/P_Monitor/Polling";
	    ///发送方式 
	    var method = "POST";
	    ///事件载体
	    //event_host: $("body"),
	    ///连接失败时重连接时间
	    var period = 1000 * 20;
	    ///连接超时时间
	    var timeOut = 180 * 1000;
	    var v = 0;
	    ///连接ID
	    var id = "";
	    var No = $("#Item_ProjectSerial").val();
	    //var PushFrequency = $("#Item_PushFrequency").val();
	    var PageNameCn = $("#Item_PageNameCn").val();
	    var UserName = $("#Item_UserName").val();
	    var error_num = 0;

	    var Reconnect = function () {
	        v++;
	        $.ajax({
	            url: connectionUrl,
	            type: method,
	            data: {
	                ProjectSerial: No,
	                //PushFrequency: PushFrequency,
	                PageNameCn: PageNameCn,
	                UserName: UserName,
	                id: id,
	                v: v
	            },
	            dataType: "json",
	            timeout: timeOut,
	            success: function (json) {
	                //alert(json.id);
	                id = json.id;
	                ///版本号相同才回发服务器
	                if (json.v == v)
	                    Reconnect();
	                ///无消息返回时不处理
	                if (json.result == "-1")
	                    return;
	                //alert(json.datas.data.content);
	                $.each(json.datas, function (i, ajaxData) {
	                    ajaxData.data.type = ajaxData.t;
	                    //alert(ajaxData.data.content);
	                    if (ajaxData.t != "1")
	                        return;

	                    var UIDatas = ajaxData.data;//.UIDatas;
	                    var ss = typeof UIDatas === "undefined";
	                    if (!ss) {
	                        var tempp = $.ENumberable.From(UIDatas);
	                        //alert($(".uidisp").length);
	                        $(".uidisp").each(function (i) {
	                            var dom = $(this);
	                            var B_GshpSystemGatherDataItemID = dom.data("b_gshpsystemgatherdataitemid");
	                            var monitorpagedispmodeid = dom.data("monitorpagedispmodeid");
	                            var dataformat = dom.data("dataformat");

	                            //            <div class="drag" style="text-align: center; left:317px; top: 401px;">
	                            //    <div class="redcircle uidisp" data-p_gatherdataconfigid="5" data-monitorpagedispmodeid="1"></div>

	                            //        <div style="display:none">2#空调泵运行信号</div>
	                            //</div>

	                            var items = tempp.Where(function (x) {
	                                return (x.B_GshpSystemGatherDataItemID == B_GshpSystemGatherDataItemID)
	                            });
	                            if (items.Count() > 0) {
	                                var item = items.First();
	                                if (monitorpagedispmodeid == "1")//状态显示
	                                {
	                                    //"停止=0.运行=1，故障=2
	                                    dom.removeClass("redcircle");
	                                    dom.removeClass("greencircle");
	                                    if (item.Value == "1")
	                                        dom.addClass("greencircle");
	                                    else if (item.Value == "2")
	                                        dom.addClass("redcircle");
	                                }
	                                else {
	                                    dom.html(item.Value);
	                                }
	                            }
	                        });
	                    }
	                    //data-address="10" data-disptype="s"
	                    $("#new_msg").html($("<p>" + ajaxData.data.content + "</p>"));
	                    //polling.event_host.triggerHandler("sys_msg", [ajaxData.data]);
	                });
	            }, ///出错时重连
	            error: function (errorMess) {
	                //alert("error111");
	                if (error_num < 5) {
	                    setTimeout(Reconnect, 1000 * 2);
	                    error_num++;
	                    return;
	                }
	                //alert("error222");
	                ///20秒后重新连接
	                setTimeout(Reconnect, period);
	            }, ///释放资源
	            complete: function (XHR, TS) { XHR = null }
	        });
	    }

	    Reconnect();
	}
);

