﻿
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

//namespace Framework.Web.Mvc
namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：Pre_Role(角色管理)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_Role_AddSave()
        {
            var resp = new MyResponseBase();

            resp = ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                resp = AddSave();
                Item.Pre_RoleID = resp.Item.Pre_RoleID;
                Pre_RolePremSet_AddUpdateDeleteSaves();
            }));
            Pre_Role_AddCache();//更新缓存
            return resp;
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_Role_EditSave()
        {
            var resp = new MyResponseBase();

            resp = ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                resp = EditSave();
                Pre_RolePremSet_AddUpdateDeleteSaves();
            }));
            Pre_Role_UpdateCache();//更新缓存
            return resp;
        }

        #region 缓存、界面元素

        public void Pre_Role_AddCache()
        {
            #region 添加：角色缓存

            ModularOrFunCode = "AuthorizationAreas.Pre_Role.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.Pre_Roles.Add(resp.Item);

            #endregion
        }

        public void Pre_Role_UpdateCache()
        {
            #region (3)根据ID查询，替换

            ModularOrFunCode = "AuthorizationAreas.Pre_Role.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var Pre_Role = SoftProjectAreaEntityDomain.Pre_Roles.Where(p => p.Pre_RoleID == Item.Pre_RoleID).FirstOrDefault();

            SoftProjectAreaEntityDomain.Pre_Roles.Remove(Pre_Role);
            SoftProjectAreaEntityDomain.Pre_Roles.Add(resp.Item);

            #endregion
        }

        /// <summary>
        /// 生成树或下拉树，缓存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_Role_GetAll()
        {
            string sql = "SELECT * FROM V_Pre_Role ";
            var resp = Query16(sql, 2);
            return resp;
        }

        #region 角色

        static List<SoftProjectAreaEntity> _Pre_Roles = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> Pre_Roles
        {
            get
            {
                if (_Pre_Roles.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _Pre_Roles = domain.Pre_Role_GetAll().Items;
                }
                return _Pre_Roles;
            }
        }

        public static void Pre_Roles_Clear()
        {
            _Pre_Roles = new List<SoftProjectAreaEntity>();
        }

        #endregion

        public static string QueryHtmlDropDownList_Pre_RoleID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            //var Pre_Roles = ProjectCache.Caches["Pre_RoleID"];
            var Pre_RolesTemp = Pre_Roles.Where(p => p.LoginCategoryID == item.LoginCategoryID);
            var str = HtmlHelpers.DropDownList(null, "Pre_RoleID___equal", Pre_RolesTemp, "Pre_RoleID", "RoleName", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        public static string HtmlDropDownLiss_Pre_RoleID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            //必须知道是公司、企业、顾客功能，从而进行过滤
            var Pre_RolesTemp = Pre_Roles.Where(p => p.LoginCategoryID == item.LoginCategoryID);
            var str = HtmlHelpers.DropDownList(null, "Item.Pre_RoleID", Pre_RolesTemp, "Pre_RoleID", "RoleName", val, "");
            return str.ToString();
        }

        #endregion
    }
}
