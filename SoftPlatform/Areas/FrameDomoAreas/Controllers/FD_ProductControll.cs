﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：FD_Product(框架演示商品管理)
    /// </summary>
    public class FD_ProductController : BaseController
    {
        public FD_ProductController()
        {
        }

        /// <summary>
        /// 框架演示商品管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            //if (!domain.Querys.QueryDicts.ContainsKey("___equal"))
            //{
            //    if (domain.Item. == null)
            //throw new Exception("主键不能为空");
            //domain.Querys.Add(new Query { QuryType = 0, FieldName = "___equal", Value = domain.Item..ToString() });
            //}

           ModularOrFunCode = "FrameDomoAreas.FD_Product.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 框架演示商品管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();

            #region 初始化
            resp.Item.FDAddPersonID = LoginInfo.Pre_UserID;
            resp.Item.FDAddPersonName = LoginInfo.UserName;
            resp.Item.FD_ProductAttachmentGuid = Guid.NewGuid().ToString();
            #endregion

            ModularOrFunCode = "FrameDomoAreas.FD_Product.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 框架演示商品管理--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            domain.Item.FDProductContext = Server.UrlDecode(domain.Item.FDProductContext);
            ModularOrFunCode = "FrameDomoAreas.FD_Product.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.AddSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 框架演示商品管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Product.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            ModularOrFunCode = "FrameDomoAreas.FD_Product.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 框架演示商品管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            domain.Item.FDProductContext = Server.UrlDecode(domain.Item.FDProductContext);
            ModularOrFunCode = "FrameDomoAreas.FD_Product.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 框架演示商品管理--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Product.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        public JsonResult AutoCompleteProduct(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Product.Index";
            domain.ModularOrFunCode = ModularOrFunCode;

            domain.Querys.Add(new Query { QuryType = 0, FieldName = "FDProductNo__FDProductName___like", Value = domain.Item.FDProductNo__FDProductName });
            domain.PageQueryBase.PageSize = 10000;
            var resp = domain.QueryIndex();
            List<AutocompleteItem> AutocompleteItems = new List<AutocompleteItem>();
            foreach (var item in resp.Items)
            {
                AutocompleteItems.Add(new AutocompleteItem
                {
                    label = "【" + item.FDProductNo + "】" + "【" + item.FDProductName + "】" + "【" + item.FDSpecifications + "】",
                    value = "【" + item.FDProductNo + "】" + "【" + item.FDProductName + "】" + "【" + item.FDSpecifications + "】",
                    id = item.FD_ProductID.ToString()
                });
            }
            return Json(AutocompleteItems, JsonRequestBehavior.AllowGet);
        }

        #region Panle部分视图

        /// <summary>
        /// 框架演示商品管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexPanle(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Product.IndexPanle";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }


        /// <summary>
        /// 框架演示商品管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult AddPanle(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();

            #region 初始化
            resp.Item.FDAddPersonID = LoginInfo.Pre_UserID;
            resp.Item.FDAddPersonName = LoginInfo.UserName;
            resp.Item.FD_ProductAttachmentGuid = Guid.NewGuid().ToString();
            #endregion

            ModularOrFunCode = "FrameDomoAreas.FD_Product.AddPanle";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 框架演示商品管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult EditPanle(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Product.EditPanle";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            ModularOrFunCode = "FrameDomoAreas.FD_Product.EditPanle";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        #endregion

        #region PanleHead部分视图

        /// <summary>
        /// 框架演示商品管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexPanleHead(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Product.IndexPanleHead";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }


        /// <summary>
        /// 框架演示商品管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult AddPanleHead(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();

            #region 初始化
            resp.Item.FDAddPersonID = LoginInfo.Pre_UserID;
            resp.Item.FDAddPersonName = LoginInfo.UserName;
            resp.Item.FD_ProductAttachmentGuid = Guid.NewGuid().ToString();
            #endregion

            ModularOrFunCode = "FrameDomoAreas.FD_Product.AddPanleHead";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 框架演示商品管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult EditPanleHead(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "FrameDomoAreas.FD_Product.EditPanleHead";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            ModularOrFunCode = "FrameDomoAreas.FD_Product.EditPanleHead";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        #endregion

    }
}
