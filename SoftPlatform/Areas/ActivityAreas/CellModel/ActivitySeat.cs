
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：ActivitySeat(会议座位管理)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 活动座位编号
        /// </summary>
        public  int?  ActivitySeatID{get;set;}

        /// <summary>
        /// 活动编号
        /// </summary>
        //public  int?  ActivityID{get;set;}

        /// <summary>
        /// 客户编号
        /// </summary>
        //public  int?  C_CustomerID{get;set;}

        /// <summary>
        /// 座位
        /// </summary>
        public  string  Seat{get;set;}

        public SoftProjectAreaEntity ActivitySeat { get; set; }
        public List<SoftProjectAreaEntity> ActivitySeats { get; set; }
    }
}
