﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

//namespace Framework.Web.Mvc
namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：Pre_User(用户管理)
    /// </summary>
    public partial class Pre_UserController : BaseController
    {
        public Pre_UserController()
        {
        }

        #region 修改密码

        /// <summary>
        /// 用户管理--显示修改修改密码页面
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult ChangePass(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            resp.FunNameEn = "Edit";
            ModularOrFunCode = "AuthorizationAreas.Pre_User.ChangePass";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            resp.Item.LoginName = LoginInfo.LoginName;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 用户管理--修改密码保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult ChangePassSave(SoftProjectAreaEntityDomain domain)
        {
            domain.Item.Pre_UserID = LoginInfo.Pre_UserID;
            ModularOrFunCode = "AuthorizationAreas.Pre_User.ChangePass";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.Pre_User_ChangPass();

            return new HJsonResult(new { Data = resp});
        }

        
        #endregion

        #region 重置密码

        /// <summary>
        /// 用户管理--重置密码
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult ResetPass(SoftProjectAreaEntityDomain domain)
        {
            domain.Item.Pre_UserID = LoginInfo.Pre_UserID;
            domain.Item.PasswordDigest = ConfigurationManager.AppSettings["InitialPass"];
            ModularOrFunCode = "AuthorizationAreas.Pre_User.ResetPass";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.Pre_User_ChangPass();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 用户管理--重置密码（通过列表行）
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult ResetPassByRow(SoftProjectAreaEntityDomain domain)
        {
            
           
            domain.Item.PasswordDigest = ConfigurationManager.AppSettings["InitialPass"];
            ModularOrFunCode = "AuthorizationAreas.Pre_User.ResetPassByRow";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.Pre_User_ChangPass();
          
            return new HJsonResult(new { Data = resp });
        }

        #endregion

        #region 用户管理

        /// <summary>
        /// 用户管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            domain.Querys.Add(new Query { QuryType = 0, FieldName = "Pre_CompanyID___equal", Value = LoginInfo.Pre_CompanyID.ToString() });
            domain.Querys.Add(new Query { QuryType = 0, FieldName = "LoginCategoryID___equal", Value = "1" });

            ModularOrFunCode = "AuthorizationAreas.Pre_User.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            resp.Item.LoginCategoryID = 1;
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 用户管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            resp.Item.LoginCategoryID = 1;
            resp.FunNameEn = "Add";
            ModularOrFunCode = "AuthorizationAreas.Pre_User.Add";
            MenuCode = "AuthorizationPanel";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 用户管理--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            domain.Item.Pre_CompanyID = LoginInfo.Pre_CompanyID;
            domain.Item.LoginCategoryID = 1;//公司角色
            ModularOrFunCode = "AuthorizationAreas.Pre_User.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            
            if (!domain.IsEmail(domain.Item.Email))
            {
                throw new Exception("Email地址格式不正确！");
            }
            var resp = domain.Pre_User_AddSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 用户管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "AuthorizationAreas.Pre_User.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            resp.Item.LoginCategoryID = 1;
            resp.FunNameEn = "Edit";
            ModularOrFunCode = "AuthorizationAreas.Pre_User.Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 用户管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "AuthorizationAreas.Pre_User.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (!domain.IsEmail(domain.Item.Email))
            {
                throw new Exception("Email地址格式不正确！");
            }
            var resp = domain.Pre_User_EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 用户管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "AuthorizationAreas.Pre_User.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            resp.FunNameEn = "Detail";
            ModularOrFunCode = "AuthorizationAreas.Pre_User.Detail";
            MenuCode = "AuthorizationPanel";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        #endregion

        #region 用户个人信息修改

        /// <summary>
        /// 用户管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult EditByMy(SoftProjectAreaEntityDomain domain)
        {
            domain.Item.Pre_UserID = LoginInfo.Pre_UserID;
            ModularOrFunCode = "AuthorizationAreas.Pre_User.EditByMy";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            ModularOrFunCode = "AuthorizationAreas.Pre_User.EditByMy";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 用户管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HJsonResult EditByMySave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "AuthorizationAreas.Pre_User.EditByMy";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.Pre_User_EditByMySave();
            return new HJsonResult(new { Data = resp });
        }

        #endregion

        public ActionResult Login(SoftProjectAreaEntityDomain domain)
        {
            //LoginInfo ss = new SoftProject.CellModel.SoftProjectAreaEntity();
            var resp = new MyResponseBase();
            resp.Item = domain.Item;
            if (domain.Item.MobilePhone != null)
            {
                try
                {
                    resp = domain.Pre_User_Login();
                    //resp = new MyResponseBase();
                    //resp = domain.getHandleRemind();
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception e)
                {
                    resp.Item.ErrorMessage = e.Message;
                    return View(resp);
                }
            }
            return View(resp);
        }

        public ActionResult LogOut()
        {
            Session.Remove("LoginInfo");
            return RedirectToAction("Login", "Pre_User", new { area = "AuthorizationAreas" });
        }

    }

}
