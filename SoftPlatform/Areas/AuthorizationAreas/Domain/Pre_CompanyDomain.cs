﻿
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

//namespace Framework.Web.Mvc
namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：Pre_Company(公司管理)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 添加保存，包含添加企业管理员信息
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_Company_AddSave()
        {
            var resp = new MyResponseBase();
            #region 根据省、市、县组合为地区
            if (Item.Ba_AreaID1 != null)
            {
                Item.AreaName1 = SoftProjectAreaEntityDomain.Ba_Area_GetByAreaID(Item.Ba_AreaID1).AreaName;
                Item.AreaName = Item.AreaName1;
            }
            if (Item.Ba_AreaID2 != null)
            {
                Item.AreaName2 = SoftProjectAreaEntityDomain.Ba_Area_GetByAreaID(Item.Ba_AreaID2).AreaName;
                Item.AreaName += Item.AreaName2;
            }
            if (Item.Ba_AreaID3 != null)
            {
                Item.AreaName3 = SoftProjectAreaEntityDomain.Ba_Area_GetByAreaID(Item.Ba_AreaID3).AreaName;
                Item.AreaName += Item.AreaName3;
            }
            #endregion
            resp = ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                resp = AddSave();
                Item.Pre_CompanyID = resp.Item.Pre_CompanyID;
                Pre_User_AddAdminSave();
            }));
            return resp;
        }

        /// <summary>
        /// 编辑保存,包含更新企业管理员信息
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_Company_EditSave()
        {
            var resp = new MyResponseBase();
            #region 数据处理

            if (Item.Ba_AreaID1 != null)
            {
                Item.AreaName1 = SoftProjectAreaEntityDomain.Ba_Area_GetByAreaID(Item.Ba_AreaID1).AreaName;
                Item.AreaName = Item.AreaName1;
            }
            if (Item.Ba_AreaID2 != null)
            {
                Item.AreaName2 = SoftProjectAreaEntityDomain.Ba_Area_GetByAreaID(Item.Ba_AreaID2).AreaName;
                Item.AreaName += Item.AreaName2;
            }
            if (Item.Ba_AreaID3 != null)
            {
                Item.AreaName3 = SoftProjectAreaEntityDomain.Ba_Area_GetByAreaID(Item.Ba_AreaID3).AreaName;
                Item.AreaName += Item.AreaName3;
            }
            #endregion

            //更新加盟商零售价格表的商品为有效值
            var sqlProductPrice = ProjectCache.Sys_HOperControls.Where(p => p.OperCode == "Pre_Company.Fra_ProductPriceByUpdateBValidate").FirstOrDefault().DBTSql;
            sqlProductPrice = string.Format(sqlProductPrice, Item.Pre_CompanyID, Item.OperatingItemIDs);
            //更新加盟商的合作商价格表的商品为有效值
            var sqlPartnerProductPrice = ProjectCache.Sys_HOperControls.Where(p => p.OperCode == "Pre_Company.BC_PartnerProductPriceByUpdateBValidate").FirstOrDefault().DBTSql;
            sqlPartnerProductPrice = string.Format(sqlPartnerProductPrice, Item.Pre_CompanyID, Item.OperatingItemIDs);

            resp = ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                resp = EditSave();
                Pre_User_EditAdminSave();
                Query16(sqlProductPrice, 1);//更新加盟商零售价格表中，商品有效性。
                Query16(sqlPartnerProductPrice, 1);//更新加盟商的合作商价格表中，商品有效性。
            }));
            return resp;
        }


        /// <summary>
        /// 编辑保存,包含更新企业管理员信息
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_Company_ByID()
        {
            var resp = new MyResponseBase();
            Design_ModularOrFun = null;// new SoftProjectAreaEntity();
            ModularOrFunCode = "AuthorizationAreas.Pre_Company.CompanyDashboard";
            //domain.ModularOrFunCode = ModularOrFunCode;
            resp = ByID();
            return resp;
        }

    }
}
