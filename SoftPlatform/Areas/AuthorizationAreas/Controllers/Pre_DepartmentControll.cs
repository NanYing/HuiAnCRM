﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：Pre_Department(部门管理)
    /// </summary>
    public class Pre_DepartmentController : BaseController
    {
        public Pre_DepartmentController()
        {
        }

        /// <summary>
        /// 部门管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
[HttpGet]
public ActionResult Index(SoftProjectAreaEntityDomain domain)
{
    //domain.Querys.Add(new Query { QuryType = 0, FieldName = "Pre_CompanyID___equal", Value = LoginInfo.Pre_CompanyID.ToString() });

    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Index";
    domain.ModularOrFunCode = ModularOrFunCode;
    var resp = domain.QueryIndex();

    if (Request.IsAjaxRequest())
        return View(Design_ModularOrFun.PartialView, resp);
    resp.ViewContextName = Design_ModularOrFun.PartialView;
    return View(Design_ModularOrFun.MainView, resp);
}

        /// <summary>
        /// 部门管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            #region 初始化代码
            #endregion
    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 部门管理--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            if (domain.Item.DepartmentParentID == null)
                domain.Item.DepartmentParentID = 0;
    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.AddSave();
            domain.Pre_Department_AddCache();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 部门管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 部门管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            if (domain.Item.DepartmentParentID == null)
                domain.Item.DepartmentParentID = 0;
    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.EditSave();
            domain.Pre_Department_UpdateCache();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 部门管理--Row
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Row(SoftProjectAreaEntityDomain domain)
        {
    ModularOrFunCode = "AuthorizationAreas.Pre_Department.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            resp.Items.Add(resp.Item);
             resp.ViewContextName = Design_ModularOrFun.PartialView;
             return View("Rows", resp);
        }

   }
}
