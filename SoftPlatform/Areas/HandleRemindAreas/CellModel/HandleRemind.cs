
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：HandleRemind(提醒信息)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 提醒信息ID
        /// </summary>
        public  int?  HandleRemindID{get;set;}

        /// <summary>
        /// 提醒用户
        /// </summary>
        //public  int?  Pre_UserID{get;set;}

        /// <summary>
        /// 相关客户
        /// </summary>
        //public  int?  C_CustomerID{get;set;}

        /// <summary>
        /// 提醒时间
        /// </summary>
        public  DateTime?  RemindTime{get;set;}

        /// <summary>
        /// 办理时间
        /// </summary>
        public  DateTime?  DoThisTime{get;set;}

        /// <summary>
        /// 提醒类型
        /// </summary>
        public  string  RemindType{get;set;}

        /// <summary>
        /// 提醒类型
        /// </summary>
        public  string  RemindTypeText{get;set;}

        /// <summary>
        /// 提醒方式
        /// </summary>
        public  string  RemindFS{get;set;}

        /// <summary>
        /// 提醒方式
        /// </summary>
        public  string  RemindFSText{get;set;}

        /// <summary>
        /// 提醒方式集合
        /// </summary>
        public  string  RemindFSs{get;set;}

        /// <summary>
        /// 提醒内容
        /// </summary>
        public  string  RemindNR{get;set;}

        /// <summary>
        /// 提醒状态
        /// </summary>
        public  int?  Status{get;set;}

        public SoftProjectAreaEntity HandleRemind { get; set; }
        public List<SoftProjectAreaEntity> HandleReminds { get; set; }
    }
}
