
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：FD_ProductAttachment(商品附件框架演示)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 商品附件框架演示ID
        /// </summary>
        public  int?  FD_ProductAttachmentID{get;set;}

        /// <summary>
        /// 文档名
        /// </summary>
        //public  string  AttachmentFileName{get;set;}

        /// <summary>
        /// 文档路径
        /// </summary>
        //public  string  AttachmentFileNameGuid{get;set;}

        /// <summary>
        /// 文件类型
        /// </summary>
        //public  string  AttachmentFileType{get;set;}

        /// <summary>
        /// 文件大小
        /// </summary>
        //public  decimal?  AttachmentFileSize{get;set;}

        /// <summary>
        /// 文件大小
        /// </summary>
        //public  string  AttachmentFileSizeDisp{get;set;}

        /// <summary>
        /// 关联主表Guid
        /// </summary>
        //public  string  RefPKTableGuid{get;set;}

        public SoftProjectAreaEntity FD_ProductAttachment { get; set; }
        public List<SoftProjectAreaEntity> FD_ProductAttachments { get; set; }
    }
}
