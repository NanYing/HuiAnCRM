﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：(会议室下拉)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 缓存字段和属性
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        #region 缓存定义
        static List<SoftProjectAreaEntity> _MeetingRoomDrops = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> MeetingRoomDrops
        {
            get
            {
                if (_MeetingRoomDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _MeetingRoomDrops = domain.MeetingRoomDrops_GetAll().Items;
                }
                return _MeetingRoomDrops;
            }
        }

        #endregion

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static void MeetingRoomDrop_Clear()
        {
            _MeetingRoomDrops = new List<SoftProjectAreaEntity>();
        }


        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase MeetingRoomDrops_GetAll()
        {
            string sql = "SELECT * FROM V_MeetingRoom ";
            var resp = Query16(sql, 2);
            return resp;
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void MeetingRoomDrop_AddCache()
        {

            ModularOrFunCode = "MeetingAreas.MeetingRoom.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.MeetingRoomDrops.Add(resp.Item);

        }

        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void MeetingRoomDrop_UpdateCache()
        {
            ModularOrFunCode = "MeetingAreas.MeetingRoom.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var MeetingRoomDrop = SoftProjectAreaEntityDomain.MeetingRoomDrops.Where(p => p.MeetingRoomID == Item.MeetingRoomID).FirstOrDefault();

            SoftProjectAreaEntityDomain.MeetingRoomDrops.Remove(MeetingRoomDrop);
            SoftProjectAreaEntityDomain.MeetingRoomDrops.Add(resp.Item);
        }

        /// <summary>
        /// 查询条件-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string QueryHtmlDropDownList_MeetingRoomID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var MeetingRoomDropsTemp = MeetingRoomDrops;
            var str = HtmlHelpers.DropDownList(null, "MeetingRoomID___equal", MeetingRoomDropsTemp, "MeetingRoomID", "MeetingRoomIDName", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        /// <summary>
        /// 编辑页面-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string HtmlDropDownList_MeetingRoomID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var MeetingRoomDropsTemp = MeetingRoomDrops;
            var str = HtmlHelpers.DropDownList(null, "Item.MeetingRoomID", MeetingRoomDropsTemp, "MeetingRoomID", "MeetingRoomName", val, "");
            return str.ToString();
        }
        /// <summary>
        /// 活动使用会议室情况
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public Boolean MeetingRoomIsUse(int? MeetingRoomID, DateTime? sDate, DateTime? eDate, int ? activityID)
        {
            var sql = string.Format("select * from V_Activity where MeetingRoomID={0} and ('{1}' between ActivitysDate and ActivityeDate or '{2}' between ActivitysDate and ActivityeDate or ActivitysDate between '{1}' and '{2}' or ActivityeDate between '{1}' and '{2}')", MeetingRoomID, sDate, eDate);
            if (activityID != null)
            {
                sql = sql + " and activityID !=" + activityID;
            }
            var resp = Query16(sql, 2);
            if (resp.Items.Count > 0)
            {
                return true;
            }
            else {
                return false;
            }
        }

    }
}
