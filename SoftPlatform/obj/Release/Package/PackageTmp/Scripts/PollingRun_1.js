﻿
$(document).ready(
    function () {

    }
);


$(function () {
    window.polling = {
        ///长连接地址
        //connectionUrl: "/channel/polling",
        connectionUrl: "/ProjectAreas/P_Monitor/Polling",
        ///发送方式 
        method: "POST",
        ///事件载体
        //event_host: $("body"),
        ///连接失败时重连接时间
        period: 1000 * 20,
        ///连接超时时间
        timeOut: 180 * 1000,
        v: 0,
        ///连接ID
        id: "",
        No: $("#Item_ProjectCode").val(),
        PushFrequency: $("#Item_PushFrequency").val(),
        PageNameCn: $("#Item_PageNameCn").val(),
        UserName: $("#Item_UserName").val(),
        error_num: 0,
        Reconnect: function () {
            polling.v++;
            $.ajax({
                url: polling.connectionUrl,
                type: polling.method,
                data: {
                    No: polling.No,
                    PushFrequency: polling.PushFrequency,
                    PageNameCn: polling.PageNameCn,
                    UserName:polling.UserName,
                    id: polling.id, v: polling.v
                },
                dataType: "json",
                timeout: polling.timeOut,
                success: function (json) {
                    //alert(json.id);
                    polling.id = json.id;
                    ///版本号相同才回发服务器
                    if (json.v == polling.v)
                        polling.Reconnect();
                    ///无消息返回时不处理
                    if (json.result == "-1")
                        return;
                    //alert(json.datas.data.content);
                    $.each(json.datas, function (i, ajaxData) {
                        ajaxData.data.type = ajaxData.t;
                        //alert(ajaxData.data.content);
                        if (ajaxData.t != "1")
                            return;

                        var UIDatas = ajaxData.data.UIDatas;
                        if (!Framework.Tool.isUndefined(UIDatas)) {
                            var tempp = $.ENumberable.From(UIDatas);
                            //alert($(".uidisp").length);
                            $(".uidisp").each(function (i) {
                                var dom = $(this);
                                var address = dom.data("address");
                                var disptype = dom.data("disptype");
                                var dataformat = dom.data("dataformat");

                                var items = tempp.Where(function (x) {
                                    return (x.Address == address)
                                });
                                if (items.Count() > 0) {
                                    var item = items.First();
                                    if (disptype == "s")//状态显示
                                    {
                                        //"停止=0.运行=1，故障=2
                                        dom.removeClass("redcircle");
                                        dom.removeClass("greencircle");
                                        if (item.Value == "1")
                                            dom.addClass("greencircle");
                                        else if (item.Value == "2")
                                            dom.addClass("redcircle");
                                    }
                                    else {
                                        dom.html(item.Value);
                                    }
                                }
                            });
                        }
                        //data-address="10" data-disptype="s"
                        $("#new_msg").html($("<p>" + ajaxData.data.content + "</p>"));
                        //polling.event_host.triggerHandler("sys_msg", [ajaxData.data]);
                    });
                }, ///出错时重连
                error: function (errorMess) {
                    //alert("error111");
                    if (polling.error_num < 5) {
                        setTimeout(polling.Reconnect, 1000 * 2);
                        polling.error_num++;
                        return;
                    }
                    //alert("error222");
                    ///20秒后重新连接
                    setTimeout(polling.Reconnect, polling.period);
                }, ///释放资源
                complete: function (XHR, TS) { XHR = null }
            });
        }
    }
    polling.Reconnect();
    ///*-----------------------------------------------------------------------------*/
    /////新消息事件订阅
    //$("body").bind("sys_msg", function (event, json) {
    //    if (json.type != "1")
    //        return;
    //    $("#new_msg").append($("<p>" + json.content + "</p>"));
    //});
    ///*-----------------------------------------------------------------------------*/
    /////发送消息事件绑定
    //$("#sendMsg").click(function () {
    //    var self = $(this);
    //    $.post("/ProjectAreas/P_Project/AddNewMsg", $("#msg").serialize(), null, "json")
    //});
});
