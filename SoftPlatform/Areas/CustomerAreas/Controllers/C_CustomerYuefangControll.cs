﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：C_CustomerYuefang(客户约访记录)
    /// </summary>
    public class C_CustomerYuefangController : BaseController
    {
        public C_CustomerYuefangController()
        {
        }

        /// <summary>
        /// 客户约访记录--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            if (!domain.Querys.QueryDicts.ContainsKey("C_CustomerID___equal"))
            {
                if (domain.Item.C_CustomerID == null)
                    throw new Exception("主键不能为空");
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "C_CustomerID___equal", Value = domain.Item.C_CustomerID.ToString() });
            }

            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            SoftProjectAreaEntityDomain.initCustomerTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Index";
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户约访记录--列表
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult List(SoftProjectAreaEntityDomain domain)
        {
            if (!domain.Querys.QueryDicts.ContainsKey("C_CustomerID___equal"))
            {
                if (domain.Item.C_CustomerID == null)
                    throw new Exception("主键不能为空");
                domain.Querys.Add(new Query { QuryType = 0, FieldName = "C_CustomerID___equal", Value = domain.Item.C_CustomerID.ToString() });
            }

            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.List";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            SoftProjectAreaEntityDomain.initCustomerTabView(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.List";
            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户约访记录--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();            
            resp.Item.C_CustomerID = domain.Item.C_CustomerID;
            resp.Item.YuefangInputTime = DateTime.Now;
            resp.Item.YuefangInputMen = LoginInfo.UserName;
            resp.Item.YuefangInputMenID = LoginInfo.Pre_UserID;
          //  SoftProjectAreaEntityDomain.initTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户约访记录--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.AddSave();
            domain.updateVisitCount(domain.Item.C_CustomerID);
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 客户约访记录--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            domain.Item.C_CustomerID = resp.Item.C_CustomerID;

           // SoftProjectAreaEntityDomain.initTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户约访记录--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 客户约访记录--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            domain.Item.C_CustomerID = resp.Item.C_CustomerID;

          //  SoftProjectAreaEntityDomain.initTab(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Detail";
            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 客户约访记录--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail2(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Detail2";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            domain.Item.C_CustomerID = resp.Item.C_CustomerID;

          //  SoftProjectAreaEntityDomain.initTabView(this, resp, domain);
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Detail2";
            resp.FunNameEn = "Detail2";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

    }
}