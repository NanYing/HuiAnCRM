
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：Activity(活动管理)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 活动编号
        /// </summary>
        public  int?  ActivityID{get;set;}

        /// <summary>
        /// 会议室
        /// </summary>
        //public  int?  MeetingRoomID{get;set;}

        /// <summary>
        /// 活动名称
        /// </summary>
        public  string  ActivityName{get;set;}

        /// <summary>
        /// 活动开始时间
        /// </summary>
        public  DateTime?  ActivitysDate{get;set;}

        /// <summary>
        /// 活动结束时间
        /// </summary>
        public  DateTime?  ActivityeDate{get;set;}

        /// <summary>
        /// 活动状态
        /// </summary>
        public  int?  ActivityStatuID{get;set;}

        /// <summary>
        /// 活动状态
        /// </summary>
        public  string  ActivityStatuName{get;set;}

        public SoftProjectAreaEntity Activity { get; set; }
        public List<SoftProjectAreaEntity> Activitys { get; set; }
    }
}
