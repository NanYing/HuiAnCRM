
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：FD_OrderDetail(订单明细)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {

        public MyResponseBase FD_OrderDetail_AddEditDelete()
        {
            MyResponseBase resp = new MyResponseBase();
            using (var scope = new TransactionScope())
            {
                try
                {
                    #region (1)根据功能模块ID查询所有字段
                    var sql = string.Format(";SELECT * FROM [dbo].[FD_OrderDetail] A WHERE FD_OrderID={0} ", Item.FD_OrderID);
                    var OldItems = Query16(sql, 2).Items;
                    #endregion
                    #region (2)模块字段--数据整理
                    Item.Items.ForEach(p =>
                    {
                        p.FDTotalPrice = p.FDProductPrice * p.Number;//计算每个商品的总价
                        p.FD_OrderID = Item.FD_OrderID;//设置商品明细表的订单ID
                    });
                    var deleteIDsEnum = (from p in OldItems select p.FD_OrderDetailID).Except(from o in Item.Items select o.FD_OrderDetailID);
                    var updateItems = Item.Items.Where(p => p.FD_OrderDetailID != null && !deleteIDsEnum.Contains(p.FD_OrderDetailID));
                    var addItems = Item.Items.Where(p => p.FD_OrderDetailID == null);
                    #endregion

                    MyResponseBase resptemp = new MyResponseBase();
                    #region (3)删除元素:执行删除，通过In进行删除
                    if (deleteIDsEnum.Count() > 0)
                    {
                        var deleteIDs = string.Join(",", deleteIDsEnum);//deleteForecastIDsEnum.ToArray()
                        sql = string.Format("DELETE [dbo].[FD_OrderDetail] WHERE  FD_OrderDetailID IN({0})", deleteIDs);
                        resptemp = Query16(sql, 1);
                    }
                    #endregion
                    #region (4)更新模块字段
                    if (updateItems.Count() > 0)
                    {
                        Items = updateItems.ToList();
                        ModularOrFunCode = "FrameDomoAreas.FD_OrderDetail.Edit";
                        //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
                        ExcuteEnumsByModularOrFunCode(2);
                    }
                    #endregion

                    #region (6)添加
                    if (addItems.Count() > 0)
                    {
                        Items = addItems.ToList();
                        ModularOrFunCode = "FrameDomoAreas.FD_OrderDetail.Add";
                        //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
                        ExcuteEnumsByModularOrFunCode(1);
                    }
                    #endregion

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    scope.Dispose();
                }
            }

            return resp;
        }

        /// <summary>
        /// 根据页面ID查询页面字段
        /// </summary>
        /// <returns></returns>
        public MyResponseBase FD_OrderDetail_Rows()
        {
            MyResponseBase resp = new MyResponseBase();
            if (Item.FD_ProductID == null)
            {
                throw new Exception("商品ID主键不能为空！");
            }

            string sql = string.Format("SELECT * FROM V_FD_Product  WHERE   FD_ProductID={0} ", Item.FD_ProductID);
            resp = Query16(sql, 2);
            resp.Items.ForEach(p => { p.FDProductPrice = p.FDProductPrice; p.Number = 1; p.FDTotalPrice = p.FDProductPrice; });
            return resp;
        }

    }
}
