﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：MeetingRoom(会议室管理)
    /// </summary>
    public class MeetingRoomController : BaseController
    {
        public MeetingRoomController()
        {
        }

        /// <summary>
        /// 会议室管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {

            ModularOrFunCode = "MeetingAreas.MeetingRoom.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 会议室管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            ModularOrFunCode = "MeetingAreas.MeetingRoom.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 会议室管理--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            ModularOrFunCode = "MeetingAreas.MeetingRoom.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (domain.Item.SeatRow > 20)
            {
                throw new Exception("会议室座位最大行数不能超过20！");
            }
            if (domain.Item.SeatColumn > 25)
            {
                throw new Exception("会议室座位最大列数不能超过25！");
            }
            var resp = domain.AddSave();
            domain.MeetingRoomDrop_AddCache();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 会议室管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "MeetingAreas.MeetingRoom.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            resp.Item.MeetingRoomMemo = Server.UrlDecode(resp.Item.MeetingRoomMemo);
            ModularOrFunCode = "MeetingAreas.MeetingRoom.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 会议室管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "MeetingAreas.MeetingRoom.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (domain.Item.SeatRow > 20)
            {
                throw new Exception("会议室座位最大行数不能超过20！");
            }
            if (domain.Item.SeatColumn > 25)
            {
                throw new Exception("会议室座位最大列数不能超过25！");
            }
            var resp = domain.EditSave();
            domain.MeetingRoomDrop_UpdateCache();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 会议室管理--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "MeetingAreas.MeetingRoom.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();
            resp.Item.MeetingRoomMemo = Server.UrlDecode(resp.Item.MeetingRoomMemo);
            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

    }
}
