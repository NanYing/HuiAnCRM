
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：FD_OrderDetail(订单明细框架演示)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 订单明细ID
        /// </summary>
        public  int?  FD_OrderDetailID{get;set;}

        /// <summary>
        /// 订单ID
        /// </summary>
        //public  int?  FD_OrderID{get;set;}

        /// <summary>
        /// 商品ID
        /// </summary>
        //public  int?  FD_ProductID{get;set;}

        /// <summary>
        /// 数量
        /// </summary>
        public  int?  Number{get;set;}

        /// <summary>
        /// 单价
        /// </summary>
        //public  decimal?  FDProductPrice{get;set;}

        /// <summary>
        /// 总价
        /// </summary>
        public  decimal?  FDTotalPrice{get;set;}

        public SoftProjectAreaEntity FD_OrderDetail { get; set; }
        public List<SoftProjectAreaEntity> FD_OrderDetails { get; set; }
    }
}
