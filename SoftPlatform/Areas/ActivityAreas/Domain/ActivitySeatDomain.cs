﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoftProject.Domain
{
    /// <summary>
    /// 活动座位：(业务逻辑)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        public MyResponseBase ActivityBaseInfo(string ActivityID)
        {
            var sql = string.Format("select * from V_activity where ActivityID={0}", ActivityID);
            var resp = Query16(sql, 4);
            return resp;
        }

        public MyResponseBase ActivitySeatInfo(string ActivityID)
        {
            var sql = string.Format("select Seat from V_Activity inner join V_ActivitySeat on V_Activity.ActivityID = V_ActivitySeat.ActivityID where V_Activity.ActivityID ={0}", ActivityID);
            var resp = Query16(sql, 2);
            return resp;
        }

        public MyResponseBase CustomerBaseInfo(string CustomerIdCardNo)
        {
            var sql = string.Format("select C_CustomerID,CustomerIdCardNo,CustomerName,CustomerCardLevelText,CustomerTypeText from V_C_Customer where CustomerIdCardNo = '{0}' or C_CustomerID='{1}'", CustomerIdCardNo, CustomerIdCardNo);
            var resp = Query16(sql, 4);
            return resp;
        }

        public MyResponseBase CustomerSeatInfo(string CustomerIdCardNo,string ActivityID)
        {
            var sql = string.Format("select ActivitySeatID,Seat,V_ActivitySeat.C_CustomerID from V_ActivitySeat left outer join V_C_Customer  on V_C_Customer.C_CustomerID = V_ActivitySeat.C_CustomerID  where ActivityID = '{0}' and CustomerIdCardNo='{1}'", ActivityID, CustomerIdCardNo);
            var resp = Query16(sql, 4);
            return resp;
        }

       

    }
}
