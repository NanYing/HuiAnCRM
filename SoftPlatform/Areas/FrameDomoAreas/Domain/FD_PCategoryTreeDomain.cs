﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：FD_PCategoryTree(商品类别树框架演示)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 缓存字段和属性
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
#region 缓存定义
static List<SoftProjectAreaEntity> _FD_PCategoryTrees = new List<SoftProjectAreaEntity>();

public static List<SoftProjectAreaEntity> FD_PCategoryTrees
{
    get
    {
        if (_FD_PCategoryTrees.Count == 0)
        {
            SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
            _FD_PCategoryTrees = domain.FD_PCategoryTree_GetAll().Items;
        }
        return _FD_PCategoryTrees;
    }
}

#endregion

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static void FD_PCategoryTrees_Clear()
{
    _FD_PCategoryTrees = new List<SoftProjectAreaEntity>();
}


        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public MyResponseBase FD_PCategoryTree_GetAll()
{
    string sql = "SELECT * FROM V_FD_PCategoryTree ";
    var resp = Query16(sql, 2);
    return resp;
}

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public void FD_PCategoryTree_AddCache()
{

    ModularOrFunCode = "FrameDomoAreas.FD_PCategoryTree.Index";
    //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
    resp = ByID();
    SoftProjectAreaEntityDomain.FD_PCategoryTrees.Add(resp.Item);

}

        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public void FD_PCategoryTree_UpdateCache()
{
    ModularOrFunCode = "FrameDomoAreas.FD_PCategoryTree.Index";
    //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
    resp = ByID();
    var FD_PCategoryTree = SoftProjectAreaEntityDomain.FD_PCategoryTrees.Where(p => p.FD_PCategoryTreeID == Item.FD_PCategoryTreeID).FirstOrDefault();

    SoftProjectAreaEntityDomain.FD_PCategoryTrees.Remove(FD_PCategoryTree);
    SoftProjectAreaEntityDomain.FD_PCategoryTrees.Add(resp.Item);
}

        /// <summary>
        /// 查询条件-下拉树(父节点)--自身管理
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string QueryHtmlDropDownList_PCategoryTreeParentID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items = FD_PCategoryTrees;
var tt = new SelectTreeList(Items, "0", "PCategoryTreeName", "FD_PCategoryTreeID", "PCategoryTreeParentID", "FD_PCategoryTreeID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "PCategoryTreeParentID___equal", tt, "=="+NameCn+"==");
    var strDrop = str.ToString();
    return strDrop;
}

        /// <summary>
        /// 编辑页面-下拉树(父节点)-模块自身管理
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string HtmlDropDownLiss_PCategoryTreeParentID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items= FD_PCategoryTrees;
var tt = new SelectTreeList(Items, "0", "PCategoryTreeName", "FD_PCategoryTreeID", "PCategoryTreeParentID", "FD_PCategoryTreeID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "Item.PCategoryTreeParentID", tt, "=="+NameCn+"==");
    return str.ToString();
}


        /// <summary>
        /// 查询条件-下拉树(节点)--其它模块使用
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string QueryHtmlDropDownList_FD_PCategoryTreeID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items = FD_PCategoryTrees;
var tt = new SelectTreeList(Items, "0", "PCategoryTreeName", "FD_PCategoryTreeID", "PCategoryTreeParentID", "FD_PCategoryTreeID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "FD_PCategoryTreeID___equal", tt, "=="+NameCn+"==");
    var strDrop = str.ToString();
    return strDrop;
}

        /// <summary>
        /// 编辑页面-下拉树(父节点)-其它模块使用
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string HtmlDropDownLiss_FD_PCategoryTreeID(string val, string NameCn, SoftProjectAreaEntity item)
{
    var Items= FD_PCategoryTrees;
var tt = new SelectTreeList(Items, "0", "PCategoryTreeName", "FD_PCategoryTreeID", "PCategoryTreeParentID", "FD_PCategoryTreeID", val, true, "");
var str = HtmlHelpers.DropDownForTree(null, "Item.FD_PCategoryTreeID", tt, "=="+NameCn+"==");
    return str.ToString();
}


        /// <summary>
        /// 列表页面-树(父节点)
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
public static string JqTreeNs_FD_PCategoryTreeID(string paramNameType, string NameCn, SoftProjectAreaEntity item)
{
    var TreeQueryType = Convert.ToInt32(paramNameType);
    var Itemst = SoftProjectAreaEntityDomain.FD_PCategoryTrees;
    var treeList = new TreeList(Itemst, "0", "PCategoryTreeName", "FD_PCategoryTreeID", "PCategoryTreeParentID", "", "", "FD_PCategoryTreeID", "", "");
    var str = HtmlHelpersProject.JqTreeN(null, "FD_PCategoryTreeID", treeList, "", TreeQueryType);
    return str.ToString();
}

   }
}
