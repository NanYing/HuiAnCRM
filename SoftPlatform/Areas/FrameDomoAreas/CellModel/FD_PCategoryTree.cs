
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：FD_PCategoryTree(商品类别树框架演示)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 商品类别树ID
        /// </summary>
        public  int?  FD_PCategoryTreeID{get;set;}

        /// <summary>
        /// 父节点
        /// </summary>
        public  int?  PCategoryTreeParentID{get;set;}

        /// <summary>
        /// 父节点
        /// </summary>
        public  string  PCategoryTreeParentName{get;set;}

        /// <summary>
        /// 商品类别树框架演示名称
        /// </summary>
        public  string  PCategoryTreeName{get;set;}

        /// <summary>
        /// 排序号
        /// </summary>
        public  int?  PCategoryTreeSort{get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        public  int?  PCategoryTreeStatuID{get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        public  string  PCategoryTreeStatuName{get;set;}

        public SoftProjectAreaEntity FD_PCategoryTree { get; set; }
        public List<SoftProjectAreaEntity> FD_PCategoryTrees { get; set; }
    }
}
