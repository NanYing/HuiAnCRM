
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：FD_OrderID(订单管理)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase FD_Order_AddSave()
        {
            var resp = new MyResponseBase();
            //计算订单明细总金额
            //Item.Items：保存订单明细数据
            Item.FDOrderAmount = Item.Items.Sum(p => p.FDProductPrice * p.Number);
            ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                //添加订单
                resp = AddSave();
                //添加订单明细：添加、修改、删除
                FD_OrderDetail_AddEditDelete();
            }));

            return resp;
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase FD_Order_EditSave()
        {
            var resp = new MyResponseBase();
            Item.FDOrderAmount = Item.Items.Sum(p => p.FDProductPrice * p.Number);
            ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                //编辑订单
                resp = EditSave();
                //编辑订单明细
                FD_OrderDetail_AddEditDelete();
            }));

            return resp;
        }
    }
}
