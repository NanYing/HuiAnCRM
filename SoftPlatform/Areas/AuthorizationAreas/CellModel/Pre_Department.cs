
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：Pre_Department(部门)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public  int?  Pre_DepartmentID{get;set;}

        /// <summary>
        /// 父节点
        /// </summary>
        public  int?  DepartmentParentID{get;set;}

        /// <summary>
        /// 父节点
        /// </summary>
        public  string  DepartmentParentName{get;set;}

        /// <summary>
        /// 名称
        /// </summary>
        public  string  DepartmentName{get;set;}

        /// <summary>
        /// 排序号
        /// </summary>
        public  int?  DepartmentSort{get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        public  int?  DepartmentStatuID{get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        public  string  DepartmentStatuName{get;set;}

        public SoftProjectAreaEntity Pre_Department { get; set; }
        public List<SoftProjectAreaEntity> Pre_Departments { get; set; }
    }
}
