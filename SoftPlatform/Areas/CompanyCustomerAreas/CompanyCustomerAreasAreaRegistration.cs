﻿using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    public class CompanyCustomerAreasAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CompanyCustomerAreas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CompanyCustomerAreas_default",
                "CompanyCustomerAreas/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
