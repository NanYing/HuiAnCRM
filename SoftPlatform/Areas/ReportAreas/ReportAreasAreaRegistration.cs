﻿using SoftProject.Domain;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    public class ReportAreasAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportAreas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportAreas_default",
                "ReportAreas/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            var load1 = ProjectCache.Sys_Dicts;
            var load2 = ProjectCache.Sys_HOperControls;
        }
    }
}
