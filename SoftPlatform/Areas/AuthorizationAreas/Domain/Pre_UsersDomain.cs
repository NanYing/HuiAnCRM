﻿
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftPlatform.Controllers;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

//namespace Framework.Web.Mvc
namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：Pre_User(用户管理)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <returns></returns>
        public void Pre_User_Domain()
        {
            PKField = "Pre_UserID";
            //PKFields = new List<string> { "Pre_UserID" };
            TableName = "Pre_User";
            TabViewName = "V_Pre_User";
        }

        public void ValidateMobilePhoneAdd()
        {
            if (SoftProjectAreaEntityDomain.Pre_UserRoleAll.Where(p => p.MobilePhone == Item.MobilePhone).Count() > 0)
                throw new Exception("手机号码重复：公司用户已使用些号码");
        }

        public void ValidateMobilePhoneUpdate()
        {
            if (SoftProjectAreaEntityDomain.Pre_UserRoleAll.Where(p => p.MobilePhone == Item.MobilePhone && p.Pre_UserID != Item.Pre_UserID).Count() > 0)
                throw new Exception("手机号码重复：公司用户已使用些号码");
        }

        #region 管理员

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_User_AddSave()
        {
            var resp = new MyResponseBase();
            ValidateMobilePhoneAdd();
            resp = AddSave();
            Item.Pre_UserID = resp.Item.Pre_UserID;
            Pre_User_AddCache();
            return resp;
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_User_EditSave()
        {
            var resp = new MyResponseBase();
            ValidateMobilePhoneUpdate();
            resp = EditSave();
            Pre_User_UpdateCache();
            return resp;
        }

        /// <summary>
        /// 修改个人信息
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_User_EditByMySave()
        {
            var resp = new MyResponseBase();
            ValidateMobilePhoneUpdate();
            resp = EditSave();
            Pre_User_UpdateCacheByMy();
            return resp;
        }

        #endregion

        public MyResponseBase Pre_User_ChangPass()
        {
            var resp = new MyResponseBase();
            try
            {
                var sql = string.Format("Update  Pre_User  SET  PasswordDigest='{0}' WHERE  Pre_UserID={1}", Item.PasswordDigest, Item.Pre_UserID);
                resp = Query16(sql, 1);
                var item = SoftProjectAreaEntityDomain.Pre_UserRoleAll.Where(p => p.Pre_UserID == LoginInfo.Pre_UserID).FirstOrDefault();
                item.PasswordDigest = Item.PasswordDigest;
            }
            catch
            {
                throw new Exception("修改密码失败");
            }
            return resp;
        }

        #region 缓存、界面元素

        #region 所有用户角色：保存用户时清空

        public void Pre_User_AddCache()
        {
            #region 更新：企业用户缓存

            ModularOrFunCode = "AuthorizationAreas.Pre_User.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.Pre_UserRoleAll.Add(resp.Item);

            #endregion
        }

        public void Pre_User_UpdateCache()
        {
            #region (3)根据ID查询，替换

            ModularOrFunCode = "AuthorizationAreas.Pre_User.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var Pre_User = SoftProjectAreaEntityDomain.Pre_UserRoleAll.Where(p => p.Pre_UserID == Item.Pre_UserID).FirstOrDefault();
            SoftProjectAreaEntityDomain.Pre_UserRoleAll.Remove(Pre_User);
            SoftProjectAreaEntityDomain.Pre_UserRoleAll.Add(resp.Item);
            #endregion
        }

        public void Pre_User_UpdateCacheByMy()
        {
            #region (3)根据ID查询，替换

            ModularOrFunCode = "AuthorizationAreas.Pre_User.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var Pre_User = SoftProjectAreaEntityDomain.Pre_UserRoleAll.Where(p => p.Pre_UserID == Item.Pre_UserID).FirstOrDefault();
            SoftProjectAreaEntityDomain.Pre_UserRoleAll.Remove(Pre_User);
            SoftProjectAreaEntityDomain.Pre_UserRoleAll.Add(resp.Item);
            //Pre_User = resp.Item;

            var user = resp.Item;

            Pre_User_BulidSession(user);

            #endregion
        }

        /// <summary>
        /// 所有用户管理：缓存
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_User_All()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.AppendLine("SELECT *  FROM [dbo].[V_Pre_User] A ");
            var sql = sbSql.ToString();
            var resp = Query16(sql);
            return resp;
        }

        static List<SoftProjectAreaEntity> _Pre_UserRoleAll = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> Pre_UserRoleAll
        {
            get
            {
                if (_Pre_UserRoleAll.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    //_Pre_UserRoleAll = domain.Pre_UserRole_All().Items;
                    _Pre_UserRoleAll = domain.Pre_User_All().Items;
                }
                return _Pre_UserRoleAll;
            }
        }

        public static void Pre_UserRoleAll_Clare()
        {
            _Pre_UserRoleAll = new List<SoftProjectAreaEntity>();
        }

        #endregion

        public static string QueryHtmlDropDownList_Pre_UserID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var Pre_UsersTemp = Pre_UserRoleAll.Where(p => p.Pre_CompanyID == item.Pre_CompanyID);
            var str = HtmlHelpers.DropDownList(null, "Pre_UserID___equal", Pre_UsersTemp, "Pre_UserID", "UserName", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        public static string HtmlDropDownLiss_Pre_UserID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            //必须知道是公司、企业、顾客功能，从而进行过滤
            var Pre_UsersTemp = Pre_UserRoleAll.Where(p => p.Pre_CompanyID == item.Pre_CompanyID);
            var str = HtmlHelpers.DropDownList(null, "Item.Pre_UserID", Pre_UsersTemp, "Pre_UserID", "UserName", val, "");
            return str.ToString();
        }

        #endregion

        public MyResponseBase Pre_User_Login()
        {
            if (string.IsNullOrEmpty(Item.MobilePhone) || string.IsNullOrEmpty(Item.PasswordDigest))
            {
                throw new Exception("手机号和密码不能为空");
            }

            #region 用户登录

            var Users = SoftProjectAreaEntityDomain.Pre_UserRoleAll.Where(p => p.MobilePhone == Item.MobilePhone && p.PasswordDigest == Item.PasswordDigest && p.UserStatuID == 1);

            if (Users.Count() > 0)
            {
                #region 公司
                var user = Users.First();
                if (user.LogCategoryID == 2)//企业用户
                {
                    //如果企业名称、用户名称不同在此处都赋值级...
                }
                resp.Item = user;
                Pre_User_BulidSession(user);
                #region 写日志
                
                var log = new SoftProjectAreaEntity
                {
                    LoginCategoryID = resp.Item.LoginCategoryID,
                    OperName = "登录",
                    LogPersonID = user.Pre_UserID,
                    LogPerson = resp.Item.UserName,
                    OperLogIdent = 0,
                    IdentPkName = "Login",
                    Pre_CompanyID=user.Pre_CompanyID,
                    Pre_UserID=user.Pre_UserID
                };
                SoftProjectAreaEntityDomain.Sys_OperLogDetail_AddSave(log);
                #endregion

                #endregion
            }
            else
            {
                throw new Exception("用户、密码不正确，或此用户已被停用!");
            }
            #endregion
            return resp;
        }

        public static void Pre_User_BulidSession(SoftProjectAreaEntity Pre_User)
        {
            if (Pre_User.LogCategoryID == 2)
            {
            }
            HttpContext.Current.Session["LoginInfo"] = Pre_User;
        }

        #region email地址格式验证
        
        public bool IsEmail(string str_Email)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(str_Email, @"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
        }
        #endregion

    }
}
