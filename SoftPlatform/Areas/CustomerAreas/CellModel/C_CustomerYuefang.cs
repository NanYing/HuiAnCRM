
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：C_CustomerYuefang(客户约访记录)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 约访编号
        /// </summary>
        public  int?  C_CustomerYuefangID{get;set;}

        /// <summary>
        /// 客户编号
        /// </summary>
        //public  int?  C_CustomerID{get;set;}

        /// <summary>
        /// 到访时间
        /// </summary>
        public  DateTime?  YuefangVisitingTime{get;set;}

        /// <summary>
        /// 礼品
        /// </summary>
        public  int?  YuefangGift{get;set;}

        /// <summary>
        /// 礼品
        /// </summary>
        public  string  YuefangGiftText{get;set;}

        /// <summary>
        /// 数量及价格
        /// </summary>
        public  string  YuefangGiftNumber{get;set;}

        /// <summary>
        /// 课程
        /// </summary>
        public  int?  YuefangCourse{get;set;}

        /// <summary>
        /// 课程
        /// </summary>
        public  string  YuefangCourseText{get;set;}

        /// <summary>
        /// 录入时间
        /// </summary>
        public  DateTime?  YuefangInputTime{get;set;}

        /// <summary>
        /// 录入人ID
        /// </summary>
        public  int?  YuefangInputMenID{get;set;}

        /// <summary>
        /// 录入人员
        /// </summary>
        public  string  YuefangInputMen{get;set;}

        /// <summary>
        /// 与投保人关系
        /// </summary>
        public  int?  YuefangCustomerRelate{get;set;}

        /// <summary>
        /// 与客户关系
        /// </summary>
        public  string  YuefangCustomerRelateText{get;set;}

        /// <summary>
        /// 是否成交
        /// </summary>
        public  int?  YuefangIsDeal{get;set;}

        /// <summary>
        /// 是否成交
        /// </summary>
        public  string  YuefangIsDealText{get;set;}

        /// <summary>
        /// 面谈详细
        /// </summary>
        public  string  YuefangInterviewDetails{get;set;}

        public SoftProjectAreaEntity C_CustomerYuefang { get; set; }
        public List<SoftProjectAreaEntity> C_CustomerYuefangs { get; set; }
    }
}
