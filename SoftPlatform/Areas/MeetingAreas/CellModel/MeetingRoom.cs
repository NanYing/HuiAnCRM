
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：MeetingRoom(会议室管理)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 会议室编号
        /// </summary>
        public  int?  MeetingRoomID{get;set;}

        /// <summary>
        /// 会议室名称
        /// </summary>
        public  string  MeetingRoomName{get;set;}

        /// <summary>
        /// 座位列数
        /// </summary>
        public  int?  SeatColumn{get;set;}

        /// <summary>
        /// 座位行数
        /// </summary>
        public  int?  SeatRow{get;set;}

        /// <summary>
        /// 地点
        /// </summary>
        public  string  Location{get;set;}

        /// <summary>
        /// 备注
        /// </summary>
        public  string  MeetingRoomMemo{get;set;}

        public SoftProjectAreaEntity MeetingRoom { get; set; }
        public List<SoftProjectAreaEntity> MeetingRooms { get; set; }
    }
}
