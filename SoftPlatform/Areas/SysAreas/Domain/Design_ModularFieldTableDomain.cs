﻿
using Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Web.Mvc.Sys;
using System.Transactions;
using SoftProject.CellModel;
using Framework.Web.Mvc;
using System.IO;
using System.Web;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：Design_ModularFieldTable
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        #region 公共部分

        public void Design_ModularFieldTable_Domain()
        {
            PKField = "Design_ModularFieldTableID";
            //PKFields = new List<string> { "Design_ModularFieldTableID" };
            TableName = "Design_ModularFieldTable";
        }

        /// <summary>
        /// 主键验证
        /// </summary>
        /// <returns></returns>
        public void Design_ModularFieldTable_PKCheck()
        {
            if (Item.Design_ModularFieldTableID == null)
            {
                throw new Exception("功能模块主键不能为空！");
            }
        }

        #endregion

        public MyResponseBase Design_ModularFieldTable_Index()
        {
            MyResponseBase resp = new MyResponseBase();
            var sql = "SELECT * FROM  Design_ModularFieldTable ORDER BY ModularFieldTableSort";
            resp=Query16(sql);
            return resp;
        }

        public MyResponseBase Design_ModularFieldTable_AddSave()
        {
            MyResponseBase resp = new MyResponseBase();
            Design_ModularFieldTable_Domain();
            string DBFieldVals = "Design_ModularFieldTableName,Design_ModularFieldTableNameCN,ModularFieldTableSort,ModularFieldTableAreasCode";
            resp = AddSave(DBFieldVals);
            return resp;
        }

        public MyResponseBase Design_ModularFieldTable_EditSave()
        {
            MyResponseBase resp = new MyResponseBase();
            ////(1)检查主键ID
            //Design_ModularFieldTable_PKCheck();

            Design_ModularFieldTable_Domain();
            //if (Item.PrimaryKey == null)
            //    Item.PrimaryKey = Item.ControllCode + "ID";
            string DBFieldVals = "Design_ModularFieldTableName,Design_ModularFieldTableNameCN,ModularFieldTableSort,ModularFieldTableAreasCode";
            resp = EditSave(DBFieldVals,null);
            //ProjectCache.Design_ModularFieldTables_Clear();
            return resp;
        }

    }
}
