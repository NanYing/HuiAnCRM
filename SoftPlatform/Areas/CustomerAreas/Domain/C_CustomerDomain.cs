﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using SoftPlatform.Controllers;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：C_Customer(客户信息)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        #region Tab页
        public static void initCustomerTab(BaseController controller, MyResponseBase resp,
            SoftProjectAreaEntityDomain domain)
        {
            controller.TabModularOrFunCode = "CustomerAreas.C_Customer.TabEdit";
            resp.BaseAction = new SoftProject.CellModel.SoftProjectAreaEntity
            {
                C_CustomerID = domain.Item.C_CustomerID,
                ControllName = "C_Customer",
                ActionNameEn = "BaseInfo",
            };
        }
        public static void initCustomerTabView(BaseController controller, MyResponseBase resp,
            SoftProjectAreaEntityDomain domain)
        {
            controller.TabModularOrFunCode = "CustomerAreas.C_Customer.TabView";
            resp.BaseAction = new SoftProject.CellModel.SoftProjectAreaEntity
            {
                C_CustomerID = domain.Item.C_CustomerID,
                ControllName = "C_Customer",
                ActionNameEn = "BaseInfo",
            };
        }
        private static int? _C_CustomerID;
        private static string _C_CustomerIDs;

        public static string C_CustomerIDs
        {
            set { SoftProjectAreaEntityDomain._C_CustomerIDs = value; }
        }
        public static int? C_CustomerID
        {
            set
            {
                _C_CustomerID = value;
            }
        }
        #endregion
        #region 约访记录
        #region 缓存定义
        static List<SoftProjectAreaEntity> _CustomerYuefangDrops = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> CustomerYuefangDrops
        {
            get
            {
                // if (_CustomerYuefangDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _CustomerYuefangDrops = domain.CustomerYuefangDrops_GetAll().Items;
                }
                return _CustomerYuefangDrops;
            }
        }

        #endregion

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static void CustomerYuefangDrop_Clear()
        {
            _CustomerYuefangDrops = new List<SoftProjectAreaEntity>();
        }


        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase CustomerYuefangDrops_GetAll()
        {
            string sql = string.Format("SELECT * FROM V_C_CustomerYuefang where C_CustomerID={0}", _C_CustomerID);
            var resp = Query16(sql, 2);
            return resp;
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void CustomerYuefangDrop_AddCache()
        {

            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.CustomerYuefangDrops.Add(resp.Item);

        }

        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void CustomerYuefangDrop_UpdateCache()
        {
            ModularOrFunCode = "CustomerAreas.C_CustomerYuefang.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var CustomerYuefangDrop = SoftProjectAreaEntityDomain.CustomerYuefangDrops.Where(p => p.C_CustomerYuefangID == Item.C_CustomerYuefangID).FirstOrDefault();

            SoftProjectAreaEntityDomain.CustomerYuefangDrops.Remove(CustomerYuefangDrop);
            SoftProjectAreaEntityDomain.CustomerYuefangDrops.Add(resp.Item);
        }

        /// <summary>
        /// 查询条件-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string QueryHtmlDropDownList_C_CustomerYuefangID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var CustomerYuefangDropsTemp = CustomerYuefangDrops;
            var str = HtmlHelpers.DropDownList(null, "C_CustomerYuefangID___equal", CustomerYuefangDropsTemp, "C_CustomerYuefangID", "YuefangVisitingTime", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        /// <summary>
        /// 编辑页面-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string HtmlDropDownList_C_CustomerYuefangID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var CustomerYuefangDropsTemp = CustomerYuefangDrops;
            var str = HtmlHelpers.DropDownList(null, "Item.C_CustomerYuefangID", CustomerYuefangDropsTemp, "C_CustomerYuefangID", "YuefangVisitingTime", val, "");
            return str.ToString();
        }
        #endregion
        #region 受益人

        #region 缓存定义
        static List<SoftProjectAreaEntity> _TransactionBeneficiaryDrops = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> TransactionBeneficiaryDrops
        {
            get
            {
                // if (_TransactionManDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _TransactionBeneficiaryDrops = domain.TransactionBeneficiaryDrops_GetAll().Items;

                }
                return _TransactionBeneficiaryDrops;
            }
        }
        #endregion
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static void TransactionBeneficiaryDrops_Clear()
        {
            _TransactionBeneficiaryDrops = new List<SoftProjectAreaEntity>();
        }
        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase TransactionBeneficiaryDrops_GetAll()
        {
            string sql = string.Format("SELECT * FROM V_C_CustomerTransactionMan where C_CustomerID={0}", _C_CustomerID);
            var resp = Query16(sql, 2);
            return resp;
        }
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void TransactionBeneficiaryDrops_AddCache()
        {

            ModularOrFunCode = "CustomerAreas.C_Customer.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.TransactionBeneficiaryDrops.Add(resp.Item);

        }
        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void TransactionBeneficiaryDrops_UpdateCache()
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var TransactionBeneficiaryDrop = SoftProjectAreaEntityDomain.TransactionBeneficiaryDrops.Where(p => p.C_CustomerID == Item.C_CustomerID).FirstOrDefault();

            SoftProjectAreaEntityDomain.TransactionBeneficiaryDrops.Remove(TransactionBeneficiaryDrop);
            SoftProjectAreaEntityDomain.TransactionBeneficiaryDrops.Add(resp.Item);
        }
        /// <summary>
        /// 查询条件-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string QueryHtmlDropDownList_TransactionBeneficiaryID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var TransactionBeneficiaryDropsTemp = TransactionBeneficiaryDrops;
            var str = HtmlHelpers.DropDownList(null, "TransactionBeneficiary___equal", TransactionBeneficiaryDropsTemp, "DText", "DText", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        /// <summary>
        /// 编辑页面-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string HtmlDropDownList_TransactionBeneficiaryID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var TransactionBeneficiaryDropsTemp = TransactionBeneficiaryDrops;
            var str = HtmlHelpers.DropDownList(null, "Item.TransactionBeneficiary", TransactionBeneficiaryDropsTemp, "DText", "DText", val, "");
            return str.ToString();
        }
        #endregion
        #region 被保险人
        #region 缓存定义
        static List<SoftProjectAreaEntity> _TransactionInsuredDrops = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> TransactionInsuredDrops
        {
            get
            {
                // if (_TransactionManDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _TransactionInsuredDrops = domain.TransactionInsuredDrops_GetAll().Items;

                }
                return _TransactionInsuredDrops;
            }
        }
        #endregion
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static void TransactionInsuredDrops_Clear()
        {
            _TransactionInsuredDrops = new List<SoftProjectAreaEntity>();
        }

        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase TransactionInsuredDrops_GetAll()
        {
            string sql = string.Format("SELECT * FROM V_C_CustomerTransactionMan where C_CustomerID={0}", _C_CustomerID);
            var resp = Query16(sql, 2);
            return resp;
        }
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void TransactionInsuredDrops_AddCache()
        {

            ModularOrFunCode = "CustomerAreas.C_Customer.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.TransactionInsuredDrops.Add(resp.Item);

        }
        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void TransactionInsuredDrops_UpdateCache()
        {
            ModularOrFunCode = "CustomerAreas.C_Customer.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var TransactionInsuredDrop = SoftProjectAreaEntityDomain.TransactionInsuredDrops.Where(p => p.C_CustomerID == Item.C_CustomerID).FirstOrDefault();

            SoftProjectAreaEntityDomain.TransactionInsuredDrops.Remove(TransactionInsuredDrop);
            SoftProjectAreaEntityDomain.TransactionInsuredDrops.Add(resp.Item);
        }


        /// <summary>
        /// 查询条件-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string QueryHtmlDropDownList_TransactionInsuredID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var TransactionInsuredDropsTemp = TransactionInsuredDrops;
            var str = HtmlHelpers.DropDownList(null, "TransactionInsured___equal", TransactionInsuredDropsTemp, "DText", "DText", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        /// <summary>
        /// 编辑页面-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string HtmlDropDownList_TransactionInsuredID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var TransactionInsuredDropsTemp = TransactionInsuredDrops;
            var str = HtmlHelpers.DropDownList(null, "Item.TransactionInsured", TransactionInsuredDropsTemp, "DText", "DText", val, "");
            return str.ToString();
        }
        #endregion
        #region 服务人员
        #region 缓存定义
        static List<SoftProjectAreaEntity> _serviceUserDrops = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> ServiceUserDrops
        {
            get
            {
                // if (_TransactionManDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _serviceUserDrops = domain.ServiceUserDrops_GetAll().Items;

                }
                return _serviceUserDrops;
            }
        }
        #endregion
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static void ServiceUserDrops_Clear()
        {
            _serviceUserDrops = new List<SoftProjectAreaEntity>();
        }
        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase ServiceUserDrops_GetAll()
        {
            string sql = string.Format("SELECT * FROM V_Pre_User where Pre_RoleID=5");
            var resp = Query16(sql, 2);
            return resp;
        }
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void ServiceUserDrops_AddCache()
        {

            ModularOrFunCode = "CustomerAreas.Pre_User.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.ServiceUserDrops.Add(resp.Item);

        }
        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void ServiceUserDrops_UpdateCache()
        {
            ModularOrFunCode = "CustomerAreas.Pre_User.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var ServiceUserDrop = SoftProjectAreaEntityDomain.ServiceUserDrops.Where(p => p.Pre_UserID == Item.Pre_UserID).FirstOrDefault();
            SoftProjectAreaEntityDomain.ServiceUserDrops.Remove(ServiceUserDrop);
            SoftProjectAreaEntityDomain.ServiceUserDrops.Add(resp.Item);
        }

        /// <summary>
        /// 查询条件-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string QueryHtmlDropDownList_ServiceUserID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var ServiceUserIDDropsTemp = ServiceUserDrops;
            var str = HtmlHelpers.DropDownList(null, "ServiceUserID___equal", ServiceUserIDDropsTemp, "Pre_UserID", "UserName", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        /// <summary>
        /// 编辑页面-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string HtmlDropDownList_ServiceUserID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var ServiceUserIDDropsTemp = ServiceUserDrops;
            var str = HtmlHelpers.DropDownList(null, "Item.ServiceUserID", ServiceUserIDDropsTemp, "Pre_UserID", "UserName", val, "");
            return str.ToString();
        }
        #endregion
        public static void initCustomerTransactionDropDown()
        {
            #region 清空缓存
            SoftProjectAreaEntityDomain.CustomerYuefangDrop_Clear();
            SoftProjectAreaEntityDomain.TransactionBeneficiaryDrops_Clear();
            SoftProjectAreaEntityDomain.TransactionInsuredDrops_Clear();
            #endregion
            #region 约访记录
            ProjectCache.HtmlDropDownLiss.Remove("C_CustomerYuefangID");
            ProjectCache.HtmlDropDownLiss.Add("C_CustomerYuefangID", SoftProjectAreaEntityDomain.HtmlDropDownList_C_CustomerYuefangID);
            #endregion
            #region 受益人
            ProjectCache.HtmlDropDownLiss.Remove("TransactionBeneficiary");
            ProjectCache.HtmlDropDownLiss.Add("TransactionBeneficiary", SoftProjectAreaEntityDomain.HtmlDropDownList_TransactionBeneficiaryID);
            #endregion
            #region 被保险人
            ProjectCache.HtmlDropDownLiss.Remove("TransactionInsured");
            ProjectCache.HtmlDropDownLiss.Add("TransactionInsured", SoftProjectAreaEntityDomain.HtmlDropDownList_TransactionInsuredID);
            #endregion
        }
        public MyResponseBase deleteCurstomerStatic()
        {
            string sql = string.Format("update C_Customer set CustomerDataStatus=1,CustomerDeleteSmsStatus=0,UpdateUserID={0},UpdateDate=getdate(),UpdateUserName='{1}' where  C_CustomerID in ({2})",
                LoginInfo.Pre_UserID, LoginInfo.UserName, _C_CustomerIDs);
            var resp = Query16(sql, 1);
            return resp;
        }
        public MyResponseBase reDeleteCurstomerStatic()
        {
            string sql = string.Format("update C_Customer set CustomerDataStatus=0,UpdateUserID={0},UpdateDate=getdate(),UpdateUserName='{1}' where  C_CustomerID in ({2})",
                LoginInfo.Pre_UserID, LoginInfo.UserName, _C_CustomerIDs);
            var resp = Query16(sql, 1);
            return resp;
        }
        public MyResponseBase queryCurstomerByIds(string ids)
        {
            string sql = string.Format("select * from V_C_Customer  where  C_CustomerID in ({0})", ids);
            var resp = Query16(sql, 2);
            return resp;
        }
        public MyResponseBase queryUserList()
        {
            string sql = string.Format("select * from V_Pre_User where Pre_RoleID=5");
            var resp = Query16(sql, 2);
            return resp;
        }
        public MyResponseBase setCurstomerServiceUser(string serviceuserid)
        {
            string sql = string.Format("update C_Customer set ServiceUserID={0} where  C_CustomerID in ({1})", serviceuserid, _C_CustomerIDs);
            var resp = Query16(sql, 1);
            return resp;
        }
        public MyResponseBase queryCurstomerByServiceUser(int? suid)
        {
            string sql = string.Format("select * from V_C_Customer  where  ServiceUserID={0}", suid);
            var resp = Query16(sql, 2);
            return resp;
        }


        

        //查询客户删除信息

        public MyResponseBase quueryDeleteCurstomerStatic()
        {
            string sql = string.Format("select * from V_C_CustomerAllData where CustomerDataStatus=1 and CustomerDeleteSmsStatus=0 and datediff(day,UpdateDate,getdate())=0");
            var resp = Query16(sql);
            return resp;
        }
        //更新客户删除信息

        public MyResponseBase updateDeleteSmsCurstomerStatic()
        {
            string sql = string.Format("update C_Customer set CustomerDeleteSmsStatus=1 where CustomerDataStatus=1 and datediff(day,UpdateDate,getdate())=0");
            var resp = Query16(sql);
            return resp;
        }

        public string queryUserPhone(int? id)
        {
            string sql = string.Format("select * from V_Pre_User   where  Pre_UserID = {0}", id);
            var resp = Query16(sql,4);
            return resp.Item.MobilePhone;
        }
    }
}