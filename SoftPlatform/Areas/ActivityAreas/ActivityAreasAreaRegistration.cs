﻿using System.Web.Mvc;
using SoftProject.Domain;
namespace SoftPlatform.Controllers
{
    public class ActivityAreasAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ActivityAreas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ActivityAreas_default",
                "ActivityAreas/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
            ProjectCache.QueryHtmlDropDownLists.Add("MeetingRoomID", SoftProjectAreaEntityDomain.QueryHtmlDropDownList_MeetingRoomID);
            ProjectCache.HtmlDropDownLiss.Add("MeetingRoomID", SoftProjectAreaEntityDomain.HtmlDropDownList_MeetingRoomID);

        }
    }
}
