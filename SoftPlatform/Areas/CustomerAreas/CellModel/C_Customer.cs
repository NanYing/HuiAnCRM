
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：C_Customer(客户基本信息)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 客户编号
        /// </summary>
        public  int?  C_CustomerID{get;set;}

        /// <summary>
        /// 客户姓名
        /// </summary>
        public  string  CustomerName{get;set;}

        /// <summary>
        /// 性别
        /// </summary>
        public  int?  CustomerSex{get;set;}

        /// <summary>
        /// 性别
        /// </summary>
        public  string  CustomerSexText{get;set;}

        /// <summary>
        /// 出生年月
        /// </summary>
        public  DateTime?  CustomerBirthday{get;set;}

        /// <summary>
        /// 客户年龄
        /// </summary>
        public  int?  CustomerAge{get;set;}

        /// <summary>
        /// 客户等级
        /// </summary>
        public  int?  Customerlevel{get;set;}

        /// <summary>
        /// 客户等级
        /// </summary>
        public  string  CustomerLevelText{get;set;}

        /// <summary>
        /// 客户类型
        /// </summary>
        public  int?  CustomerType{get;set;}

        /// <summary>
        /// 客户类型
        /// </summary>
        public  string  CustomerTypeText{get;set;}

        /// <summary>
        /// 来源
        /// </summary>
        public  int?  CustomerSource{get;set;}

        /// <summary>
        /// 来源
        /// </summary>
        public  string  CustomerSourceText{get;set;}

        /// <summary>
        /// 服务人员
        /// </summary>
        public  int?  ServiceUserID{get;set;}

        /// <summary>
        /// Email
        /// </summary>
        public  string  CustomerEmail{get;set;}

        /// <summary>
        /// 会员卡等级
        /// </summary>
        public  int?  CustomerCardLevel{get;set;}

        /// <summary>
        /// 会员卡等级
        /// </summary>
        public  string  CustomerCardLevelText{get;set;}

        /// <summary>
        /// 头像文件名
        /// </summary>
        public  string  CustomerPhotoFileName{get;set;}

        /// <summary>
        /// 头像
        /// </summary>
        public  string  CustomerPhotoFilePath{get;set;}

        /// <summary>
        /// 手机
        /// </summary>
        public  string  CustomerPhone{get;set;}

        /// <summary>
        /// 住宅电话
        /// </summary>
        public  string  CustomerResidentialPhone{get;set;}

        /// <summary>
        /// 办公电话
        /// </summary>
        public  string  CustomerOfficePhone{get;set;}

        /// <summary>
        /// 其他
        /// </summary>
        public  string  CustomerOtherPhone{get;set;}

        /// <summary>
        /// 证件类型
        /// </summary>
        public  int?  CustomerCardType{get;set;}

        /// <summary>
        /// 证件类型
        /// </summary>
        public  string  CustomerCardTypeText{get;set;}

        /// <summary>
        /// 证件号码
        /// </summary>
        public  string  CustomerCardNo{get;set;}

        /// <summary>
        /// 民族
        /// </summary>
        public  int?  CustomerNation{get;set;}

        /// <summary>
        /// 民族
        /// </summary>
        public  string  CustomerNationText{get;set;}

        /// <summary>
        /// 客户编号
        /// </summary>
        public  int?  CustomerNo{get;set;}

        /// <summary>
        /// 识别卡号
        /// </summary>
        public  string  CustomerIdCardNo{get;set;}

        /// <summary>
        /// 录入员ID
        /// </summary>
        //public  int?  Pre_UserID{get;set;}

        /// <summary>
        /// 身高
        /// </summary>
        public  int?  CustomerStature{get;set;}

        /// <summary>
        /// 体重
        /// </summary>
        public  decimal?  CustomerWeight{get;set;}

        /// <summary>
        /// 学历
        /// </summary>
        public  int?  CustomerEducation{get;set;}

        /// <summary>
        /// 学历
        /// </summary>
        public  string  CustomerEducationText{get;set;}

        /// <summary>
        /// 爱好
        /// </summary>
        public  string  CustomerHobbies{get;set;}

        /// <summary>
        /// 备注
        /// </summary>
        public  string  CustomerRemarks{get;set;}

        /// <summary>
        /// 婚姻状况
        /// </summary>
        public  int?  CustomerMarital{get;set;}

        /// <summary>
        /// 婚姻状况
        /// </summary>
        public  string  CustomerMaritalText{get;set;}

        /// <summary>
        /// 结婚纪念日
        /// </summary>
        public  DateTime?  CustomerWedding{get;set;}

        /// <summary>
        /// 籍贯
        /// </summary>
        public  string  CustomerOrigin{get;set;}

        /// <summary>
        /// 家庭邮编
        /// </summary>
        public  string  CustomerFamily{get;set;}

        /// <summary>
        /// 家庭电话
        /// </summary>
        public  string  CustomerHomePhone{get;set;}

        /// <summary>
        /// 家庭地址
        /// </summary>
        public  string  CustomerHomeAddress{get;set;}

        /// <summary>
        /// 工作单位
        /// </summary>
        public  string  CustomerWorkUnit{get;set;}

        /// <summary>
        /// 职务
        /// </summary>
        public  string  CustomerPost{get;set;}

        /// <summary>
        /// 单位地址
        /// </summary>
        public  string  CustomerUnitAddress{get;set;}

        /// <summary>
        /// 邮编
        /// </summary>
        public  string  CustomerZipCode{get;set;}

        /// <summary>
        /// 月收入分档
        /// </summary>
        public  int?  CustomerMonthIncome{get;set;}

        /// <summary>
        /// 月收入分档
        /// </summary>
        public  string  CustomerMonthIncomeText{get;set;}

        /// <summary>
        /// 月收入金额
        /// </summary>
        public  decimal?  CustomerMonthAmount{get;set;}

        /// <summary>
        /// 客户成员表
        /// </summary>
        public  string  CustomerMembersTable{get;set;}

        /// <summary>
        /// 录入时间
        /// </summary>
        public  DateTime?  CustomerInputTime{get;set;}

        /// <summary>
        /// 录入人员
        /// </summary>
        public  string  CustomerInputMen{get;set;}

        /// <summary>
        /// 录入人ID
        /// </summary>
        public  int?  CustomerInputMenID{get;set;}

        /// <summary>
        /// 客户数据状态
        /// </summary>
        public  int?  CustomerDataStatus{get;set;}

        /// <summary>
        /// 服务人员
        /// </summary>
        public  string  ServiceUserName{get;set;}

        /// <summary>
        /// 删除短信状态
        /// </summary>
        public  int?  CustomerDeleteSmsStatus{get;set;}

        /// <summary>
        /// 约访次数
        /// </summary>
        public  int?  VisitCount{get;set;}

        /// <summary>
        /// 成交次数
        /// </summary>
        public  int?  BargainCount{get;set;}

        public SoftProjectAreaEntity C_Customer { get; set; }
        public List<SoftProjectAreaEntity> C_Customers { get; set; }
    }
}
