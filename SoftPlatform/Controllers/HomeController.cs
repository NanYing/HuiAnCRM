﻿using Aspose.Cells;
using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.CellModel;
using SoftProject.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//修改测试
//修改测试2
//软件中进行修改
namespace SoftPlatform.Controllers
{
    public class HomeController : BaseController
    {
        //Git__Test


        //public ActionResult IndexTemp()
        //{
        //    //return RedirectToAction("ProductByCategory", "P_ProductAreas/P_Product");//, new { DataTokens="P_ProductAreas" });
        //    //P_ProductAreas/P_Product/ProductByCategory
        //    //if (LoginInfo.LoginCategoryID == 1)
        //    //    return View("CompMagerIndex");
        //    //return View("FraMagerIndex");

        //    return View();
        //}


        public ActionResult Demo()
        {
            return View("Demo");
        }

        public ActionResult Drop()
        {
            return View();
        }

        public ActionResult Drop2()
        {
            return View();
        }

        public ActionResult Drop3()
        {
            return View();
        }

        public ActionResult IndexP()
        {
            return View();
        }

        public ActionResult AddNewMsg(string content)
        {
            //PollingMannger.BeginSend(new ClientData(ClientData.MsgNewInformation, "P0001",0,1,
            //    new
            //    {
            //        content
            //    }), null, null);
            return Json(new { result = "发送完成!" });
        }


        public ActionResult Index()
        {
            #region 日期格式

            var dt = DateTime.Now;
            var dtbytes = dt.ToBytesGater();
            dt = dtbytes.ToDateTimeGater();

            DateTime? dtnull = dt;
            var dtnullbytes = dtnull.ToBytesGater();
            dtnull = dtnullbytes.ToDateTimeNullGater();

            #endregion

            #region 4整数

            var int4 = 1234;
            var int4bytes = int4.ToBytes4();
            int4 = int4bytes.ToInt32();

            int? intnull4 = int4;
            var int4nullbyte = intnull4.ToBytes4();
            intnull4 = int4nullbyte.ToInt32();

            #endregion

            #region 2整数

            var int2 = 1234;
            var int2bytes = int2.ToBytes2();
            int2= int2bytes.ToInt32(0,2);

            int? int2null = int2;
            var int2nullbyte = int2null.ToBytes2();
            int2null = int2nullbyte.ToInt32(0,2);

            #endregion


            #region 小数

            var dec = 1234;
            var decbytes = dec.ToBytes2();
            Decimal dec1 = decbytes.ToDec(0, 2, 1);
            Decimal dec2 = decbytes.ToDec(0, 2, 2);
            Decimal dec3 = decbytes.ToDec(0, 2, 3);

            #endregion

            #region 旧代码

            //return RedirectToAction("ProductByCategory", "P_ProductAreas/P_Product");//, new { DataTokens="P_ProductAreas" });
            //P_ProductAreas/P_Product/ProductByCategory
            //if (LoginInfo.LoginCategoryID == 1)
            //    return View("CompMagerIndex");
            //return View("FraMagerIndex");

            //HtmlHelpersProject.PageFormEleTypes
            //HtmlHelpersProject.SortFormEleTypes
            //HtmlHelpersProject.QueryFormEleTypes
            //Excel();

            #endregion

            if (!string.IsNullOrEmpty(LoginInfo.RoleHomePageUrl))
                return Redirect(LoginInfo.RoleHomePageUrl);
            return View();
            //return Redirect("/CustomerAreas/C_Customer/Index");
        }

        public void Excel()
        {
            string outName = "";// table.TableName;
            Workbook workBook = new Workbook();
            workBook.Worksheets.Clear();
            workBook.Worksheets.Add(outName);//New Worksheet是Worksheet的name
            Worksheet ws = workBook.Worksheets[0];

            workBook.Worksheets[0].AutoFitColumns();
            var modulars = ProjectCache.Design_ModularOrFuns.Where(p => p.ParentPremID == 0).ToList();
            var k = 0;
            for (int i = 0; i < modulars.Count(); i++)
            {
                k++;
                var modular = modulars[i];
                #region 主表
                //(1)字段
                var ttt = HtmlHelpersProject.PageFormEleTypes(modular).Where(p => p.PageFormEleSort != 1 && p.PageFormEleSort != 100 && p.PageFormEleSort != null && p.FormEleType != 8)
                          .OrderBy(p => p.Page01FormEleSort);
                //PageFormEleSort = p.Page01FormEleSort; p.PageFormElePos = p.Page01FormElePos; p.FormEleType
                var DispNameCn = string.Join(",", ttt.Select(p => p.NameCn));
                //(2)查询条件
                var mmm = HtmlHelpersProject.QueryFormEleTypes(modular).Where(p => p.PageFormEleSort != null);
                var queryNameCn = string.Join(",", mmm.Select(p => p.NameCn));
                //按钮
                ws.Cells[k, 0].PutValue(modular.ModularName);
                ws.Cells[k, 1].PutValue(DispNameCn);
                ws.Cells[k, 2].PutValue(queryNameCn);
                #endregion
                #region 子模块

                var modularsChilds = ProjectCache.Design_ModularOrFuns.Where(p => p.ParentPremID == modular.Design_ModularOrFunID).ToList();
                for (var j = 0; j < modularsChilds.Count; j++)
                {
                    k++;
                    modular = modularsChilds[j];
                    ttt = HtmlHelpersProject.PageFormEleTypes(modular).Where(p => p.PageFormEleSort != 1 && p.PageFormEleSort != 100 && p.PageFormEleSort != null && p.FormEleType != 8)
                          .OrderBy(p => p.Page01FormEleSort);
                    //PageFormEleSort = p.Page01FormEleSort; p.PageFormElePos = p.Page01FormElePos; p.FormEleType
                    DispNameCn = string.Join(",", ttt.Select(p => p.NameCn));
                    //(2)查询条件
                    mmm = HtmlHelpersProject.QueryFormEleTypes(modular).Where(p => p.PageFormEleSort != null);
                    queryNameCn = string.Join(",", mmm.Select(p => p.NameCn));
                    //按钮
                    ws.Cells[k, 0].PutValue(modular.ModularName);
                    ws.Cells[k, 1].PutValue(DispNameCn);
                    ws.Cells[k, 2].PutValue(queryNameCn);
                }
                #endregion
            }
            workBook.Worksheets[0].AutoFitColumns();
            workBook.Save("C:\\aaa.xls");
        }

        public ActionResult CompMagerIndex()
        {
            MyResponseBase resp = new MyResponseBase();
            var domain = new SoftProjectAreaEntityDomain();
            resp = domain.Doc_Docment_Home();
            return View(resp);
        }

        public ActionResult FraMagerIndex()
        {
            MyResponseBase resp = new MyResponseBase();
            var domain = new SoftProjectAreaEntityDomain();
            resp = domain.Doc_Docment_Home();
            return View(resp);
        }

        public ActionResult AuthorizationPanel()
        {
            MenuCode = "AuthorizationPanel";
            var resp = new MyResponseBase();
            return View("SubPanel", resp);
        }
    }
}
