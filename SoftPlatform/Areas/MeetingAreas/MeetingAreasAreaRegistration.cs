﻿using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    public class MeetingAreasAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MeetingAreas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MeetingAreas_default",
                "MeetingAreas/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
