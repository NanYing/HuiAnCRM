﻿using Framework.Core;
using Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：Design_ModularFieldTable(表或实体)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 表或实体ID
        /// </summary>
        public int? Design_ModularFieldTableID { get; set; }

        /// <summary>
        /// 表或实体序号
        /// </summary>
        public int? ModularFieldTableSort { get; set; }

        /// <summary>
        /// 表或实体区域名称
        /// </summary>
        public string ModularFieldTableAreasCode { get; set; }

        /// <summary>
        /// 表或实体名称
        /// </summary>
        public string Design_ModularFieldTableName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Design_ModularFieldTableNameCN { get; set; }

        /// <summary>
        /// 功能模块字段列表
        /// </summary>
        public List<SoftProjectAreaEntity> Design_ModularFieldTables { get; set; }

    }
}
