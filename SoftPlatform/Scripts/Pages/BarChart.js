var myec; 
initDrawStdBar = function () {
    //配置
    require.config({
        paths: {
            echarts: '/Scripts/echarts' //echarts路径
        }
    });
    require(//按需加载
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],
       
    function (ec) {
        myec = ec;      
    }
    );
}

DrawStdBar = function (datas, divID, legend, title, subtitle) {
        setTimeout(100);
        //基于准备好的dom,初始化echart图表
        var myChart = myec.init(document.getElementById(divID));
       
        //分离出：Xdata值为x轴数据,Ydata值为y轴数据
        var xdata = [];
        var ydata = [];
        for (var i = 0; i < datas.Data.Items.length; i++) {
            xdata.push(datas.Data.Items[i].Xdata);
            ydata.push(datas.Data.Items[i].Ydata);
        }
        //定义图表option
        var option = {
            //标题，每个图表最多仅有一个标题控件，每个标题控件可设主副标题
            title: {
                text: title,//主标题文本
                subtext: subtitle//副标题文本
            },
            tooltip: {
                trigger: 'axis'//tooltip触发方式:axis以X轴线触发,item以每一个数据项触发
            },
            legend: {//图例，每个图表最多仅有一个图例
                data: legend
            },
            toolbox: {//工具箱，每个图表最多仅有一个工具箱
                show: true,//是否显示工具栏 
                feature: {
                    //辅助线标志  
                    mark: { show: false },
                    //dataZoom，框选区域缩放，自动与存在的dataZoom控件同步，分别是启用，缩放后退  
                    dataZoom: {
                        show: true,
                        title: {
                            dataZoom: '区域缩放',
                            dataZoomReset: '区域缩放后退'
                        }
                    },
                    dataView: { show: true, readOnly: false },//数据预览
                    restore: { show: true },//复原
                    magicType: { show: true, type: ['line', 'bar'] }, //支持柱形图和折线图的切换
                    //是否启用拖拽重计算特性，默认关闭(即值为false)  
                    calculable: true,
                    saveAsImage: { show: true }//是否保存图片
                }
            },
            //直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
            //横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
            xAxis: [//X轴均为category，Y轴均为value
                {
                    //显示策略，可选为：true（显示） | false（隐藏），默认值为true  
                    show: true,
                    //坐标轴类型，横轴默认为类目型'category'  
                    type: 'category',
                    //类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行  
                    data: xdata,
                    //boundaryGap: false,//数值轴两端的空白策略 
                    axisLabel: {//X轴刻度配置
                        show: true,
                        interval: 'auto',//0：表示全部显示不间隔；auto:表示自动根据刻度个数和宽度自动设置间隔个数
                        rotate: 0,//标签旋转角度，默认为0，不旋转，正直为逆时针，负值为顺时针，可选为：-90 ~ 90
                        margin: 8//坐标轴文本标签与坐标轴的间距，默认为8，单位px
                    }
                }
            ],
            //直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
            //纵轴通常为数值型，但条形图时则纵轴为类目型  
            yAxis: [
                {
                    //显示策略，可选为：true（显示） | false（隐藏），默认值为true  
                    show: true,
                    //坐标轴类型，纵轴默认为数值型'value'  
                    type: 'value',
                    //分隔区域，默认不显示  
                    splitArea: { show: true }
                }
            ],
            //sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象
            series: [
                {
                    //系列名称，如果启用legend，该值将被legend.data索引相关  
                    name: legend[0],
                    //图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
                    type: 'bar',
                    //系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值
                    data: ydata,
                    //系列中的数据标注内容  
                    markPoint: {
                        data: [
                            { type: 'max', name: '最大值' },
                            { type: 'min', name: '最小值' }
                        ]
                    },
                    //系列中的数据标线内容  
                    markLine: {
                        data: [
                            { type: 'average', name: '平均值' }
                        ]
                    }
                }
            ]
        };
        //为echarts对象加载数据
        myChart.setOption(option);
    }
