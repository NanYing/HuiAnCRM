
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：FD_Order(订单管理)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 订单ID
        /// </summary>
        public  int?  FD_OrderID{get;set;}

        /// <summary>
        /// 订单日期
        /// </summary>
        public  DateTime?  FDOrderDate{get;set;}

        /// <summary>
        /// 订单金额
        /// </summary>
        public  decimal?  FDOrderAmount{get;set;}

        public SoftProjectAreaEntity FD_Order { get; set; }
        public List<SoftProjectAreaEntity> FD_Orders { get; set; }
    }
}
