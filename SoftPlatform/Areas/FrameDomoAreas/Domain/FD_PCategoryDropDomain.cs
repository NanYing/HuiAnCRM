﻿using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：FD_PCategoryDrop(商品类别下拉框架演示)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 缓存字段和属性
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        #region 缓存定义
        static List<SoftProjectAreaEntity> _FD_PCategoryDrops = new List<SoftProjectAreaEntity>();

        public static List<SoftProjectAreaEntity> FD_PCategoryDrops
        {
            get
            {
                if (_FD_PCategoryDrops.Count == 0)
                {
                    SoftProjectAreaEntityDomain domain = new SoftProjectAreaEntityDomain();
                    _FD_PCategoryDrops = domain.FD_PCategoryDrop_GetAll().Items;
                }
                return _FD_PCategoryDrops;
            }
        }

        #endregion

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static void FD_PCategoryDrops_Clear()
        {
            _FD_PCategoryDrops = new List<SoftProjectAreaEntity>();
        }


        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public MyResponseBase FD_PCategoryDrop_GetAll()
        {
            string sql = "SELECT * FROM V_FD_PCategoryDrop ";
            var resp = Query16(sql, 2);
            return resp;
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void FD_PCategoryDrop_AddCache()
        {

            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFunOrgs.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            SoftProjectAreaEntityDomain.FD_PCategoryDrops.Add(resp.Item);

        }

        /// <summary>
        /// 更新缓存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public void FD_PCategoryDrop_UpdateCache()
        {
            ModularOrFunCode = "FrameDomoAreas.FD_PCategoryDrop.Index";
            //Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            resp = ByID();
            var FD_PCategoryDrop = SoftProjectAreaEntityDomain.FD_PCategoryDrops.Where(p => p.FD_PCategoryDropID == Item.FD_PCategoryDropID).FirstOrDefault();

            SoftProjectAreaEntityDomain.Pre_Roles.Remove(FD_PCategoryDrop);
            SoftProjectAreaEntityDomain.FD_PCategoryDrops.Add(resp.Item);
        }

        /// <summary>
        /// 查询条件-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string QueryHtmlDropDownList_FD_PCategoryDropID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var FD_PCategoryDropsTemp = FD_PCategoryDrops;
            var str = HtmlHelpers.DropDownList(null, "FD_PCategoryDropID___equal", FD_PCategoryDropsTemp, "FD_PCategoryDropID", "PCategoryDropName", val, "", "==" + NameCn + "==");
            var strDrop = str.ToString();
            return strDrop;
        }

        /// <summary>
        /// 编辑页面-下拉列表框
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static string HtmlDropDownLiss_FD_PCategoryDropID(string val, string NameCn, SoftProjectAreaEntity item)
        {
            var FD_PCategoryDropsTemp = FD_PCategoryDrops;
            var str = HtmlHelpers.DropDownList(null, "Item.FD_PCategoryDropID", FD_PCategoryDropsTemp, "FD_PCategoryDropID", "PCategoryDropName", val, "");
            return str.ToString();
        }


    }
}
