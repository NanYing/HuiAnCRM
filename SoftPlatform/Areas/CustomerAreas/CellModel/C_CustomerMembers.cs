
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：C_CustomerMembers(客户成员信息)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 客户成员编号
        /// </summary>
        public  int?  C_CustomerMembersID{get;set;}

        /// <summary>
        /// 客户编号
        /// </summary>
        //public  int?  C_CustomerID{get;set;}

        /// <summary>
        /// 与客户关系
        /// </summary>
        public  int?  C_CustomerRelate{get;set;}

        /// <summary>
        /// 与客户关系
        /// </summary>
        public  string  CustomerRelateText{get;set;}

        /// <summary>
        /// 姓名
        /// </summary>
        public  string  MemberName{get;set;}

        /// <summary>
        /// 性别
        /// </summary>
        public  int?  MemberSex{get;set;}

        /// <summary>
        /// 性别
        /// </summary>
        //public  string  CustomerSexText{get;set;}

        /// <summary>
        /// 出生年月
        /// </summary>
        public  DateTime?  MemberBirthday{get;set;}

        /// <summary>
        /// 年龄
        /// </summary>
        public  int?  MemberAge{get;set;}

        /// <summary>
        /// 手机
        /// </summary>
        public  string  MemberPhone{get;set;}

        /// <summary>
        /// 住宅电话
        /// </summary>
        public  string  MemberResidentialPhone{get;set;}

        /// <summary>
        /// 办公电话
        /// </summary>
        public  string  MemberOfficePhone{get;set;}

        /// <summary>
        /// 其他
        /// </summary>
        public  string  MemberOtherPhone{get;set;}

        /// <summary>
        /// Email
        /// </summary>
        public  string  MemberEmail{get;set;}

        /// <summary>
        /// 证件类型
        /// </summary>
        public  int?  MemberCardType{get;set;}

        /// <summary>
        /// 证件类型
        /// </summary>
        public  string  MemberCardTypeText{get;set;}

        /// <summary>
        /// 证件号码
        /// </summary>
        public  string  MemberCardNo{get;set;}

        /// <summary>
        /// 民族
        /// </summary>
        public  int?  MemberNation{get;set;}

        /// <summary>
        /// 民族
        /// </summary>
        public  string  NationText{get;set;}

        /// <summary>
        /// 录入人ID
        /// </summary>
        //public  int?  Pre_UserID{get;set;}

        /// <summary>
        /// 录入人
        /// </summary>
        public  string  MemberInputMen{get;set;}

        /// <summary>
        /// 录入时间
        /// </summary>
        public  DateTime?  MemberInputTime{get;set;}

        /// <summary>
        /// 头像文件名
        /// </summary>
        public  string  MemberPhotoFileName{get;set;}

        /// <summary>
        /// 头像
        /// </summary>
        public  string  MemberPhotoFilePath{get;set;}

        /// <summary>
        /// 身高
        /// </summary>
        public  int?  MemberStature{get;set;}

        /// <summary>
        /// 体重
        /// </summary>
        public  decimal?  MemberWeight{get;set;}

        /// <summary>
        /// 学历
        /// </summary>
        public  int?  MemberEducation{get;set;}

        /// <summary>
        /// 学历
        /// </summary>
        public  string  MemberEducationText{get;set;}

        /// <summary>
        /// 爱好
        /// </summary>
        public  string  MemberHobbies{get;set;}

        /// <summary>
        /// 备注
        /// </summary>
        public  string  MemberRemarks{get;set;}

        /// <summary>
        /// 工作单位
        /// </summary>
        public  string  MemberWorkUnit{get;set;}

        /// <summary>
        /// 职务
        /// </summary>
        public  string  MemberPost{get;set;}

        /// <summary>
        /// 单位地址
        /// </summary>
        public  string  MemberUnitAddress{get;set;}

        /// <summary>
        /// 邮编
        /// </summary>
        public  string  MemberZipCode{get;set;}

        /// <summary>
        /// 月收入分档
        /// </summary>
        public  int?  MemberMonthIncome{get;set;}

        /// <summary>
        /// 月收入分档
        /// </summary>
        public  string  MemberMonthIncomeText{get;set;}

        /// <summary>
        /// 月收入金额
        /// </summary>
        public  decimal?  MemberMonthAmount{get;set;}

        public SoftProjectAreaEntity C_CustomerMembers { get; set; }
        public List<SoftProjectAreaEntity> C_CustomerMemberss { get; set; }
    }
}
