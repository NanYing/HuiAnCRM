﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.CellModel;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：FD_OrderDetail(订单明细框架演示)
    /// </summary>
    public class FD_OrderDetailController : BaseController
    {
        public FD_OrderDetailController()
        {
        }

        /// <summary>
        /// 订单明细框架演示--编辑列表
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexEdit(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            ModularOrFunCode = "FrameDomoAreas.FD_OrderDetail.IndexEdit";
            domain.ModularOrFunCode = ModularOrFunCode;

            if (domain.Item.FD_OrderID == null)
            {
                //添加时:初始化汇总字段
                resp.ItemTotal = new SoftProjectAreaEntity { Number = 0, FDTotalPrice=0 };
                return View(Design_ModularOrFun.MainView, resp);
            }
            domain.Querys.Add(new Query { QuryType = 0, FieldName = "FD_OrderID___equal", Value = domain.Item.FD_OrderID.ToString() });
            resp = domain.QueryIndex();

            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 订单明细框架演示--查看列表
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexDetail(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            ModularOrFunCode = "FrameDomoAreas.FD_OrderDetail.IndexDetail";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (domain.Item.FD_OrderID == null)
                return View(Design_ModularOrFun.MainView, resp);

            domain.Querys.Add(new Query { QuryType = 0, FieldName = "FD_OrderID___equal", Value = domain.Item.FD_OrderID.ToString() });
            resp = domain.QueryIndex();
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 订单明细框架演示--Rows
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Rows(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.FD_OrderDetail_Rows();
            //var resp = new MyResponseBase();
            ModularOrFunCode = "FrameDomoAreas.FD_OrderDetail.IndexEdit";

            return View("Rows", resp);
        }

    }
}
