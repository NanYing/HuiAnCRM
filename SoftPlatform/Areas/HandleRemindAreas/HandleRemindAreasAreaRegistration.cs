﻿using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    public class HandleRemindAreasAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HandleRemindAreas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HandleRemindAreas_default",
                "HandleRemindAreas/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
