﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Core
{
    public static class DateTimeExt
    {
        /// <summary>
        /// 日期间相差月份
        /// </summary>
        /// <param name="dateA"></param>
        /// <param name="dateB"></param>
        /// <returns></returns>
        public static int GetMonth(this DateTime? dateA, DateTime? dateB)
        {
            if (dateA == null || dateB == null)
            {
                return 0;
            }
            var date1 = (DateTime)dateA;
            var date2 = (DateTime)dateB;
            int year1 = date1.Year;
            int year2 = date2.Year;
            int month1 = date1.Month;
            int month2 = date2.Month;
            int months = 12 * (year2 - year1) + (month2 - month1);
            return months;
        }

        public static int GetDay(this DateTime? dateA, DateTime? dateB)
        {
            var dateATemp = Convert.ToDateTime(dateA);
            var dateBTemp = Convert.ToDateTime(dateB);
            var day = dateATemp.Subtract(dateBTemp);
            return day.Days;
        }


        /// <summary>
        /// 转换为：yyyy-MM-dd
        /// </summary>
        /// <param name="dateA"></param>
        /// <returns></returns>
        public static string Format_yyyy_MM_dd(this DateTime? dateA)
        {
            var strFmt = "";
            strFmt=dateA == null ? "" : Convert.ToDateTime(dateA).ToString("yyyy-MM-dd");
            return strFmt;
        }

        /// <summary>
        /// 转换为：yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="dateA"></param>
        /// <returns></returns>
        public static string Format_yyyy_MM_dd_HH_mm_ss(this DateTime dateA)
        {
            var strFmt = "";
            strFmt = dateA == null ? "" : dateA.ToString("yyyy-MM-dd HH:mm:ss");
            return strFmt;
        }

        /// <summary>
        /// 转换为：yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="dateA"></param>
        /// <returns></returns>
        public static string Format_yyyy_MM_dd_HH_mm_ss(this DateTime? dateA)
        {
            var strFmt = "";
            strFmt = dateA == null ? "" : Convert.ToDateTime(dateA).ToString("yyyy-MM-dd HH:mm:ss");
            return strFmt;
        }

        /// <summary>
        /// 转换为：yyyy-MM-dd HH:mm
        /// </summary>
        /// <param name="dateA"></param>
        /// <returns></returns>
        public static string Format_yyyy_MM_dd_HH_mm(this DateTime dateA)
        {
            var strFmt = "";
            strFmt = dateA == null ? "" : dateA.ToString("yyyy-MM-dd HH:mm");
            return strFmt;
        }

        /// <summary>
        /// 转换为：yyyy-MM-dd HH:mm
        /// </summary>
        /// <param name="dateA"></param>
        /// <returns></returns>
        public static string Format_yyyy_MM_dd_HH_mm(this DateTime? dateA)
        {
            var strFmt = "";
            strFmt = dateA == null ? "" : Convert.ToDateTime(dateA).ToString("yyyy-MM-dd HH:mm");
            return strFmt;
        }

        /// <summary>
        /// 将日期转换为byte数组
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this DateTime? date)
        {
            var dt = DateTime.Now;
            if (date != null)
                dt = (DateTime)date;
            var bytes = dt.ToBytes();
            return null;
        }

        /// <summary>
        /// 将日期转换为byte数组，采集
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static byte[] ToBytesGater(this DateTime? date)
        {
            DateTime dt=(DateTime)date;
            var dtArr = dt.ToBytesGater();
            return dtArr;
        }

        /// <summary>
        /// 将日期转换为byte数组，采集
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static byte[] ToBytesGater(this DateTime date)
        {
            DateTime dt = date;
            var mmmm= dt.ToBytes();

            byte[] dtArr = new byte[8];
            var hex = dt.Year.ToBytes2();
            dtArr[0] = hex[0];
            dtArr[1] = hex[1];

            dtArr[2] = dt.Month.ToBytes1();
            dtArr[3] = dt.Day.ToBytes1();

            dtArr[4] = dt.Hour.ToBytes1();
            dtArr[5] = dt.Minute.ToBytes1();
            dtArr[6] = dt.Second.ToBytes1();
            dtArr[7] =((int)dt.DayOfWeek).ToBytes1();
            return dtArr;
        }

        /// <summary>
        /// 将日期转换为byte数组
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this DateTime dt)
        {
            return BitConverter.GetBytes(dt.Ticks);
        }
    }


}
