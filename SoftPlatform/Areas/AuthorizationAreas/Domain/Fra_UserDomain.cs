﻿
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftPlatform.Controllers;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

//namespace Framework.Web.Mvc
namespace SoftProject.Domain
{
    /// <summary>
    /// 业务层：Pre_User(用户管理)
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        /// <summary>
        /// 添加加盟商管理员
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_User_AddAdminSave()
        {
            var resp = new MyResponseBase();
            //查询企业类型为2的管理员角色
            var Pre_RoleID = SoftProjectAreaEntityDomain.Pre_Roles.Where(p => p.LoginCategoryID == 2 && p.BAdmin == 1).FirstOrDefault().Pre_RoleID;
            if (Pre_RoleID == null)
                throw new Exception("企业管理员角色还不有添加");
            //(1)验证
            ValidateMobilePhoneAdd();
            ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                Pre_User_Domain();
                Sys_HOperControl = null;
                Item.Pre_RoleID = Pre_RoleID;
                Item.UserCategoryID = 1;
                Item.UserStatuID = 1;
                Item.LoginCategoryID = 2;
                OperCode = "Pre_User.AddAdminSave";
                Item.UserName = Item.PreCompanyName2;
                resp = Execute();
            }));

            //(3)添加到缓存
            Item.Pre_UserID = resp.Item.Pre_UserID;
            Pre_User_AddCache();
            return resp;
        }

        /// <summary>
        /// 编辑加盟商管理员
        /// </summary>
        /// <returns></returns>
        public MyResponseBase Pre_User_EditAdminSave()
        {
            var resp = new MyResponseBase();
            //查询企业类型为2的管理员角色
            var Pre_RoleID = SoftProjectAreaEntityDomain.Pre_Roles.Where(p => p.LoginCategoryID == 2 && p.BAdmin == 1).FirstOrDefault().Pre_RoleID;
            //(1) 验证
            ValidateMobilePhoneUpdate();
            resp = ExecuteDelegate(new Action<SoftProjectAreaEntityDomain>(p =>
            {
                Pre_User_Domain();
                Sys_HOperControl = null;
                Item.Pre_RoleID = Pre_RoleID;
                Item.UserName = Item.PreCompanyName2;
                OperCode = "Pre_User.EditAdminSave";
                resp = Execute();
            }));
            Pre_User_UpdateCache();
            return resp;
        }

    }
}
