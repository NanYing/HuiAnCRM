
using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftProject.CellModel
{
    /// <summary>
    /// 表：FD_PCategoryDrop(商品类别下拉框架演示)
    /// </summary>
    public partial class SoftProjectAreaEntity
    {
        /// <summary>
        /// 商品类别下拉框架演示ID
        /// </summary>
        public  int?  FD_PCategoryDropID{get;set;}

        /// <summary>
        /// 商品类别下拉框架演示名称
        /// </summary>
        public  string  PCategoryDropName{get;set;}

        /// <summary>
        /// 排序号
        /// </summary>
        public  int?  PCategoryDropSort{get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        public  int?  PCategoryDropStatuID{get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        public  string  PCategoryDropStatuName{get;set;}

        public SoftProjectAreaEntity FD_PCategoryDrop { get; set; }
        public List<SoftProjectAreaEntity> FD_PCategoryDrops { get; set; }
    }
}
