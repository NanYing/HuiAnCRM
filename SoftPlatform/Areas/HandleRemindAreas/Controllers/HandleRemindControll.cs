﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：HandleRemind(处理提醒)
    /// </summary>
    public class HandleRemindController : BaseController
    {
        public HandleRemindController()
        {
        }

        /// <summary>
        /// 处理提醒--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "HandleRemindAreas.HandleRemind.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            domain.Querys.Add(new Query { QuryType = 0, FieldName = "Pre_UserID___equal", Value = domain.Item.Pre_UserID.ToString() });
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 处理提醒--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            ModularOrFunCode = "HandleRemindAreas.HandleRemind.Add";
            resp.Item.Pre_UserID = LoginInfo.Pre_UserID;
            resp.Item.Status = 0;
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;

            ProjectCache.HtmlDropDownLiss.Remove("C_CustomerID");
            ProjectCache.HtmlDropDownLiss.Add("C_CustomerID", SoftProjectAreaEntityDomain.HtmlDropDownList_C_CustomerID);
           
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 处理提醒--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 初始值
            #endregion
            ModularOrFunCode = "HandleRemindAreas.HandleRemind.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.HandleRemind_AddSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 处理提醒--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        /// <summary>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "HandleRemindAreas.HandleRemind.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }
        
          /// <summary>
        /// 客户信息--获取提醒
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public HJsonResult GetHandleRemind(SoftProjectAreaEntityDomain domain)
        {

            #region 初始值

            #endregion
            ModularOrFunCode = "CustomerAreas.C_Customer.GetHandleRemind";
            domain.ModularOrFunCode = ModularOrFunCode;
           // string Pre_UserID = Request.QueryString["Pre_UserID"];
           // SoftProjectAreaEntityDomain.C_CustomerIDs = C_CustomerID;
            //int? x = domain.Item.C_CustomerID;
            var resp = domain.getHandleRemind();
            return new HJsonResult(new { Data = resp });
        }


    }
}