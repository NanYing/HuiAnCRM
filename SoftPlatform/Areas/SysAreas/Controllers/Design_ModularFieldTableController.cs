using Framework.Core;
using Framework.Web.Mvc;
using Framework.Web.Mvc.Sys;
using SoftProject.CellModel;
using SoftProject.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    public class Design_ModularFieldTableController : BaseController
    {
        public Design_ModularFieldTableController()
        {
        }

        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Design_ModularFieldTable_Index();
            return View(resp);
        }

        [HttpGet]
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();
            resp.FunNameEn = "Add";
            return View("Add", resp);
        }

        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Design_ModularFieldTable_AddSave();
            return new HJsonResult(new { Data = resp });
        }


        #region 编辑

        [HttpGet]
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            var resp = new MyResponseBase();// domain.Design_ModularFieldTable_GetByID();
            resp.FunNameEn = "Edit";
            //resp.FunNameCn = "编辑";
            //resp.FunBtnNameCn = "保存";
            //resp.ModularOrFunCode = "AuthorizationAreas.De_MemberNewP.Edit";
            //if (resp.Item.GroupModularOrFun == 3)
            //    return View("EditFun", resp);
            return View("Edit", resp);
        }

        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Design_ModularFieldTable_EditSave();
            return new HJsonResult(new { Data = resp });
        }

        #endregion

    }
}

