﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Core
{
    public static class ByteExt
    {
        /// <summary>
        /// 转换成整数
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static Int32 ToInt32(this byte[] bytes, int offset=0, int length=4)
        {
            var value = 0;
            switch (length)
            {
                case 4:
                    value = BitConverter.ToInt32(bytes, offset);
                    break;
                case 2:
                    value = BitConverter.ToInt16(bytes, offset);
                    break;
                case 1:
                    value = bytes[offset];
                    break;
            }
            return value;
        }

        /// <summary>
        /// 转换成整数
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static Int32 ToInt32(this byte bytes, int offset = 0)
        {
            var value = 0;
            value = bytes;
            return value;
        }

        /// <summary>
        /// 转换成日期
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static decimal ToDec(this byte[] bytes, int offset=0, int length=1, int Digit=1)
        {
            var val = bytes.ToInt32(offset, length);
            decimal decValue = val;
            switch (Digit)
            {
                case 1:
                    decValue = decValue *0.1M;
                    break;
                case 2:
                    decValue = decValue *0.01M;
                    break;
                case 3:
                    decValue = decValue *0.001M;
                    break;
                case 4:
                    decValue = decValue *0.0001M;
                    break;
            }
            return decValue;
        }

        public static DateTime ToDateTime(this byte[] bytes, int offset=0)
        {
            if (bytes != null)
            {
                long ticks = BitConverter.ToInt64(bytes, offset);
                if (ticks < DateTime.MaxValue.Ticks && ticks > DateTime.MinValue.Ticks)
                {
                    DateTime dt = new DateTime(ticks);
                    return dt;
                }
            }
            return new DateTime();
        }

        /// <summary>
        /// 转换为DateTime
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static DateTime ToDateTimeGater(this byte[] bytes, int offset = 0)
        {
            var dt = DateTime.Now;
            if (bytes != null)
            {
                var year=bytes.ToInt32(offset, 2);
                int month = bytes[offset + 2];
                int day = bytes[offset + 3];
                int HH = bytes[offset + 4];
                int mi = bytes[offset + 5];
                int hh = bytes[offset + 6];

                dt = new DateTime(year, month, day,HH,mi,hh);
            }
            return dt;
        }

        /// <summary>
        /// 转换为DateTime?
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static DateTime? ToDateTimeNullGater(this byte[] bytes, int offset = 0)
        {
            var dt = DateTime.Now;

            if (bytes != null)
            {
                var year = bytes.ToInt32(offset, 2);
                int month = bytes[offset + 2];
                int day = bytes[offset + 3];
                int HH = bytes[offset + 4];
                int mi = bytes[offset + 5];
                int hh = bytes[offset + 6];

                dt = new DateTime(year, month, day, HH, mi, hh);
            }
            return dt;
        }

        public static string ToString(byte[] bytes, int offset=0,int count=1)
        {
            string str = System.Text.Encoding.Default.GetString(bytes, offset,count);
            return str;
        }

        public static string ToStringASCII(byte[] bytes, int offset=0,int count=1)
        {
            string str = System.Text.Encoding.ASCII.GetString(bytes,offset,count);
            return str;
        }

        //public static string ToHexString(byte[] bytes)
        //{
        //    string hexString = string.Empty;
        //    if (bytes != null)
        //    {
        //        StringBuilder strB = new StringBuilder();
        //        for (int i = 0; i < bytes.Length; i++)
        //        {
        //            strB.Append(bytes[i].ToString("X2"));
        //        }
        //        hexString = strB.ToString();
        //    } 
        //    return hexString;
        //}

        ///// <summary>
        ///// 字段转换为16进制
        ///// </summary>
        ///// <param name="bytes"></param>
        ///// <returns></returns>
        //public static string ToHexString(this byte[] bytes)
        //{
        //    string byteStr = string.Empty;
        //    if (bytes != null || bytes.Length > 0)
        //    {
        //        foreach (var item in bytes)
        //        {
        //            byteStr += string.Format("{0:X2}", item);
        //        }
        //    }
        //    return byteStr;
        //}

        ///// <summary>
        ///// 字段转换为16进制
        ///// </summary>
        ///// <param name="bytes"></param>
        ///// <returns></returns>
        public static string ToHexString(this byte[] InBytes, int offset, int len)
        {
            string StringOut = "";
            len = offset + len;
            for (; offset < len; offset++)
            {
                StringOut = StringOut + String.Format("{0:X2} ", InBytes[offset]);
            }
            return StringOut;
        }
    }
}
