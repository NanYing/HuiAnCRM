﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：Pre_Company(公司管理)
    /// </summary>
    public class Pre_CompanyController : BaseController
    {
        public Pre_CompanyController()
        {
        }

        /// <summary>
        /// 公司管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "AuthorizationAreas.Pre_Company.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            domain.Item.Pre_CompanyID = LoginInfo.Pre_CompanyID;
            var resp = domain.ByID();

            ModularOrFunCode = "AuthorizationAreas.Pre_Company.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 公司管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            domain.Item.CompanyCategoryID = 1;
            ModularOrFunCode = "AuthorizationAreas.Pre_Company.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;

            var resp = domain.EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 公司管理--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "AuthorizationAreas.Pre_Company.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            domain.Item.Pre_CompanyID = LoginInfo.Pre_CompanyID;
            var resp = domain.ByID();

            ModularOrFunCode = "AuthorizationAreas.Pre_Company.Detail";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

    }
}
