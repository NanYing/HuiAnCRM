﻿
using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.CellModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace SoftProject.Domain
{
    /// <summary>
    /// 软件项目：业务层
    /// </summary>
    public partial class SoftProjectAreaEntityDomain
    {
        #region 公共部分

        //登录人信息
        public SoftProjectAreaEntity LoginInfo
        {
            get
            {
                if (HttpContext.Current.Session != null)
                    return HttpContext.Current.Session["LoginInfo"] as SoftProjectAreaEntity;
                return new SoftProjectAreaEntity();
            }
        }

        public static SoftProjectAreaEntity LoginInfostatic
        {
            get
            {
                if (HttpContext.Current.Session != null)
                    return HttpContext.Current.Session["LoginInfo"] as SoftProjectAreaEntity;
                return new SoftProjectAreaEntity();
            }
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public SoftProjectAreaEntityDomain()
        {
            //UIDatas = new Dictionary<string, List<UIData>>();
            //_P_GatherDatas = new Dictionary<string, List<SoftProjectAreaEntity>>();

            //排序
            RAInfo = new RespAttachInfo();
            //分页信息
            PageQueryBase = new PageQueryBase();
            //查询条件信息
            _Querys = new Querys();
            //单个实体初始化
            this.Item = new SoftProjectAreaEntity();
            //实体集合初始化
            this.Items = new List<SoftProjectAreaEntity>();
        }

        /// <summary>
        /// 默认Model对象
        /// </summary>
        public MyResponseBase Default()
        {
            var resp = new MyResponseBase { Item = new SoftProjectAreaEntity() };
            return resp;
        }

        /// <summary>
        /// 生成查询语句的数据库
        /// </summary>
        public void BulidHOperControl()
        {
            Sys_HOperControl = new SoftProjectAreaEntity
            {
                DBTSql = ProjectCache.GetQuerySql(TabViewName),
                DBOperType = 8,
                SelectSubType = 6,
                DBSelectResultType = 2,
                EqualQueryParam = ""
            };
        }

        public void SaveLog(object OperLogIdent, string DetailDescription = "")
        {
            //日志ID、登录人类别(LoginCategoryID)、
            //操作人ID(LogPersonID)、操作人(LogPerson)、操作日期(OperDate)
            //主键名(IdentPkName)、主键值(OperLogIdent)、
            //操作名称(添加、修改、下载、查询)、日志描述、
            //var Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
            if (!string.IsNullOrEmpty(DetailDescription))
            {
                int? OperLogIdentInt = null;
                if (OperLogIdent != null && OperLogIdent != DBNull.Value)
                {
                    OperLogIdentInt = Convert.ToInt32(OperLogIdent);
                }
                //var DetailDescription = "";

                var sql = ProjectCache.GetSqlLog(LoginInfo.LoginCategoryID, Design_ModularOrFun.ModularOrFunCode, LoginInfo.Pre_UserID,
                LoginInfo.UserName, OperLogIdentInt, ModularName, DetailDescription);
                Query16(sql, 1);
            }
        }

        /// <summary>
        /// 初始化排序字段
        /// </summary>
        public void BulidPageQueryBase()
        {
            if (PageQueryBase.RankInfo == null || PageQueryBase.RankInfo.Length == 0)
            {
                if (string.IsNullOrEmpty(Design_ModularOrFun.TSqlDefaultSort))
                {
                    PageQueryBase.RankInfo = "UpdateDate|1";
                }
                else
                {
                    var sorts = Design_ModularOrFun.TSqlDefaultSort.Split('|');
                    if (sorts.Length == 2)
                        PageQueryBase.RankInfo = sorts[0] + "|" + sorts[1];
                    else
                        PageQueryBase.RankInfo = sorts[0] + "|1";
                }
            }
        }

        public MyResponseBase QueryIndex()
        {
            var resp = new MyResponseBase();

            if (PageQueryBase.RankInfo == null || PageQueryBase.RankInfo.Length == 0)
            {
                if (string.IsNullOrEmpty(Design_ModularOrFun.TSqlDefaultSort))
                {
                    PageQueryBase.RankInfo = "UpdateDate|0";
                }
                else
                {
                    var sorts = Design_ModularOrFun.TSqlDefaultSort.Split('|');
                    if (sorts.Length == 2)
                        PageQueryBase.RankInfo = sorts[0] + "|" + sorts[1];
                    else
                        PageQueryBase.RankInfo = sorts[0] + "|1";
                }
            }
            SoftProjectAreaEntityDomain_Domain();

            Sys_HOperControl = new SoftProjectAreaEntity
            {
                DBTSql = ProjectCache.GetQuerySql(TabViewName),
                DBOperType = 8,
                SelectSubType = 6,
                DBSelectResultType = 2,
                EqualQueryParam = ""
            };
            if (Design_ModularOrFun.BCalCol == 1)
            {
                bCal = 1;
                ModularOrFunCode = Design_ModularOrFun.ModularOrFunCode;
                //bCal,ModularOrFunCode
            }
            //BulidPageQueryBase();
            //BulidHOperControl();
            resp = Execute();

            resp.Querys = Querys;
            resp.Item = Item;
            return resp;
        }

        public MyResponseBase QueryIndexByTree(string ParentFieldName)
        {
            //ViewName, string ParentFieldName, int? PkValue, string PkFieldName
            var resp = new MyResponseBase();

            if (PageQueryBase.RankInfo == null || PageQueryBase.RankInfo.Length == 0)
            {
                if (string.IsNullOrEmpty(Design_ModularOrFun.TSqlDefaultSort))
                {
                    PageQueryBase.RankInfo = "UpdateDate|0";
                }
                else
                {
                    var sorts = Design_ModularOrFun.TSqlDefaultSort.Split('|');
                    if (sorts.Length == 2)
                        PageQueryBase.RankInfo = sorts[0] + "|" + sorts[1];
                    else
                        PageQueryBase.RankInfo = sorts[0] + "|1";
                }
            }
            SoftProjectAreaEntityDomain_Domain();

            Sys_HOperControl = new SoftProjectAreaEntity
            {
                DBTSql = ProjectCache.GetQuerySqlByTree(TabViewName, ParentFieldName, PKField),
                DBOperType = 8,
                SelectSubType = 6,
                DBSelectResultType = 2,
                EqualQueryParam = ""
            };
            if (Design_ModularOrFun.BCalCol == 1)
            {
                bCal = 1;
                ModularOrFunCode = Design_ModularOrFun.ModularOrFunCode;
                //bCal,ModularOrFunCode
            }
            //BulidPageQueryBase();
            //BulidHOperControl();
            resp = Execute();

            resp.Querys = Querys;
            resp.Item = Item;
            return resp;
        }

        public void SoftProjectAreaEntityDomain_Domain()
        {
            ModularName = Design_ModularOrFun.ModularName;
            PKField = Design_ModularOrFun.PrimaryKey;// "Comp_CompanyID";
            //PKFields = new List<string> { "Comp_CompanyID" };
            TableName = Design_ModularOrFun.Design_ModularFieldTableName;// "Comp_Company";
            TabViewName = Design_ModularOrFun.TabViewName;// "V_Comp_Company"; 
            if (string.IsNullOrEmpty(TabViewName))
                TabViewName = "V_" + TableName;
        }

        public MyResponseBase Execute()
        {
            //SoftProjectAreaEntity LoginInfo = HttpContext.Current.Session["LoginInfo"] as SoftProjectAreaEntity;

            #region 设置创建人、创建时间、更新人、更新时间
            if (LoginInfo != null)
            {
                Item.CreateUserID = LoginInfo.Pre_UserID;
                Item.CreateUserName = LoginInfo.UserName;

                Item.UpdateUserID = LoginInfo.Pre_UserID;
                Item.UpdateUserName = LoginInfo.UserName;
            }
            #endregion

            Item.CreateDate = DateTime.Now;
            Item.UpdateDate = DateTime.Now;

            MyResponseBase response = new MyResponseBase();

            if (!RAInfo.bError)
            {
                //数据库存操作
                MyADORepository dal = new MyADORepository(this);
                dal.Execute(response);
            }
            return response;
        }


        #endregion

        public SoftProjectAreaEntity _Design_ModularOrFun { get; set; }

        public SoftProjectAreaEntity Design_ModularOrFun
        {
            get
            {
                if (_Design_ModularOrFun == null)
                {
                    //ModularOrFunCode = "DocArea.Doc_Category.Index";
                    _Design_ModularOrFun = ProjectCache.Design_ModularOrFuns.Where(p => p.ModularOrFunCode == ModularOrFunCode).FirstOrDefault();
                }
                return _Design_ModularOrFun;
            }
            set
            {
                _Design_ModularOrFun = value;
            }
        }


        /// <summary>
        /// 是否记录日志
        /// </summary>
        public int? bLog { get; set; }

        /// <summary>
        /// 日志字段
        /// </summary>
        public string LogFields { get; set; }

        private Querys _Querys;
        public Querys Querys
        {
            get
            {
                return _Querys;
            }
            set
            {
                _Querys = value;
                //var type = Item.GetType();
                //foreach (var item in Querys)
                //{
                //    var FieldName = item.FieldName;
                //    if (item.QuryType == 0)
                //        FieldName = Regex.Split(FieldName, "___", RegexOptions.IgnoreCase)[0];
                //    if (!string.IsNullOrEmpty(item.Value))
                //    {
                //        SetValue(Item, FieldName, item.Value);
                //        //PropertyInfo propertyInfo = type.GetProperty(FieldName);
                //        //propertyInfo.SetValue(Item, item.Value, null);
                //    }
                //}
            }
        }

        /// <summary>
        /// 设置相应属性的值
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="fieldName">属性名</param>
        /// <param name="fieldValue">属性值</param>
        public static void SetValue(object entity, string fieldName, string fieldValue)
        {
            Type entityType = entity.GetType();

            PropertyInfo propertyInfo = entityType.GetProperty(fieldName);
            if (propertyInfo == null)
                return;
            if (IsType(propertyInfo.PropertyType, "System.String"))
            {
                if (fieldValue == "")
                    fieldValue = "";
                propertyInfo.SetValue(entity, fieldValue, null);
            }
            else if (IsType(propertyInfo.PropertyType, "System.Boolean"))
            {
                propertyInfo.SetValue(entity, Boolean.Parse(fieldValue), null);

            }
            else if (IsType(propertyInfo.PropertyType, "System.Int32"))
            {
                if (fieldValue != "")
                    propertyInfo.SetValue(entity, int.Parse(fieldValue), null);
                else
                    propertyInfo.SetValue(entity, 0, null);

            }
            else if (IsType(propertyInfo.PropertyType, "System.Decimal"))
            {
                if (fieldValue != "")
                    propertyInfo.SetValue(entity, Decimal.Parse(fieldValue), null);
                else
                    propertyInfo.SetValue(entity, new Decimal(0), null);

            }
            else if (IsType(propertyInfo.PropertyType, "System.Nullable`1[System.DateTime]"))
            {
                if (fieldValue != "")
                {
                    try
                    {
                        if (fieldValue == "")
                            propertyInfo.SetValue(entity, null, null);
                        else
                        {
                            var datetemp = DateTime.Now;
                            DateTime.TryParse(fieldValue, out datetemp);
                            propertyInfo.SetValue(
                                entity, datetemp,
                                //(DateTime?)DateTime.ParseExact(fieldValue, "yyyy-MM-dd HH:mm:ss", null),
                            null);
                        }
                    }
                    catch
                    {
                        propertyInfo.SetValue(entity, (DateTime?)DateTime.ParseExact(fieldValue, "yyyy-MM-dd", null), null);
                    }
                }
                else
                    propertyInfo.SetValue(entity, null, null);
            }
            else if (IsType(propertyInfo.PropertyType, "System.Nullable`1[System.Int32]"))
            {
                if (fieldValue == "")
                    propertyInfo.SetValue(entity, null, null);
                else
                {
                    var val = 0;
                    Int32.TryParse(fieldValue, out val);
                    propertyInfo.SetValue(entity, val, null);
                }
            }
        }

        /// <summary>
        /// 类型匹配
        /// </summary>
        /// <param name="type"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static bool IsType(Type type, string typeName)
        {
            if (type.ToString() == typeName)
                return true;
            if (type.ToString() == "System.Object")
                return false;

            return IsType(type.BaseType, typeName);
        }

        public PageQueryBase PageQueryBase
        {
            get;
            set;
        }


        /// <summary>
        /// 功能(英文)
        /// </summary>
        public string FunNameEn { get; set; }

        /// <summary>
        /// 功能(中文)
        /// </summary>
        public string FunNameCn { get; set; }

        /// <summary>
        /// 子查询
        /// </summary>
        public int? SelectSubType { get; set; }

        RespAttachInfo RAInfo { get; set; }

        /// <summary>
        /// 查询是否计算
        /// </summary>
        public int? bCal { get; set; }

        /// <summary>
        /// 页面编码
        /// </summary>
        public string ModularOrFunCode { get; set; }

        public string LogMessage { get; set; }

        #region 功能配置

        SoftProjectAreaEntity _Sys_HOperControl;

        public SoftProjectAreaEntity Sys_HOperControl
        {
            get
            {
                if (_Sys_HOperControl == null)
                {
                    var sys_HOperControls = ProjectCache.Sys_HOperControls.Where(p => p.OperCode == OperCode);
                    if (sys_HOperControls.Count() > 0)
                        _Sys_HOperControl = sys_HOperControls.FirstOrDefault();
                    else
                        _Sys_HOperControl = null;
                }
                return _Sys_HOperControl;
            }
            set
            {
                _Sys_HOperControl = value;
            }
        }

        #endregion

        public string OperCode { get; set; }

        public string ModularName { get; set; }
        public string TableName { get; set; }
        public string TabViewName { get; set; }
        public string PKField { get; set; }

        //public List<string> PKFields { get; set; }
        public string PKOperCode { get; set; }

        public SoftProjectAreaEntity Item { get; set; }

        public List<SoftProjectAreaEntity> Items { get; set; }

        public MyResponseBase Query16(string sql, int DBSelectResultType = 2)
        {
            SoftProjectAreaEntity hOperControl = new SoftProjectAreaEntity
            {
                DBTSql = sql,
                DBOperType = 16,
                DBSelectResultType = DBSelectResultType,
            };
            Sys_HOperControl = hOperControl;

            var resp = Execute();
            resp.Querys = Querys;
            if (DBSelectResultType != 4)
                resp.Item = Item;
            return resp;
        }

        public MyResponseBase Query4(int? PKValue)
        {
            string sql = ProjectCache.GetQuerySqlByID(TabViewName, PKField, PKValue);

            SoftProjectAreaEntity hOperControl = new SoftProjectAreaEntity
            {
                DBTSql = sql,
                DBOperType = 16,
                DBSelectResultType = 4,
            };
            Sys_HOperControl = hOperControl;

            var resp = Execute();
            resp.Querys = Querys;
            return resp;
        }

        /// <summary>
        /// 根据主键查询--显示
        /// </summary>
        /// <returns></returns>
        public MyResponseBase ByID()
        {
            SoftProjectAreaEntityDomain_Domain();
            #region 获取主键值
            var PKFieldValue = 0;

            if (string.IsNullOrEmpty(PKField))
                throw new Exception(ModularName + "主键字段名：不能为空！");
            var type = Item.GetType(); //获取类型

            PropertyInfo propertyInfo = type.GetProperty(PKField);
            var PrimaryKeyItem = propertyInfo.GetValue(Item, null);
            if (PrimaryKeyItem == null || PrimaryKeyItem == DBNull.Value)
            {
                throw new Exception(ModularName + "主键值：不能为空！");
            }
            else
                PKFieldValue = Convert.ToInt32(PrimaryKeyItem);
            #endregion

            string sql = ProjectCache.GetQuerySqlByID(TabViewName, PKField, PKFieldValue);

            SoftProjectAreaEntity hOperControl = new SoftProjectAreaEntity
            {
                DBTSql = sql,
                DBOperType = 16,
                DBSelectResultType = 4,
            };
            Sys_HOperControl = hOperControl;

            var resp = Execute();
            resp.Querys = Querys;

            ByIDSaveLog(PrimaryKeyItem);
            return resp;
        }

        public void ByIDSaveLog(object OperLogIdent)
        {
            var DetailDescription = "";

            var fields = HtmlHelpersProject.LogSortFormEleTypes(Design_ModularOrFun);
            if (fields.Count > 0)
            {
                var type = Item.GetType();
                PropertyInfo propertyInfo = null;

                foreach (var item in fields)
                {
                    propertyInfo = type.GetProperty(item.name);
                    if (propertyInfo == null)
                        continue;
                    var val = propertyInfo.GetValue(Item, null);
                    if (val != null)
                        DetailDescription += item.NameCn + ":" + Convert.ToString(val) + ",";
                }
                DetailDescription = DetailDescription.Substring(0, DetailDescription.Length - 1);
                SaveLog(OperLogIdent, DetailDescription);
            }
        }


        /// <summary>
        /// 根据主键查询--显示
        /// </summary>
        /// <returns></returns>
        public MyResponseBase DeleteByID()
        {
            SoftProjectAreaEntityDomain_Domain();
            #region 获取主键值
            var PKFieldValue = 0;

            if (string.IsNullOrEmpty(PKField))
                throw new Exception(ModularName + "主键字段名：不能为空！");
            var type = Item.GetType(); //获取类型

            PropertyInfo propertyInfo = type.GetProperty(PKField);
            var PrimaryKeyItem = propertyInfo.GetValue(Item, null);
            if (PrimaryKeyItem == null || PrimaryKeyItem == DBNull.Value)
            {
                throw new Exception(ModularName + "主键值：不能为空！");
            }
            else
                PKFieldValue = Convert.ToInt32(PrimaryKeyItem);
            #endregion
            var sql = string.Format("DELETE  {0}  WHERE {1}={2} ", TableName, PKField, PKFieldValue);
            var resp = Query16(sql, 1);
            return resp;
        }


        #region 委托执行
        public MyResponseBase ExecuteDelegate(Action<SoftProjectAreaEntityDomain> DelegateMethod)
        {
            var resp = new MyResponseBase();

            #region (2)编辑
            using (var scope = new TransactionScope())
            {
                try
                {
                    DelegateMethod(null);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    scope.Dispose();
                }
            }
            #endregion
            return resp;
        }
        #endregion

        #region 添加

        /// <summary>
        /// 参数由配置文件完成
        /// </summary>
        /// <returns></returns>
        public MyResponseBase AddSave()
        {
            var resp = new MyResponseBase();
            SoftProjectAreaEntityDomain_Domain();
            #region (2)添加
            var DBFieldVals = Design_ModularOrFun.TSql;
            if (string.IsNullOrEmpty(DBFieldVals))//如果TSql字段为空，则从页面配置中获取
            {
                DBFieldVals = HtmlHelpersProject.GetPageSaveEleTypes(Design_ModularOrFun);
            }

            var LogFields = HtmlHelpersProject.LogSortFormEleTypes(Design_ModularOrFun);
            resp = AddSave(DBFieldVals, LogFields);
            #endregion
            return resp;
        }

        public MyResponseBase AddSave(string DBFieldVals, List<SoftProjectAreaEntity> LogFields = null)
        {
            var resp = new MyResponseBase();
            var hOperControl = new SoftProjectAreaEntity
            {
                DBOperType = 1,
                DBSelectResultType = 1,
                DBSqlParam = DBFieldVals
            };
            Sys_HOperControl = hOperControl;
            resp = Execute();
            if (LogFields != null)
            {
                AddSaveLog(resp.Obj, LogFields);
            }
            return resp;
        }

        public void AddSaveLog(object OperLogIdent, List<SoftProjectAreaEntity> LogFields)
        {
            var DetailDescription = "";

            //var fields = HtmlHelpersProject.LogSortFormEleTypes(Design_ModularOrFun);
            if (LogFields.Count > 0)
            {
                var type = Item.GetType();
                PropertyInfo propertyInfo = null;

                foreach (var item in LogFields)
                {
                    propertyInfo = type.GetProperty(item.name);
                    if (propertyInfo == null)
                        continue;
                    var val = propertyInfo.GetValue(Item, null);
                    if (val != null)
                        DetailDescription += item.NameCn + ":" + Convert.ToString(val) + ",";
                }
                DetailDescription = DetailDescription.Substring(0, DetailDescription.Length - 1);
                SaveLog(OperLogIdent, DetailDescription);
            }
        }

        /// <summary>
        /// 批量添加：从由配置文件完成
        /// </summary>
        /// <returns></returns>
        public MyResponseBase AddSaves()
        {
            var resp = new MyResponseBase();
            //Sys_HOperControl = null;
            SoftProjectAreaEntityDomain_Domain();
            var DBFieldVals = Design_ModularOrFun.TSql;
            if (string.IsNullOrEmpty(DBFieldVals))
            {
                DBFieldVals = HtmlHelpersProject.GetPageSaveEleTypes(Design_ModularOrFun);
            }
            resp = AddSaves(DBFieldVals);
            return resp;
        }

        public MyResponseBase AddSaves(string DBFieldVals)
        {
            var resp = new MyResponseBase();
            var hOperControl = new SoftProjectAreaEntity
            {
                DBOperType = 1,
                DBSelectResultType = 1,
                DBSqlParam = DBFieldVals
            };
            Sys_HOperControl = hOperControl;

            foreach (var item in Items)
            {
                Item = item;
                resp = Execute();
            }
            return resp;
        }

        #endregion

        #region 编辑

        /// <summary>
        /// 参数由配置文件完成
        /// </summary>
        /// <returns></returns>
        public MyResponseBase EditSave()
        {
            var resp = new MyResponseBase();
            SoftProjectAreaEntityDomain_Domain();

            #region (2)添加
            var DBFieldVals = Design_ModularOrFun.TSql;
            if (string.IsNullOrEmpty(DBFieldVals))//如果TSql字段为空，则从页面配置中获取
            {
                DBFieldVals = HtmlHelpersProject.GetPageSaveEleTypes(Design_ModularOrFun);
            }

            var LogFields = HtmlHelpersProject.LogSortFormEleTypes(Design_ModularOrFun);
            resp = EditSave(DBFieldVals, LogFields);
            #endregion
            return resp;
        }

        /// <summary>
        /// 编辑单个对象
        /// </summary>
        /// <param name="DBFieldVals"></param>
        /// <returns></returns>
        public MyResponseBase EditSave(string DBFieldVals, List<SoftProjectAreaEntity> LogFields=null)
        {
            #region 主键检查
            //var PKFieldValue = 0;

            if (string.IsNullOrEmpty(PKField))
                throw new Exception(ModularName + "主键字段名：不能为空！");
            var type = Item.GetType(); //获取类型

            PropertyInfo propertyInfo = type.GetProperty(PKField);
            var PrimaryKeyItem = propertyInfo.GetValue(Item, null);
            if (PrimaryKeyItem == null || PrimaryKeyItem == DBNull.Value)
            {
                throw new Exception(ModularName + "主键值：不能为空！");
            }
            #endregion

            MyResponseBase resp = new MyResponseBase();
            var hOperControl = new SoftProjectAreaEntity
                    {
                        DBOperType = 2,
                        DBSelectResultType = 1,
                        DBSqlParam = DBFieldVals
                    };
            Sys_HOperControl = hOperControl;
            resp = Execute();
            if (LogFields != null)
            {
                AddSaveLog(PrimaryKeyItem, LogFields);
            }
            return resp;
        }

        /// <summary>
        /// sql语句由数据库年提供
        /// </summary>
        /// <returns></returns>
        public MyResponseBase EditSaves()
        {
            MyResponseBase resp = new MyResponseBase();
            SoftProjectAreaEntityDomain_Domain();

            var DBFieldVals = Design_ModularOrFun.TSql;
            if (string.IsNullOrEmpty(DBFieldVals))
            {
                DBFieldVals = HtmlHelpersProject.GetPageSaveEleTypes(Design_ModularOrFun);
            }
            resp = EditSaves(DBFieldVals);
            return resp;
        }

        /// <summary>
        /// 编辑对象集
        /// </summary>
        /// <param name="DBFieldVals"></param>
        /// <returns></returns>
        public MyResponseBase EditSaves(string DBFieldVals)
        {
            MyResponseBase resp = new MyResponseBase();

            var hOperControl = new SoftProjectAreaEntity
            {
                DBOperType = 2,
                DBSelectResultType = 1,
                DBSqlParam = DBFieldVals
            };
            Sys_HOperControl = hOperControl;

            foreach (var item in Items)
            {
                Item = item;
                resp = Execute();
            }
            return resp;
        }

        #endregion

        /// <summary>
        /// 编辑对象集--后台配置数据库
        /// </summary>
        /// <param name="DBFieldVals"></param>
        /// <returns></returns>
        public MyResponseBase ExecuteEnumByOperCode(string operCode)
        {
            MyResponseBase resp = new MyResponseBase();
            Sys_HOperControl = null;

            OperCode = operCode;

            foreach (var item in Items)
            {
                Item = item;
                resp = Execute();
            }

            return resp;
        }

        /// <summary>
        /// 根据模块编码：执行添加、修改
        /// </summary>
        /// <param name="DBOperType"></param>
        /// <returns></returns>
        public MyResponseBase ExcuteEnumsByModularOrFunCode(int DBOperType = 1)
        {
            MyResponseBase resp = new MyResponseBase();

            SoftProjectAreaEntityDomain_Domain();

            #region (2)编辑
            using (var scope = new TransactionScope())
            {
                try
                {
                    var DBFieldVals = Design_ModularOrFun.TSql;
                    if (string.IsNullOrEmpty(DBFieldVals))
                    {
                        DBFieldVals = HtmlHelpersProject.GetPageSaveEleTypes(Design_ModularOrFun);
                    }

                    var hOperControl = new SoftProjectAreaEntity
                    {
                        DBOperType = DBOperType,
                        DBSelectResultType = 1,
                        DBSqlParam = DBFieldVals
                    };
                    Sys_HOperControl = hOperControl;
                    foreach (var item in Items)
                    {
                        Item = item;
                        resp = Execute();
                    }
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    scope.Dispose();
                }
            }
            #endregion
            return resp;
        }
    }
}
