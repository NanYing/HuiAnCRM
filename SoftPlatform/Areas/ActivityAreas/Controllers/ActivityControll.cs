﻿using Framework.Core;
using Framework.Web.Mvc;
using SoftProject.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SoftPlatform.Controllers
{
    /// <summary>
    /// 控制器：Activity(活动管理)
    /// </summary>
    public class ActivityController : BaseController
    {
        public ActivityController()
        {
        }

        /// <summary>
        /// 活动管理--查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ActivityAreas.Activity.Index";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.QueryIndex();

            if (Request.IsAjaxRequest())
                return View(Design_ModularOrFun.PartialView, resp);
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 活动管理--添加查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Add(SoftProjectAreaEntityDomain domain)
        {
            var resp = domain.Default();
            ModularOrFunCode = "ActivityAreas.Activity.Add";
            resp.FunNameEn = "Add";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 活动管理--添加保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult AddSave(SoftProjectAreaEntityDomain domain)
        {
            #region 数据验证
            #endregion
            ModularOrFunCode = "ActivityAreas.Activity.Add";
            domain.ModularOrFunCode = ModularOrFunCode;
            if (domain.Item.ActivitysDate > domain.Item.ActivityeDate)
            {
                throw new Exception("起始日期不能大于结束日期");
            }            
            if (domain.MeetingRoomIsUse(domain.Item.MeetingRoomID,domain.Item.ActivitysDate,domain.Item.ActivityeDate,null)){
                throw new Exception("当前会议室" + "在" + domain.Item.ActivitysDate.Format_yyyy_MM_dd() + "至" + domain.Item.ActivityeDate.Format_yyyy_MM_dd()+"期间被占用！");
            }
            var resp = domain.AddSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 活动管理--编辑查询
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Edit(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ActivityAreas.Activity.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            ModularOrFunCode = "ActivityAreas.Activity.Edit";
            resp.FunNameEn = "Edit";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }

        /// <summary>
        /// 活动管理--编辑保存
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost]
        public HJsonResult EditSave(SoftProjectAreaEntityDomain domain)
        {
            #region 数据验证
            #endregion
            ModularOrFunCode = "ActivityAreas.Activity.Edit";
            domain.ModularOrFunCode = ModularOrFunCode;
           
            if (domain.Item.ActivitysDate > domain.Item.ActivityeDate)
            {
                throw new Exception("起始日期不能大于结束日期");
            }
            if (domain.MeetingRoomIsUse(domain.Item.MeetingRoomID, domain.Item.ActivitysDate, domain.Item.ActivityeDate, domain.Item.ActivityID))
            {
                throw new Exception("当前会议室" + "在" + domain.Item.ActivitysDate.Format_yyyy_MM_dd() + "至" + domain.Item.ActivityeDate.Format_yyyy_MM_dd() + "期间被占用！");
            }
           
            var resp = domain.EditSave();
            return new HJsonResult(new { Data = resp });
        }

        /// <summary>
        /// 活动管理--查看
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public ActionResult Detail(SoftProjectAreaEntityDomain domain)
        {
            ModularOrFunCode = "ActivityAreas.Activity.Detail";
            domain.ModularOrFunCode = ModularOrFunCode;
            var resp = domain.ByID();

            resp.FunNameEn = "Detail";
            resp.ViewContextName = Design_ModularOrFun.PartialView;
            return View(Design_ModularOrFun.MainView, resp);
        }



    }
}
